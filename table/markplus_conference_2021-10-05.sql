# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.5.9-MariaDB-1:10.5.9+maria~focal)
# Database: markplus_conference
# Generation Time: 2021-10-05 03:08:25 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table chats
# ------------------------------------------------------------

DROP TABLE IF EXISTS `chats`;

CREATE TABLE `chats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'chat',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `chats` WRITE;
/*!40000 ALTER TABLE `chats` DISABLE KEYS */;

INSERT INTO `chats` (`id`, `name`, `content`, `ip`, `type`, `created_at`, `updated_at`)
VALUES
	(8,'vento','join','172.22.0.1','info','2021-09-10 19:10:15','2021-09-10 19:10:15'),
	(9,'tes','join','172.22.0.1','info','2021-09-10 21:21:19','2021-09-10 21:21:19'),
	(10,'tes','tes','172.22.0.1','chat','2021-09-10 21:21:23','2021-09-10 21:21:23');

/*!40000 ALTER TABLE `chats` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table conf_admins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `conf_admins`;

CREATE TABLE `conf_admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `conf_admins` WRITE;
/*!40000 ALTER TABLE `conf_admins` DISABLE KEYS */;

INSERT INTO `conf_admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'Admin Conference','admin@cranium.id','$2y$10$Vkj.CrsvCsom3jBp9kzywudSctx2FpJLpkO/9peUzEJ9sh./JAVnW','S7AFmjLNwh0JNCMNGcowFjwg744NlckAJ7fxiUCdtFwV82HV1ZgRlMvD6JkV','2020-11-12 07:54:03','2020-11-19 02:53:54');

/*!40000 ALTER TABLE `conf_admins` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table conf_logins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `conf_logins`;

CREATE TABLE `conf_logins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code_voucher` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `agency_name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `conf_logins` WRITE;
/*!40000 ALTER TABLE `conf_logins` DISABLE KEYS */;

INSERT INTO `conf_logins` (`id`, `code_voucher`, `name`, `email`, `phone`, `agency_name`, `created_at`, `updated_at`)
VALUES
	(24,'C82550','Kalm Kalm','chandra@cranium.id','081284376223','PT Cranium','2020-11-19 15:05:35','2021-09-12 19:54:44'),
	(26,'E98112','Kalm Kalm','iqbal@cranium.id','081284376223','PT Cranium','2020-11-19 15:57:05','2020-11-19 15:57:05'),
	(28,'E98112','Kalm Kalm','chandra2@cranium.id','081284376223','PT Cranium Indonesia','2020-11-19 16:06:34','2020-11-19 16:06:34'),
	(30,'C82550','Chandra Then','chandra3@cranium.id','081284376223','PT Cranium Indonesia','2020-11-19 16:27:38','2020-11-19 16:27:38'),
	(48,'C82550','Kalm Kalm','chandra@cranium.ids','0812843762232','PT. Cranium Royal Aditama','2020-11-24 11:11:30','2020-11-24 11:11:30'),
	(49,'C82550','Kalm Kalm','chandra@cranium.ida','0812843762233','PT. Cranium Royal Aditama','2020-11-26 13:56:37','2020-11-26 13:56:37');

/*!40000 ALTER TABLE `conf_logins` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table conf_vouchers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `conf_vouchers`;

CREATE TABLE `conf_vouchers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code_voucher` varchar(10) NOT NULL,
  `email` varchar(255) NOT NULL,
  `quota` smallint(6) NOT NULL DEFAULT 0,
  `remaining_quota` smallint(6) NOT NULL DEFAULT 0,
  `arrayPayment` text DEFAULT NULL,
  `status_code` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `conf_vouchers` WRITE;
/*!40000 ALTER TABLE `conf_vouchers` DISABLE KEYS */;

INSERT INTO `conf_vouchers` (`id`, `code_voucher`, `email`, `quota`, `remaining_quota`, `arrayPayment`, `status_code`, `status`, `created_at`, `updated_at`)
VALUES
	(1,'C82550','miqbalnugraha19@gmail.com',20,15,NULL,NULL,NULL,'2020-11-13 15:23:21','2020-11-26 16:56:14'),
	(3,'E64793','chandra@cranium.id',2,0,'{\"status_code\":200,\"status_message\":\"Success, Credit card transaction is successful\",\"bank\":\"bni\",\"transaction_id\":\"1ac7c830-3ee1-4046-9d5c-f6695cc46143\",\"order_id\":\"E64793\",\"redirect_url\":\"https:\\/\\/api.sandbox.veritrans.co.id\\/v2\\/token\\/rba\\/redirect\\/481111-1114-1ac7c830-3ee1-4046-9d5c-f6695cc46143\",\"gross_amount\":\"1000000.00\",\"payment_type\":\"credit_card\",\"transaction_time\":\"2020-11-19 02:59:31\",\"transaction_status\":\"capture\",\"fraud_status\":\"accept\",\"masked_card\":\"481111-1114\",\"card_type\":\"credit\"}',200,'PAID','2020-11-19 03:08:42','2020-11-19 13:51:05'),
	(4,'E81310','chandra@cranium.id',5,5,'{\"status_code\":200,\"status_message\":\"Success, Credit card transaction is successful\",\"bank\":\"bni\",\"transaction_id\":\"8257ded9-acf3-4380-8d12-75c84617275f\",\"order_id\":\"E81310\",\"redirect_url\":\"https:\\/\\/api.sandbox.veritrans.co.id\\/v2\\/token\\/rba\\/redirect\\/481111-1114-8257ded9-acf3-4380-8d12-75c84617275f\",\"gross_amount\":\"2000000.00\",\"payment_type\":\"credit_card\",\"transaction_time\":\"2020-11-19 03:10:46\",\"transaction_status\":\"capture\",\"fraud_status\":\"accept\",\"masked_card\":\"481111-1114\",\"card_type\":\"credit\"}',200,'PAID','2020-11-19 03:10:54','2020-11-19 15:04:46'),
	(5,'E66130','chandra@cranium.id',10,10,'{\"status_code\":200,\"status_message\":\"Success, Credit card transaction is successful\",\"bank\":\"bni\",\"transaction_id\":\"25e45f7d-12e6-47a5-ab73-b8610f169d29\",\"order_id\":\"E66130\",\"redirect_url\":\"https:\\/\\/api.sandbox.veritrans.co.id\\/v2\\/token\\/rba\\/redirect\\/481111-1114-25e45f7d-12e6-47a5-ab73-b8610f169d29\",\"gross_amount\":\"2500000.00\",\"payment_type\":\"credit_card\",\"transaction_time\":\"2020-11-19 03:14:05\",\"transaction_status\":\"capture\",\"fraud_status\":\"accept\",\"masked_card\":\"481111-1114\",\"card_type\":\"credit\"}',200,'PAID','2020-11-19 03:14:13','2020-11-19 03:14:13'),
	(6,'E98112','chandra@cranium.id',2,0,'{\"status_code\":200,\"status_message\":\"Success, Credit card transaction is successful\",\"bank\":\"bni\",\"transaction_id\":\"e2f6a4c4-c4dd-431c-baf6-6f608fb4bf54\",\"order_id\":\"E98112\",\"redirect_url\":\"https:\\/\\/api.sandbox.veritrans.co.id\\/v2\\/token\\/rba\\/redirect\\/481111-1114-e2f6a4c4-c4dd-431c-baf6-6f608fb4bf54\",\"gross_amount\":\"1000000.00\",\"payment_type\":\"credit_card\",\"transaction_time\":\"2020-11-19 03:16:16\",\"transaction_status\":\"capture\",\"fraud_status\":\"accept\",\"masked_card\":\"481111-1114\",\"card_type\":\"credit\"}',200,'PAID','2020-11-19 03:16:23','2020-11-19 16:06:34'),
	(7,'E25119','miqbalnugraha19@gmail.com',2,2,'{\"status_code\":200,\"status_message\":\"Success, Credit card transaction is successful\",\"bank\":\"bni\",\"transaction_id\":\"15e99a39-b9bb-4ffc-812e-007db278a61f\",\"order_id\":\"E25119\",\"redirect_url\":\"https:\\/\\/api.sandbox.veritrans.co.id\\/v2\\/token\\/rba\\/redirect\\/481111-1114-15e99a39-b9bb-4ffc-812e-007db278a61f\",\"gross_amount\":\"1000000.00\",\"payment_type\":\"credit_card\",\"transaction_time\":\"2020-11-19 03:17:37\",\"transaction_status\":\"capture\",\"fraud_status\":\"accept\",\"masked_card\":\"481111-1114\",\"card_type\":\"credit\"}',200,'PAID','2020-11-19 03:17:44','2020-11-19 03:17:44'),
	(13,'C02767','miqbalnugraha19@gmail.com',12,12,NULL,NULL,NULL,'2020-11-20 11:10:24','2020-11-20 11:10:24'),
	(14,'C79976','miqbalnugraha19@gmail.com',12,12,NULL,NULL,NULL,'2020-11-20 15:03:33','2020-11-20 15:03:33'),
	(15,'C00930','miqbalnugraha19@gmail.com',1,1,NULL,NULL,NULL,'2020-11-20 15:12:30','2020-11-20 15:12:30');

/*!40000 ALTER TABLE `conf_vouchers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table conversations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `conversations`;

CREATE TABLE `conversations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table interest_promos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `interest_promos`;

CREATE TABLE `interest_promos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `interest` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `interest_promos` WRITE;
/*!40000 ALTER TABLE `interest_promos` DISABLE KEYS */;

INSERT INTO `interest_promos` (`id`, `interest`, `email`, `created_at`, `updated_at`)
VALUES
	(2,'Promo Pertamina','chandra@cranium.id','2021-09-12 19:55:26','2021-09-12 19:55:26'),
	(3,'Promo Pegadaian','chandra@cranium.id','2021-09-12 19:57:19','2021-09-12 19:57:19'),
	(4,'Promo Astra','chandra@cranium.id','2021-09-12 19:58:59','2021-09-12 19:58:59');

/*!40000 ALTER TABLE `interest_promos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2014_10_12_000000_create_users_table',1),
	(2,'2014_10_12_100000_create_password_resets_table',1),
	(3,'2019_08_20_155209_create_admins_table',1),
	(4,'2019_08_20_155210_create_admin_password_resets_table',1),
	(5,'2020_12_03_113524_create_conversations_table',1),
	(6,'2020_12_03_113606_create_groups_table',1),
	(7,'2020_12_03_113620_create_group_user_table',1),
	(8,'2018_02_10_160744_create_chats_table',2),
	(9,'2020_12_04_160821_create_interest_products_table',3);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
