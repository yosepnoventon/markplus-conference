-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 07, 2020 at 04:14 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.2.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `markplus_conference`
--

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--

CREATE TABLE `chats` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'chat',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `conf_admins`
--

CREATE TABLE `conf_admins` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `conf_admins`
--

INSERT INTO `conf_admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin Conference', 'admin@cranium.id', '$2y$10$Vkj.CrsvCsom3jBp9kzywudSctx2FpJLpkO/9peUzEJ9sh./JAVnW', 'S7AFmjLNwh0JNCMNGcowFjwg744NlckAJ7fxiUCdtFwV82HV1ZgRlMvD6JkV', '2020-11-12 07:54:03', '2020-11-19 02:53:54');

-- --------------------------------------------------------

--
-- Table structure for table `conf_logins`
--

CREATE TABLE `conf_logins` (
  `id` int(11) NOT NULL,
  `code_voucher` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `agency_name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `conf_logins`
--

INSERT INTO `conf_logins` (`id`, `code_voucher`, `name`, `email`, `phone`, `agency_name`, `created_at`, `updated_at`) VALUES
(24, 'C82550', 'Kalm Kalm', 'chandra@cranium.id', '081284376223', 'PT Cranium', '2020-11-19 15:05:35', '2020-11-19 15:05:35'),
(26, 'E98112', 'Kalm Kalm', 'iqbal@cranium.id', '081284376223', 'PT Cranium', '2020-11-19 15:57:05', '2020-11-19 15:57:05'),
(28, 'E98112', 'Kalm Kalm', 'chandra2@cranium.id', '081284376223', 'PT Cranium Indonesia', '2020-11-19 16:06:34', '2020-11-19 16:06:34'),
(30, 'C82550', 'Chandra Then', 'chandra3@cranium.id', '081284376223', 'PT Cranium Indonesia', '2020-11-19 16:27:38', '2020-11-19 16:27:38'),
(48, 'C82550', 'Kalm Kalm', 'chandra@cranium.ids', '0812843762232', 'PT. Cranium Royal Aditama', '2020-11-24 11:11:30', '2020-11-24 11:11:30'),
(49, 'C82550', 'Kalm Kalm', 'chandra@cranium.ida', '0812843762233', 'PT. Cranium Royal Aditama', '2020-11-26 13:56:37', '2020-11-26 13:56:37');

-- --------------------------------------------------------

--
-- Table structure for table `conf_vouchers`
--

CREATE TABLE `conf_vouchers` (
  `id` int(11) NOT NULL,
  `code_voucher` varchar(10) NOT NULL,
  `email` varchar(255) NOT NULL,
  `quota` smallint(6) NOT NULL DEFAULT 0,
  `remaining_quota` smallint(6) NOT NULL DEFAULT 0,
  `arrayPayment` text DEFAULT NULL,
  `status_code` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `conf_vouchers`
--

INSERT INTO `conf_vouchers` (`id`, `code_voucher`, `email`, `quota`, `remaining_quota`, `arrayPayment`, `status_code`, `status`, `created_at`, `updated_at`) VALUES
(1, 'C82550', 'miqbalnugraha19@gmail.com', 20, 15, NULL, NULL, NULL, '2020-11-13 15:23:21', '2020-11-26 16:56:14'),
(3, 'E64793', 'chandra@cranium.id', 2, 0, '{\"status_code\":200,\"status_message\":\"Success, Credit card transaction is successful\",\"bank\":\"bni\",\"transaction_id\":\"1ac7c830-3ee1-4046-9d5c-f6695cc46143\",\"order_id\":\"E64793\",\"redirect_url\":\"https:\\/\\/api.sandbox.veritrans.co.id\\/v2\\/token\\/rba\\/redirect\\/481111-1114-1ac7c830-3ee1-4046-9d5c-f6695cc46143\",\"gross_amount\":\"1000000.00\",\"payment_type\":\"credit_card\",\"transaction_time\":\"2020-11-19 02:59:31\",\"transaction_status\":\"capture\",\"fraud_status\":\"accept\",\"masked_card\":\"481111-1114\",\"card_type\":\"credit\"}', 200, 'PAID', '2020-11-19 03:08:42', '2020-11-19 13:51:05'),
(4, 'E81310', 'chandra@cranium.id', 5, 5, '{\"status_code\":200,\"status_message\":\"Success, Credit card transaction is successful\",\"bank\":\"bni\",\"transaction_id\":\"8257ded9-acf3-4380-8d12-75c84617275f\",\"order_id\":\"E81310\",\"redirect_url\":\"https:\\/\\/api.sandbox.veritrans.co.id\\/v2\\/token\\/rba\\/redirect\\/481111-1114-8257ded9-acf3-4380-8d12-75c84617275f\",\"gross_amount\":\"2000000.00\",\"payment_type\":\"credit_card\",\"transaction_time\":\"2020-11-19 03:10:46\",\"transaction_status\":\"capture\",\"fraud_status\":\"accept\",\"masked_card\":\"481111-1114\",\"card_type\":\"credit\"}', 200, 'PAID', '2020-11-19 03:10:54', '2020-11-19 15:04:46'),
(5, 'E66130', 'chandra@cranium.id', 10, 10, '{\"status_code\":200,\"status_message\":\"Success, Credit card transaction is successful\",\"bank\":\"bni\",\"transaction_id\":\"25e45f7d-12e6-47a5-ab73-b8610f169d29\",\"order_id\":\"E66130\",\"redirect_url\":\"https:\\/\\/api.sandbox.veritrans.co.id\\/v2\\/token\\/rba\\/redirect\\/481111-1114-25e45f7d-12e6-47a5-ab73-b8610f169d29\",\"gross_amount\":\"2500000.00\",\"payment_type\":\"credit_card\",\"transaction_time\":\"2020-11-19 03:14:05\",\"transaction_status\":\"capture\",\"fraud_status\":\"accept\",\"masked_card\":\"481111-1114\",\"card_type\":\"credit\"}', 200, 'PAID', '2020-11-19 03:14:13', '2020-11-19 03:14:13'),
(6, 'E98112', 'chandra@cranium.id', 2, 0, '{\"status_code\":200,\"status_message\":\"Success, Credit card transaction is successful\",\"bank\":\"bni\",\"transaction_id\":\"e2f6a4c4-c4dd-431c-baf6-6f608fb4bf54\",\"order_id\":\"E98112\",\"redirect_url\":\"https:\\/\\/api.sandbox.veritrans.co.id\\/v2\\/token\\/rba\\/redirect\\/481111-1114-e2f6a4c4-c4dd-431c-baf6-6f608fb4bf54\",\"gross_amount\":\"1000000.00\",\"payment_type\":\"credit_card\",\"transaction_time\":\"2020-11-19 03:16:16\",\"transaction_status\":\"capture\",\"fraud_status\":\"accept\",\"masked_card\":\"481111-1114\",\"card_type\":\"credit\"}', 200, 'PAID', '2020-11-19 03:16:23', '2020-11-19 16:06:34'),
(7, 'E25119', 'miqbalnugraha19@gmail.com', 2, 2, '{\"status_code\":200,\"status_message\":\"Success, Credit card transaction is successful\",\"bank\":\"bni\",\"transaction_id\":\"15e99a39-b9bb-4ffc-812e-007db278a61f\",\"order_id\":\"E25119\",\"redirect_url\":\"https:\\/\\/api.sandbox.veritrans.co.id\\/v2\\/token\\/rba\\/redirect\\/481111-1114-15e99a39-b9bb-4ffc-812e-007db278a61f\",\"gross_amount\":\"1000000.00\",\"payment_type\":\"credit_card\",\"transaction_time\":\"2020-11-19 03:17:37\",\"transaction_status\":\"capture\",\"fraud_status\":\"accept\",\"masked_card\":\"481111-1114\",\"card_type\":\"credit\"}', 200, 'PAID', '2020-11-19 03:17:44', '2020-11-19 03:17:44'),
(13, 'C02767', 'miqbalnugraha19@gmail.com', 12, 12, NULL, NULL, NULL, '2020-11-20 11:10:24', '2020-11-20 11:10:24'),
(14, 'C79976', 'miqbalnugraha19@gmail.com', 12, 12, NULL, NULL, NULL, '2020-11-20 15:03:33', '2020-11-20 15:03:33'),
(15, 'C00930', 'miqbalnugraha19@gmail.com', 1, 1, NULL, NULL, NULL, '2020-11-20 15:12:30', '2020-11-20 15:12:30');

-- --------------------------------------------------------

--
-- Table structure for table `conversations`
--

CREATE TABLE `conversations` (
  `id` int(10) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `interest_promos`
--

CREATE TABLE `interest_promos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `interest` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_20_155209_create_admins_table', 1),
(4, '2019_08_20_155210_create_admin_password_resets_table', 1),
(5, '2020_12_03_113524_create_conversations_table', 1),
(6, '2020_12_03_113606_create_groups_table', 1),
(7, '2020_12_03_113620_create_group_user_table', 1),
(8, '2018_02_10_160744_create_chats_table', 2),
(9, '2020_12_04_160821_create_interest_products_table', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conf_admins`
--
ALTER TABLE `conf_admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conf_logins`
--
ALTER TABLE `conf_logins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conf_vouchers`
--
ALTER TABLE `conf_vouchers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conversations`
--
ALTER TABLE `conversations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `interest_promos`
--
ALTER TABLE `interest_promos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chats`
--
ALTER TABLE `chats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `conf_admins`
--
ALTER TABLE `conf_admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `conf_logins`
--
ALTER TABLE `conf_logins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `conf_vouchers`
--
ALTER TABLE `conf_vouchers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `conversations`
--
ALTER TABLE `conversations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `interest_promos`
--
ALTER TABLE `interest_promos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
