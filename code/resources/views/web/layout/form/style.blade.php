
<link rel="stylesheet" type="text/css" href="{{ url('/assets/form/bootstrap/css/bootstrap.min.css') }}">

<link rel="stylesheet" type="text/css" href="{{ url('/assets/form/css/font-awesome.min.css') }}">

<link rel="stylesheet" type="text/css" href="{{ url('/assets/form/vendor/animate/animate.css') }}">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css'>

<link rel="stylesheet" type="text/css" href="{{ url('/assets/form/vendor/css-hamburgers/hamburgers.min.css') }}">

<link rel="stylesheet" type="text/css" href="{{ url('/assets/form/vendor/select2/select2.min.css') }}">

<link rel="stylesheet" type="text/css" href="{{ url('/assets/form/css/util.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('/assets/form/css/main.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('/assets/form/css/style-conference.css') }}">