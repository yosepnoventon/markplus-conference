<!DOCTYPE html>
<html lang="en">
<head>
    <title>MarkPlus Conference @yield('title')</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" href="{{ url('/assets/form/img/favicon.png') }}" />

    @include('web.layout.form.style')
    @stack('style')

</head>
<body>
    @yield('content')
    @include('web.layout.form.script')
    
    @stack('script')

</body>
</html>
