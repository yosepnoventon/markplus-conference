<script defer src="{{ url('/assets/web/js/fontawesome.js') }}"></script>

<!-- Alert Toast -->
<script src="{{ url('/assets/web/js/toastr-2.1.3.min.js') }}"></script>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script> --}}

<!-- Sweet Alert -->
<script src="{{ url('/assets/web/js/sweetalert2.min.js') }}"></script>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.js"></script> --}}

<script src="{{ url('/assets/form/js/jquery-3.2.1.min.js') }}"></script>

 <!-- Popper.JS -->
<script src="{{ url('/assets/form/js/popper.js') }}"></script>
<script src="{{ url('/assets/form/bootstrap/js/bootstrap.min.js') }}"></script>

<script src="{{ url('/assets/form/vendor/select2/select2.min.js') }}"></script>

<script src="{{ url('/assets/form/vendor/tilt/tilt.jquery.min.js') }}"></script>
<script>
    $('.js-tilt').tilt({
        scale: 1.1
    })
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-155852833-1"></script>
<!-- Google Analytic -->
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-155852833-1');
</script>