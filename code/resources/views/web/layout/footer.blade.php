<section id="footer">
    <div class="new-container width-100">
        <div class="new-container-inner">
            <div class="box-middle-object">
                <div class="box-image-info">

                    <div class="box-logo">
                            <a href="{{ url('/') }}">
                            <img src="{{ url('/assets/web/img/logo.png') }}" class="img-fluid" alt="Responsive image">
                        </a>
                    </div>

                    <div class="box-info-container">
                        <div class="box-info">
                            <p>
                                <a href="{{ url('/about-us') }}" class="link-info" style="margin-right:5px">TENTANG KAMI </a>
                                {{-- <a href="{{ url('/contact-us') }}" class="link-info"> CONTACT US</a> --}}
                                {{-- <a href="{{ url('/term-and-condition') }}" class="link-info">T&C</a> --}}
                            </P>

                        </div>
                        
                        <div class="box-info">
                            <P>
                                {{-- <a href="{{ url('/faq') }}" class="link-info">FAQ </a><br> --}}
                                <a href="{{ url('/contact-us') }}" class="link-info">HUBUNGI KAMI</a>
                            </p>
                        </div>
                        <div class="box-info tnc">
                            <p>
                                <a href="{{ url('/term-and-condition') }}" class="link-info">TERMS & CONDITIONS </a><br>
                                {{-- <a href="{{ url('/contact-us') }}" class="link-info"> CONTACT US</a> --}}
                            </P>

                        </div>
                    </div>
                </div>

        {{-- <div class="box-media-container">
            @foreach($socialmedias as $socialmedia)
                @if($socialmedia->slug == 'facebook')

                    <div class="box-media" hidden>
                        <a href="{{ $socialmedia->url }}">
                            <div class="media-image">
                                <img src="{{ url('/assets/web/img/facebook.png') }}" class="img-fluid" alt="Responsive image">
                            </div>
                            <div class="media-text">
                                <p>MarkplusID</p>
                            </div>
                        </a>
                    </div>

                @elseif($socialmedia->slug == 'instagram')
                    <div class="box-media" hidden>
                        <a href="{{ $socialmedia->url }}">
                            <div class="media-image">
                                <img src="{{ url('/assets/web/img/instagram.png') }}" class="img-fluid" alt="Responsive image">
                            </div>
                            <div class="media-text">
                                <p>MarkplusIndonesia</p>
                            </div>

                        </a>
                    </div>
                @endif
            @endforeach
        </div> --}}
        {{-- <div class="label-navigation">
            <h5>BACK TO TOP</h5>
        </div> --}}
    </div>
    </div>
</div>
    @push('script')
        <script>
            jQuery(document).ready(function() {
                var offset = 220;
                var duration = 500;
                jQuery(window).scroll(function() {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('#myBtn').fadeIn(duration);
                    } else {
                        jQuery('#myBtn').fadeOut(duration);
                    }
                });

                jQuery('#myBtn').click(function(event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        </script>
    @endpush

</section>
