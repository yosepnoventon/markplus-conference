<!-- Sidebar  -->
<nav id="sidebar">
    @if(Auth::guard('user')->check())
        <div class="sidebar-header flex-between">
            <button type="button" class="btn btn-info rounded-circle" id="dismiss">
                <img src=" {{ url('assets/web/icons/left-arrow.svg') }} " alt="" class="img-fluid">
            </button>
            <div class="chek-in">
                <div class="after-login flex-between">
                    <div class="user-icon">
                        <img src="{{ url('/assets/web/icons/icon-user.svg') }}" class="img-fluid" alt="Responsive image">
                    </div>
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ Auth::guard('user')->user()->first_name }}
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="javascript:void(0)" id="assign-button-mobile">Data Peserta</a>
                            {{-- <a class="dropdown-item" href="javascript:void(0)" id="profile-button-mobile">Akun</a> --}}
                            <a class="dropdown-item" href="javascript:void(0)" id="order-button-mobile">Riwayat Order</a>
                            <a class="dropdown-item" href="javascript:void(0)" id="link-logout-mobile">Keluar
                                <img src=" {{ url('assets/web/icons/arrow-right.svg') }} " alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="sidebar-header flex-between">
            <button type="button" class="btn btn-info rounded-circle" id="dismiss">
                <img src=" {{ url('assets/web/icons/left-arrow.svg') }} " alt="" class="img-fluid">
            </button>
            <div class="chek-in">
                <div class="before-login flex-between">
                    <button type="button" class="btn btn-link" id="button-login">Masuk</button>
                    <button type="button" class="btn btn-warning" id="button-register">Daftar</button>
                </div>
            </div>
        </div>
    @endif
    <div class="box-scroll">
        <ul class="list-unstyled components">
            <li class="active">
                <a href="javascript:void(0)" id="beranda-mobile">Beranda</a>
            </li>
            @if(Auth::guard('user')->check())
                <li class="active">
                    <a href="javascript:void(0)" id="pelatihanku-mobile">Pelatihanku</a>
                    <div class="line-horizontal width-100"></div>
                </li>
            @endif
            @foreach($parents as $parent)
                <li class="active">
                    <a href="{{ url('/learn-method/detail/'.$parent->slug) }}">{{ $parent->title }}</a>
                </li>
            @endforeach
            <li class="active">
                <a href="https://shop.marketeers.com" target="blank">
                    <li><img src="{{ url('assets/web/icons/mshop.svg') }}" alt="" class="img-fluid"></li>
                </a>
            </li>
        </ul>
    </div>
</nav>

@push('script')
    <script>
        $("#button-home").click(function()
        {
            window.location.href = "{{ url('/') }}";
        });

        $("#button-register").click(function()
        {
            window.location.href = "{{ url('/user/register') }}";
        });

        $("#button-register-mobile").click(function()
        {
            window.location.href = "{{ url('/user/register') }}";
        });

        $("#button-login").click(function()
        {
            window.location.href = "{{ url('/user/login') }}";
        });

        $("#button-login-mobile").click(function()
        {
            window.location.href = "{{ url('/user/login') }}";
        });

        $("#button-logout").click(function()
        {
            {{-- window.location.href = "{{ url('/user/logout') }}"; --}}
            window.location.href = "{{ config('services.url_lms') }}/#/logout";
        });

        $('#beranda-mobile').on("click", function()
            {
                window.location.href = "{{ url('/') }}";
            });
        // Login with Auth User
        @if(Auth::guard('user')->check())

            $("#link-profile").click(function()
            {
                window.location.href = "{{ config('matrix.url') }}" + "/user/show/" + "{{ Auth::guard('user')->user()->user_id_matrix }}";
            });

            $('#pelatihanku-mobile').on("click", function()
            {
                window.location.href = "{{ url('/pelatihanku') }}";
            });

            $("#assign-button-mobile").on( "click", function()
            {
                window.location.href = "{{ url('/assign-product') }}";
            });

            $("#profile-button-mobile").on( "click", function()
            {
                window.location.href = "{{ config('matrix.url') }}" + "/user/show/"+ "{{ Auth::guard('user')->user()->user_id_matrix }}";
            });


            $("#order-button-mobile").on( "click", function()
            {
                window.location.href = "{{ url('/order-history') }}";
            });

            $("#link-logout-mobile").click(function()
            {
                {{-- window.location.href = "{{ url('/user/logout') }}"; --}}
                window.location.href = "{{ config('services.url_lms') }}/#/logout";
            });

            $("#my-course-button").click(function()
            {
                window.location.href = "{{ config('matrix.url') }}" + "/enrolled_dashboard";
            });

        @else
            $("#my-course-button").click(function()
            {
                window.location.href = "{{ url('/user/login') }}";
            });

            $("#button-register-register").click(function()
            {
                window.location.href = "{{ url('/user/register') }}";
            });

        @endif
    </script>
@endpush
