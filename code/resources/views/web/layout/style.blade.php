<link rel="stylesheet" href="{{ url('/assets/web/bootstrap/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ url('/assets/web/css/style-sidebar.css') }}">
<link rel="stylesheet" href="{{ url('/assets/web/css/jquery.mCustomScrollbar.min.css') }}">
<link rel="stylesheet" href="{{ url('/assets/web/css/fonts.css') }}">

<link rel="stylesheet" href="{{ url('/assets/web/js/slick-1.8.1/slick/slick.css') }}">
<link rel="stylesheet" href="{{ url('/assets/web/js/slick-1.8.1/slick/slick-theme.css') }}">

{{--  <script defer src="{{ url('/assets/web/js/solid.js') }}"></script>
<script defer src="{{ url('/assets/web/js/fontawesome.js') }}"></script>  --}}

<!-- Daterangepicker CSS -->
<link href="{{ url('/assets/web/vendors/daterangepicker/daterangepicker.css') }}" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css'>

{{--  datatable  --}}
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<link rel="stylesheet" type="text/css" href="{{ url('/assets/web/css/sytle-rating-stars.css') }}">

<link rel="stylesheet" href="{{ url('/assets/web/css/style-desktop.css') }}">
<link rel="stylesheet" href="{{ url('/assets/web/css/style-tablet.css') }}">
<link rel="stylesheet" href="{{ url('/assets/web/css/style-phone.css') }}">
<style>
.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 160px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}
</style>
