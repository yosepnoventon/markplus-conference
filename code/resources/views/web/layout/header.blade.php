<section class="section-header new-header">
    <div class="header-web width-100">
        <div class="new-container width-100">
            <div class="new-container-inner">
                <div class="box-header-table width-100">
                    <div class="box-header-top-inner">
                        <div class="box-header-top width-100">
                            <div class="box-logo">
                                    <a href="{{ url('/') }}"><img src="{{ url('/assets/web/img/logo.png') }}" class="img-fluid" alt="Responsive image"></a>
                            </div>
                            <div class="box-search">
                                    <div class="input-group mb-3">
                                        @if(request()->segment(1) == "courses")
                                            <input type="text" class="form-control" id="form-search" placeholder="Apa yang ingin Anda pelajari?" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                            <div class="input-group-append">
                                                <button class="btn" type="button" id="button-search-course">
                                                    <img src="{{ url('assets/web/icons/mirror-search.svg') }}" alt="" class="img-fluid">
                                                </button>
                                            </div>
                                        @else
                                            <input type="text" class="form-control" id="form-search-redirect" placeholder="Apa yang ingin Anda pelajari?" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                            <div class="input-group-append">
                                                <button class="btn" type="button" id="search-course">
                                                    <img src="{{ url('assets/web/icons/mirror-search.svg') }}" alt="" class="img-fluid">
                                                </button>
                                            </div>
                                        @endif
                                    </div>
                            </div>
                            <div class="box-action">
                                <!-- jika sudah login Auth User -->
                                @if(Auth::guard('user')->check())
                                <a href="javascript:void(0)" id="pelatihanku">Pelatihanku</a>
                                <div class="after-login box-user-info flex-between">
                                    <img src=" {{ url('assets/web/icons/user-icon.svg') }} " alt="" class="img-fluid">
                                    <div class="dropdown">
                                        <button class="btn dropdown-toggle" type="button" id="user-call" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{ Auth::guard('user')->user()->first_name }}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="user-call" id="user-show">
                                        <a class="dropdown-item" href="javascript:void(0)" id="assign-button">Data Peserta</a>
                                        {{-- <a class="dropdown-item" href="javascript:void(0)" id="profile-button">Akun</a> --}}
                                        <a class="dropdown-item" href="javascript:void(0)" id="order-button">Riwayat Order</a>
                                        <a class="dropdown-item" href="javascript:void(0)" id="logout-button">Keluar
                                            <img src=" {{ url('assets/web/icons/arrow-right.svg') }} " alt="" class="img-fluid">
                                        </a>
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="subscribe-middle">
                                    <ul>
                                        <li class="dropdown"><a href="javascript:void(0)">{{ Auth::guard('user')->user()->nickname }}</a>
                                            <ul class="isi-dropdown">
                                                <li><a href="javascript:void(0)" id="profile-button">Profil</a></li>
                                                <li><a href="javascript:void(0)" id="epass-button">E-Pass</a></li>
                                                <li><a href="javascript:void(0)" id="logout-button">Keluar</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div> --}}
                                @else
                                    <!-- jika belum login sso -->
                                    <a href="javascript:void(0)" id="pelatihanku" style="visibility: hidden;">Pelatihanku</a>
                                    <a href="javascript:void(0)" id="login">Masuk</a>
                                    <button type="button" class="btn btn-warning" id="register">Daftar</button>
                                @endif
                                <!-- tempat tampungan cart -->
                                <div class="box-cart">
                                    <a href="{{ url('cart') }}">
                                        <img src="{{ url('assets/web/icons/cart.svg') }}" alt="" class="img-fluid">
                                        <div id="box-cart-count">
                                            @if(Cart::getContent()->count() > 0)
                                                <div class="box-cart-count">
                                                    <p id="cart-item">{{ Cart::getContent()->count() }}</p>
                                                </div>
                                            @endif
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            </div>
        </div>
        <div class="new-container width-100">
            <div class="box-new-header-menu width-100">
                <div class="new-container-inner">
                    <div class="header-menu width-100">
                        <ul>
                            @foreach($parents as $parent)
                                <a href="{{ url('learn-method/detail/'.strtolower($parent->title)) }}">
                                    <li>{{ ucwords(strtolower($parent->title),"-") }}</li>
                                </a>
                            @endforeach
                            {{-- Ensiklopediamu --}}
                            <a href="javascript:void(0)" style="display: none">
                                <li>Ensiklopediamu</li>
                            </a>
                            <a href="https://shop.marketeers.com" target="blank">
                                <li><img src="{{ url('assets/web/icons/mshop.svg') }}" alt="" class="img-fluid"></li>
                            </a>

                        </ul>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <div class="header-mobile width-100">
        <div class="new-container width-100">
            <div class="new-container-inner">
                <div class="box-header-top flex-between width-100" id="header-top">
                    <div class="width-50 flex-start">
                        <div class="box-togleM">
                            <button type="button" id="sidebarCollapse" class="btn btn-info">
                                <img src=" {{ url('assets/web/icons/icon-burger.svg') }} " alt="" class="img-fluid">
                            </button>
                        </div>
                        <div class="box-logo">
                                <a href="{{ url('/') }}"><img src="{{ url('/assets/web/img/logo.png') }}" class="img-fluid" alt="Responsive image"></a>
                        </div>
                    </div>
                    <div class="box-action">
                        <div class="box-user-info flex-end width-100">
                            <div class="box-icon flex-center">
                                <img src=" {{ url('assets/web/icons/icon-mirror-white.svg') }} " alt="" class="img-fluid" id="search-show">
                            </div>
                            <div class="box-cart flex-center">
                                <a href="{{ url('cart') }}">
                                    <img src="{{ url('assets/web/icons/cart.svg') }}" alt="" class="img-fluid">
                                    <div id="box-cart-count2">
                                        @if(Cart::getContent()->count() > 0)
                                            <div class="box-cart-count">
                                                <p id="cart-item">{{ Cart::getContent()->count() }}</p>

                                            </div>
                                        @endif

                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-search width-100" id="header-search">
                    <div class="width-100 flex-between">
                        <div class="mirror-icon flex-center">
                            <img src="{{ url('assets/web/icons/mirror-search.svg') }}" alt="" class="img-fluid img-search">
                        </div>
                        @if(request()->segment(1) == "courses")
                            <input type="text" class="form-control" id="form-search2" placeholder="Apa yang ingin Anda pelajari?">
                        @else
                            <input type="text" class="form-control" id="form-search-redirect2" placeholder="Apa yang ingin Anda pelajari?">
                        @endif
                        <button class="btn btn-light" type="button" id="btn-close">
                            <img src="{{ url('assets/web/icons/icon-close.svg') }}" alt="" class="img-fluid">
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@push('script')
    <script>



        $("#user-call").click(function() {
            $("#user-show").slideToggle();
        });

        $("#search-course").on( "click", function()
        {
            var search = $('#form-search-redirect').val();
            window.location.href = "{{ url('/courses') }}?search="+search+'&category=&topik=';
        });

        $("#form-search-redirect").keyup(function()
        {
            if (event.keyCode === 13)
            {
                var search           = $(this).val();
                window.location.href = "{{ url('/courses') }}?search="+search+'&category=&topik=';
            }
        });

        $('#form-search-redirect2').keyup(function(e){
            // action enter
            if(e.keyCode == 13)
            {
                var search           = $(this).val();
                window.location.href = "{{ url('/courses') }}?search="+search+'&category=&topik=';
            }
        });

        // $("#button-subscribe").on( "click", function()
        // {
        //     window.location.href = "{{ url('/subscriptions') }}";
        // });

        $("#home-button").on( "click", function()
        {
            window.location.href = "{{ url('/') }}";
        });

        $("#login").on( "click", function()
        {
            window.location.href = "{{ url('/user/login') }}";
        });

        $("#register").on( "click", function()
        {
            window.location.href = "{{ url('/user/register') }}";
        });
        // Login with Auth User
        @if(Auth::guard('user')->check())

            $("#mycourse-button").on( "click", function()
            {
                window.location.href = "{{ config('matrix.url') }}" + "/enrolled_dashboard";
            });

            $('#pelatihanku').on("click", function()
            {
                window.location.href = "{{ url('/pelatihanku') }}";
            });

            $("#assign-button").on( "click", function()
            {
                window.location.href = "{{ url('/assign-product') }}";
            });

            $("#profile-button").on( "click", function()
            {
                window.location.href = "{{ config('matrix.url') }}" + "/user/show/"+ "{{ Auth::guard('user')->user()->user_id_matrix }}";
            });

            $("#order-button").on( "click", function()
            {
                window.location.href = "{{ url('/order-history') }}";
            });

            $("#epass-button").on( "click", function()
            {
                window.location.href = "{{ url('/e-pass') }}";
            });

            $("#logout-button").on( "click", function()
            {
                {{-- window.location.href = "{{ url('/user/logout') }}"; --}}
                window.location.href = "{{ config('services.url_lms') }}/#/logout";
            });

        @else

            $("#mycourse-button").on( "click", function()
            {
                window.location.href = "{{ url('/user/login') }}";
            });

            $('#pelatihanku').on("click", function()
            {
                window.location.href = "{{ url('/user/login') }}";
            });


            $("#profile").on( "click", function()
            {
                window.location.href = "{{ url('/user/login') }}";
            });

        @endif






    </script>
@endpush
