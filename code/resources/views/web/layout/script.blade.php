 {{-- <script src="https://code.jquery.com/jquery-1.11.0.min.js"></script> --}}
<script src="{{ url('/assets/web/js/jquery-3.5.1.min.js') }}"></script>

 <!-- Popper.JS -->
<script src="{{ url('/assets/web/js/popper.min.js') }}"></script>

<script src="{{ url('/assets/web/bootstrap/js/bootstrap.js') }}"></script>
<script src="{{ url('/assets/web/js/slick-1.8.1/slick/slick.js') }}"></script>

<script defer src="{{ url('/assets/web/js/solid.js') }}"></script>
<script defer src="{{ url('/assets/web/js/fontawesome.js') }}"></script>

{{--  <!-- jQuery CDN - Slim version (=without AJAX) -->
<script src="{{ url('/assets/web/js/jquery-3.3.1.slim.min.js') }}"></script>  --}}



{{--  <!-- Bootstrap JS -->
<script src="{{ url('/assets/web/bootstrap/js/bootstrap.min.js') }}"></script>  --}}

<!-- jQuery Custom Scroller CDN -->
<script src="{{ url('/assets/web/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>

<!-- Alert Toast -->
<script src="{{ url('/assets/web/js/toastr-2.1.3.min.js') }}"></script>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script> --}}

<script src="{{ url('/assets/web/js/script-markplus.js') }}"></script>


<script src="{{ url('/assets/web/vendors/moment/min/moment.min.js') }}"></script>
<script src="{{ url('/assets/web/vendors/daterangepicker/daterangepicker.js') }}"></script>

<!-- Sweet Alert -->
<script src="{{ url('/assets/web/js/sweetalert2.min.js') }}"></script>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.js"></script> --}}

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<script src="{{ url('/assets/web/js/jquery.star-rating.js') }}"></script>

<!-- Widgetwhatsapp -->
<script async data-id="49115" src="https://cdn.widgetwhats.com/script.min.js"></script>
{{--
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-155852833-1"></script>
<!-- Google Analytic -->
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-155852833-1');
</script>
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '162206304465684');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=162206304465684&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code --> --}}

