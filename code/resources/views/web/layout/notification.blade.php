@if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch(type)
    {
        case 'info':
        Swal.fire({
            title: "Info",
            text: "{{ Session::get('message') }}",
            type: "info",
            allowOutsideClick: false,
            showConfirmButton: false,
            timer: 2000,
        });
        break;

        case 'warning':
        Swal.fire({
            title: "Warning",
            text: "{{ Session::get('message') }}",
            type: "warning",
            allowOutsideClick: false,
            showConfirmButton: false,
            timer: 2000,
        });
        break;

        case 'success':
        Swal.fire({
            title: "Success",
            text: "{{ Session::get('message') }}",
            type: "success",
            allowOutsideClick: false,
            showConfirmButton: false,
            timer: 2000,
        });
        break;

        case 'error':
        Swal.fire({
            title: "Error",
            text: "{{ Session::get('message') }}",
            type: "error",
            allowOutsideClick: false,
            showConfirmButton: false,
            timer: 2000,
        });
        break;
    }
@endif
