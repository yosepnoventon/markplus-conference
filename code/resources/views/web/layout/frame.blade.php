<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>MarkPlus Institute @yield('title')</title>
    <link rel="icon" href="{{ url('/assets/favicon.png') }}" type="image/png">

    @include('web.layout.style')

    @stack('style')

    <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css"/>

    <script defer src="{{ url('/assets/web/js/solid.js') }}"></script>
    <script defer src="{{ url('/assets/web/js/fontawesome.js') }}"></script>

</head>

@if(request()->segment(1) == '')
    <body>
        <div class="box-wrapper">
            <div class="wrapper">
                @elseif(request()->segment(1) == 'detail')
                    <body id="background-topic-personal-selling">
                @elseif(request()->segment(1) == 'about-us')
                    <body id="background-acbout-us">
                @elseif(request()->segment(1) == 'contact-us')
                    <body id="background-contact-us">
                @elseif(request()->segment(1) == 'faq')
                    <body id="background-faq">
                @elseif(request()->segment(2) == 'profile')
                    <body id="background-account">
                @else
                    <body>
                @endif
                <div class="overlay" id="overlay-global"></div>
                <div class="overlay-header" id="overlay-header"></div>
                 <!-- layout buat menu sebelah kiri -->
                @include('web.layout.menu')
                 <!-- layout buat menu header -->
                @include('web.layout.header')


                <div id="content">
                        {{-- <div class="public-title-menu">
                            <h3>@yield('title-header')</h3>
                            <h3>HOME E-LEARNING</h3>
                        </div> --}}
                    @yield('content')
                </div>


                @if(request()->segment(1) != 'payment-success' && request()->segment(1) != 'e-pass')
                    @include('web.layout.footer')
                @endif

                @if(request()->segment(1) == '')
            </div>
        </div>
@endif

@include('web.layout.script')

    {{-- <script type="text/javascript">
        $(document).ready(function()
        {
            @include('web.layout.notification')

            $("#sidebar").mCustomScrollbar({
                theme: "minimal"
            });

            $('#dismiss, overlay-global').on('click', function() {
                $('#sidebar').removeClass('active');
                $('.overlay-global').removeClass('active');
            });

            $('#sidebarCollapse').on('click', function() {
                $('#sidebar').addClass('active');
                $('.overlay-global').addClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });
        });
    </script> --}}

@stack('script')
</body>

</html>
