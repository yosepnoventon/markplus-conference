@extends('web.layout.form.frame')

@section('title', '| Register')

@section('content')
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login">
                <div class="login100-form validate-form">
                    <span class="login100-form-title">{{ __('Success') }}</span>
                    
                    <div class="container-login100-form-btn">
                        <label>Success Login Markplus Conference</label>
                    </div>
                    <div class="text-center p-t-20">
                    <a class="txt2" href="{{ url('/logout') }}">
                        {{ __('Logout') }}
                        <i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
                    </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
<script>
	$(document).ready(function(){
        // Login with Auth User
        var login = "{{ Auth::guard('login')->check() ? Auth::guard('login')->user()->id : '' }}";
        if(login)
        {
            var login_array = new Array();
            var data_ = new Object();
            data_ = {
                "name"          :  "{{ Auth::guard('login')->user()->name }}",
                "email"         : "{{ Auth::guard('login')->user()->email }}",
                "phone"         : "{{ Auth::guard('login')->user()->phone }}",
                "code_voucher"  : "{{ Auth::guard('login')->user()->code_voucher }}",
                "agency_name"   : "{{ Auth::guard('login')->user()->agency_name }}",
            };
            sessionStorage.setItem("currentloggedin", JSON.stringify(data_));
            logins = JSON.parse(sessionStorage.getItem("currentloggedin"));
            console.log('success', logins.name);
        }
        else
        {
            sessionStorage.removeItem("currentloggedin");
            console.log('failed', login);
        }
    });
</script>
<script>
    @include('web.layout.form.notification')
</script>
@endpush
