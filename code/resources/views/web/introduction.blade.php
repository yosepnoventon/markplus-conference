@extends('web.layout.form.frame')

@section('title', '| Intro')

@section('content')
    <div class="preload">
        <img src="{{ url('assets/form/img/preloader.gif') }}" alt="airplane" class="airplane">

        <!-- <video loop muted playsinline class="preload-video" id="myPreload">
            <source src="https://media.giphy.com/media/l3vRbMRGhSmCxWj72/giphy.mp4" type="video/mp4">
        </video> -->
    </div>
    <div class="page-conference introduction width-100">
        <div class="new-container width-100">
            <div class="intro-video width-100">
                <button type="button" class="button" id="btn-sound">
                    <img src="{{ url('assets/form/img/mute.png') }}" alt="sound-mute" class="sound-mute">
                    <img src="{{ url('assets/form/img/sound-on.png') }}" alt="sound-on" class="sound-on">
                </button>
                <video autoplay id="myVideo">
                    <source src="{{ url('assets/form/img/introduction-1.mp4') }}" type="video/mp4">
                </video>
                <div class="box-btn-skip flex-between width-100">
                    <button type="button" class="btn btn-outline-secondary" id="btn">Replay <i class="fa fa-repeat"></i></button>
                    <a href="{{ url('virtual-reality') }}" class="btn btn-outline-secondary">Skip <i class="fa fa-forward"></i></a>
                </div>
               
            </div>
        </div>
    </div>
   
   
@endsection

@push('script')

<script>
	$(document).ready(function(){
        sessionStorage.setItem("base_url", JSON.stringify("{{ url('/') }}"));
        document.cookie = "base_url="+JSON.stringify("{{ url('/') }}");
        // console.log(JSON.parse(sessionStorage.getItem("base_url")));
        sessionStorage.setItem("csrf-token", "{{ csrf_token() }}");
        document.cookie = "csrf-token="+sessionStorage.getItem("csrf-token");
        // console.log(sessionStorage.getItem("csrf-token"));
        // Login with Auth User
        var login = "{{ Auth::guard('login')->check() ? Auth::guard('login')->user()->id : '' }}";
        if(login)
        {
            var login_array = new Array();
            var data_ = new Object();
            data_ = {
                "name"          :  "{{ Auth::guard('login')->user()->name }}",
                "email"         : "{{ Auth::guard('login')->user()->email }}",
                "phone"         : "{{ Auth::guard('login')->user()->phone }}",
                "code_voucher"  : "{{ Auth::guard('login')->user()->code_voucher }}",
                "agency_name"   : "{{ Auth::guard('login')->user()->agency_name }}",
            };
            sessionStorage.setItem("currentloggedin", JSON.stringify(data_));
            logins = JSON.parse(sessionStorage.getItem("currentloggedin"));
            // console.log('success', logins.name);
        }
        else
        {
            sessionStorage.removeItem("currentloggedin");
            // console.log('failed', login);
        }
    });
    
        var button = document.querySelector("#btn-sound");
        var vid = document.querySelector("#myVideo");
        // var soundMute = document.querySelector(".sound-mute");
        // var soundOn = document.querySelector(".sound-on");


        vid.oncanplaythrough = function() {
            vid.muted = true;
            vid.play();
        }
        $(button).click(function() {
            var clicks = $(this).data('clicks');
            if (clicks) {
                // button.innerHTML = 'Enable Sound'
                $(".sound-mute").css('display', 'block');
                $(".sound-on").css('display', 'none');
                vid.muted = true;
            } else {
                // button.innerHTML = 'Disable Sound'
                $(".sound-mute").css('display', 'none');
                $(".sound-on").css('display', 'block');

                vid.muted = false;
            }
            $(this).data("clicks", !clicks);
        });

        window.addEventListener('load', () => {
            const preload = document.querySelector('.preload');
            // const vidPreload = document.querySelector('#myPreload');
            // vidPreload.play();
            preload.classList.add('preload-finish');
        })
        vid.addEventListener('ended',myHandler,false);
        function myHandler(e) {
            // What you want to do after the event
            window.location.replace("{{ url('virtual-reality') }}");
        }
   
    btn.addEventListener("click",function(){
        vid.currentTime = 0;
    })
</script>
{{-- <script>
    @include('web.layout.form.notification')
</script> --}}
@endpush
