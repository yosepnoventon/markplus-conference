@extends('web.layout.frame')

@section('title', '| My Profile')

@section('content')

<section id="section1-account">
    <div class="row justify-content-sm-center">

        @include('web.auth.profile.menu')
    
        <div id="account-page" class="col-sm-8">
            <div class="label-title">
                <h4>ACCOUNT</h4>
            </div>
            <div class="label-detail">
                <h5>First Name</h5>
                <p>{{ $user->first_name }}</p>
            </div>
            <div class="label-detail">
                <h5> Nama Belakang</h5>
                <p>{{ $user->last_name }}</p>
            </div>
            <div class="label-detail">
                <h5>Tanggal Lahir</h5>
                @php
                $date = date_create($user->dob);
                @endphp
                <p>{{ date_format($date, "d-M-Y") }}</p>
            </div>
            <div class="label-detail">
                <h5>Nomor Telepon</h5>
                <p>{{ $user->phone }}</p>
            </div>
            <div class="label-detail">
                <h5>Email</h5>
                <p>{{ $user->email }}</p>
            </div>
            <div class="label-bottom">
                <button onclick="editprofile()" type="button" class="btn btn-warning">UBAH PROFILE</button>
            </div>
        </div>
    </div>
</section>

@endsection

@push('script')
    <script>
        function editprofile()
        {
            window.location.href = "{{ url('/user/edit-profile') }}";
        }
    </script>
@endpush