@extends('web.layout.frame')

@section('title', '| Review')

@section('content')

<section id="section1-account">
    <div class="row justify-content-sm-center">

        @include('web.auth.profile.menu')

        <div class="col-sm-8">
            <div class="label-title">
                <h4>RATE NOW</h4>
            </div>
            <div class="card-deck" style="width: 30%;">
                <div class="card">
                    <img src="{{ url('/upload/instructor/'.$instructor->image) }}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h3 class="card-title font-weight-bold">{{ $instructor->name }}</h3>
                        {!! $instructor->preview_text !!}
                    </div>
                </div>
            </div>
            <br>
            <form method="POST" action="{{ url('/user/review/'.$slug.'/store') }}">
                @csrf
                <div class="box-rate">
                    <div class="box-start">
                        <div class="my-rating"></div>
                    </div>
                    <br>
                    <input hidden type="text" name="stars" id="stars">
                    <h4>WRITE YOUR COMMENT</h4>
                    <textarea rows="7" cols="55" name="review"></textarea>
                </div>
                <br>
                <div class="box-rate">
                    <button type="submit" class="btn btn-warning ">SUBMIT</button>
                </div>
            </form>
        </div>

    </div>
</section>

@endsection

@push('script')
    <script>
        $(".my-rating").starRating({
            useFullStars: true,
            totalStars: 5,
            strokeColor: '#894A00',
            activeColor: '#894A00',
            disableAfterRate: false,
            callback: function(currentRating, $el)
            {
                $("#stars").val(currentRating);
            },
        });
    </script>
@endpush