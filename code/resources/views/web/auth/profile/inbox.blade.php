@extends('web.layout.frame')

@section('title', '| My Certificate')

@section('content')

<section id="section1-account">
    <div class="row justify-content-sm-center">

        @include('web.auth.profile.menu')

        <div id="inbox-page" class="col-sm-8">
            <div class="label-title">
                <h4>KOTAK MASUK</h4>
                <P hidden>History</P>
            </div>
        
            <div class="box-inbox">
                <div class="box-list-inbox">
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="materialChecked2" checked>
                        <label class="form-check-label" for="materialChecked2">Material checked</label>
                    </div>
                    <div class="list-title">
                        <p>CS Markplus</p>
                    </div>
                    <div class="list-description">
                        <p>Kami sangat senang Anda memutuskan untuk mencoba ...</p>
                    </div>
                    <div class="list-action">
                        D A S
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

@endsection