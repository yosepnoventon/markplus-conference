@extends('web.layout.frame')

@section('title', '| Change Password')

@section('content')

<section id="section1-account">
    <div class="row justify-content-sm-center">

        @include('web.auth.profile.menu')

        <div id="changepassword-page" class="col-sm-8">
            <div class="box-account ">
                <div class="label-title ">
                    <h4>UBAH PASSWORD</h4>
                </div>
                @if ($errors->any())
                <ul class="alert alert-danger" style="margin-top:15px">
                    @foreach ($errors->all() as $error)
                    <li style="margin-left: 10px">{{ $error }}</li>
                    @endforeach
                </ul>
                @endif
                <form method="POST" action="{{ url('/user/profile/change-password') }}" role="form" class="form-horizontal">
                    @csrf
                    <div class="label-detail ">
                        <h5>Password Lama</h5>
                        <div class="form-group">
                            <input type="password" class="form-control" name="old_password">
                        </div>
                    </div>
        
                    <div class="label-detail ">
                        <h5>Password Baru</h5>
                        <div class="form-group">
                            <input type="password" class="form-control" name="new_password">
                        </div>
                    </div>
        
                    <div class="label-detail ">
                        <h5>Ulangi Password Baru</h5>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password_confirmation">
                        </div>
                    </div>
        
                    <div class="label-bottom">
                        <button type="submit" class="btn btn-warning ">SELESAI</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</section>

@endsection