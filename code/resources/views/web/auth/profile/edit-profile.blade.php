@extends('web.layout.frame')

@section('title', '| Edit Profile')

@section('content')

<section id="section1-account">
    <div class="row justify-content-sm-center">

        @include('web.auth.profile.menu')

        <div id="edit-profile-page" class="col-sm-8">
            <div class="box-account">
                @if ($errors->any())
                <ul class="alert alert-danger" style="margin-top:15px">
                    @foreach ($errors->all() as $error)
                    <li style="margin-left: 10px">{{ $error }}</li>
                    @endforeach
                </ul>
                @endif
        
                <form class="form-horizontal" method="POST" action="{{ url('/user/profile/update') }}">
                    @csrf
                    <div class="label-title">
                        <h4>PROFILE SAYA</h4>
                    </div>
                    <div class="label-detail">
                        <h5> Nama Depan</h5>
                        <div class="form-group">
                            <input type="text" class="form-control" name="first_name" value="{{ $user->first_name }}">
                        </div>
                    </div>
                    <div class="label-detail">
                        <h5> Nama Belakang</h5>
                        <div class="form-group">
                            <input type="text" class="form-control" name="last_name" value="{{ $user->last_name }}">
                        </div>
                    </div>
                    <div class="label-detail">
                        <h5>Tanggal Lahir</h5>
                        <div class="form-group">
                            <input type="text" class="form-control" name="dob" id="dob" value="">
                        </div>
                    </div>
                    <div class="label-detail">
                        <h5>Nomor Telepon</h5>
                        <div class="form-group">
                            <input type="text" class="form-control" name="phone" value="{{ $user->phone }}">
                        </div>
                    </div>
                    <div class="label-detail">
                        <h5>Email</h5>
                        <div class="form-group">
                            <input type="text" class="form-control" name="email" value="{{ $user->email }}">
                        </div>
                    </div>
                    <div class="label-bottom">
                        <button type="submit" class="btn btn-warning">SIMPAN</button>
                    </div>
                </form>
            </div>
            <div class="label-bottom">
                <button onclick="changepassword()" type="button" class="btn button-change-password">UBAH PASSWORD</button>
            </div>
        </div>
    </div>
</section>

@endsection

@push('script')
    <script>
        var date;
    
        @if(isset($user))
            @if($user->dob != "")
                date = "{{ $user->dob }}";
            @else
                date = "{{ date('Y-m-d') }}";
            @endif
        @endif

        $('#dob').daterangepicker(
        {
            locale:{
                format: 'YYYY-MM-DD'
            },
            singleDatePicker: true,
            showDropdowns: true,
            minYear: 1990,
            startDate: date,
        }, function(start, end, label) 
        {
            $('#dob').val(start.format('YYYY-MM-DD'));
        });

        function changepassword()
        {
            window.location.href = "{{ url('/user/change-password') }}";
        }
    </script>
@endpush