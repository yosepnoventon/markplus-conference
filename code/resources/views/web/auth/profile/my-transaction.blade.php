@extends('web.layout.frame')

@section('title', '| My Transaction')

@section('content')

<section id="section1-account">
    <div class="row justify-content-sm-center">

        @include('web.auth.profile.menu')

        <div id="mytransaction-page" class="col-sm-8">
            <div class="label-title">
                <h4>MY TRANSACTION</h4>
                <P hidden>History</P>
            </div>
            <div class="box-button">
                <button class="btn btn-print" id="history-transaction">HISTORY OF TRANSACTION</button>
                <button class="btn btn-print" id="recent-course">RECENT COURSE</button>
            </div>
            <div id="results">
            </div>
        </div>

    </div>
</section>

@endsection

@push('script')
    <script>
        var type;

        $("#history-transaction").on( "click", function()
        {
            type = 1;

            $.ajax({
                type: "POST",
                url: "{{ route('request-history-transaction') }}",
                data: {
                    type: type,
                    cache: "false",
                    _token: '{{ csrf_token() }}'
                },
                complete: function(datas)
                {
                    $("#results").html(datas.responseText);
                }
            });
        });

        $("#recent-course").on( "click", function()
        {
            type = 2;

            $.ajax({
                type: "POST",
                url: "{{ route('request-current-course') }}",
                data: {
                    type: type,
                    cache: "false",
                    _token: '{{ csrf_token() }}'
                },
                complete: function(datas)
                {
                    
                    //$("#results").html(datas.responseText);
                }
            });
        });

        $('#history-transaction').trigger('click');
    </script>
@endpush