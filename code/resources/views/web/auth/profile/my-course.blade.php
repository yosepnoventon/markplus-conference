@extends('web.layout.frame')

@section('title', '| My Course')

@section('content')

<section id="section1-account">
    <div class="row justify-content-sm-center">

        @include('web.auth.profile.menu')

        <div id="mycourse-page" class="col-sm-8">
            <div class="label-title">
                <h4>MY COURSE</h4>
                <P>E-Learning</P>
            </div>
            @foreach($data['courses'] as $course)
                
                <div class="card-deck">
                    <div class="card">
                        <img src="{{ $course->image }}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h3 class="card-title font-weight-bold">{{ $course->title }}</h3>
                            <p>{{ $course->learn_method }}</p>
                            <p>{{ $course->previewtext }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <div class="col-sm-3"></div>
        <div id="mycourse-page" class="col-sm-8">
            <div class="label-title">
                <h4>MY COURSE</h4>
                <P>Omni Learning With Instuctor</P>
            </div>
            @foreach($data['instructors'] as $instructor)
                <a href="{{ url('/user/review/'.$instructor->slug) }}">
                    <div class="card-deck">
                        <div class="card">
                            <img src="{{ $instructor->image }}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h3 class="card-title font-weight-bold">{{ $instructor->name }}</h3>
                                <p>{{ $instructor->preview_text }}</p>
                            </div>
                        </div>
                    </div>
                </a>
            @endforeach
        </div>

    </div>
</section>

@endsection
