<div class="col-sm-3">
    <div class="myprofile-picture">
        <img src="{{ url('/assets/web/img/user.png') }}" class="img-fluid rounded-circle" alt="Responsive image">
    </div>
    <div class="myprofile-camera">
        <img src="{{ url('/assets/web/img/camera.jpg') }}" class="img-fluid rounded-circle" alt="Responsive image">
    </div>

    <div class="myprofile-bottom">
        <button onclick="account()" class="btn btn-warning {{ request()->segment(2) == 'profile' ? 'active' : '' }}">ACCOUNT</button>
        <button class="btn btn-warning" onclick="mycourse()">MY COURSE</button>
        <button class="btn btn-warning" onclick="mycertificate()">MY CERTIFICATE</button>
        <button class="btn btn-warning" onclick="mytransaction()">MY TRANSACTION</button>
        <button class="btn btn-warning" onclick="inbox()">INBOX</button>

    </div>
</div>

@push('script')
    <script>
        function account()
        {
            window.location.href = "{{ url('/user/profile') }}";
        }

        function mycourse()
        {
            window.location.href = "{{ url('/user/my-course') }}";
        }

        function mycertificate()
        {
            window.location.href = "{{ url('/user/my-certificate') }}";
        }

        function mytransaction()
        {
            window.location.href = "{{ url('/user/my-transaction') }}";
        }

        function inbox()
        {
            window.location.href = "{{ url('/user/inbox') }}";
        }
    </script>
@endpush
