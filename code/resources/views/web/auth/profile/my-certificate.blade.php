@extends('web.layout.frame')

@section('title', '| My Certificate')

@section('content')

<section id="section1-account">
    <div class="row justify-content-sm-center">

        @include('web.auth.profile.menu')

        <div id="mycertificate-page" class="col-sm-8">
            <div class="label-title">
                <h4>MY CERTIFICATE</h4>
                <P hidden>History</P>
            </div>
        
            <div class="box-certificate">
                <img src="{{ url('/assets/web/img/logo.png') }}" class="img-fluid image-logo" alt="Responsive image">
                <h3>CERTIFICATE OF ACHIEVEMENT</h3>
                <img src="{{ url('/assets/web/img/user.png') }}" class="img-fluid rounded-circle image-user"
                    alt="Responsive image">
                <h1>BEST STUDENT</h1>
                <p>Has Successfully Completed The Subject</p>
                <h6>MarkPlus Student, Join Since 2018</h6>
            </div>
        
            <div class="label-btn-certificate">
                <button id="btn-view-certificate" type="button" class="btn btn-view-certificate">LIHAT RINCIAN</button>
            </div>
        
        </div>

    </div>
</section>

@endsection