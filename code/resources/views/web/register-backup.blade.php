@extends('web.layout.form.frame')

@section('title', '| Register')

@section('content')
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login">
            {{-- <div class="wrap-login100">
                <div class="login100-pic js-tilt" data-tilt>
                    <img src="{{ url('/assets/form/img/form.jpg') }}" alt="IMG">
                </div> --}}
                <div action="{{ url('/login') }}" method="POST" id="form-login" class="login100-form validate-form">
                    @csrf
                    <span class="login100-form-title">Register</span>
                    <div class="wrap-input100 validate-input {{ $errors->has('name') ? ' alert-validate' : '' }}" data-validate="Name is required">
                        <input class="input100" type="text" name="name" id="name" placeholder="Name" value="{{ old('name') }}">
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-user" aria-hidden="true"></i>
                        </span>
                    </div>
                    <div class="wrap-input100 validate-input {{ $errors->has('email') ? ' alert-validate' : '' }}" data-validate="Valid email is required: ex@abc.xyz">
                        <input class="input100" type="text" name="email" id="email" placeholder="Email" value="{{ old('email') }}">
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </span>
                    </div>
                    <div class="wrap-input100 validate-input" data-validate="Confirm Valid email is required: ex@abc.xyz">
                        <input class="input100" type="text" name="confirm_email" id="confirm_email" placeholder="Confirm Email" value="{{ old('confirm_email') }}">
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </span>
                    </div>
                    <div class="wrap-input100 validate-input {{ $errors->has('phone') ? ' alert-validate' : '' }}" data-validate="Phone is required">
                        <input class="input100 number" type="text" name="phone" id="phone" placeholder="phone" value="{{ old('phone') }}">
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                        </span>
                    </div>
                    <div class="wrap-input100 validate-input {{ $errors->has('code_voucher') ? ' alert-validate' : '' }}" data-validate="Voucher is required">
                        <input class="input100" type="text" name="code_voucher" id="code_voucher" placeholder="Voucher" value="{{ old('code_voucher') }}">
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-gift" aria-hidden="true"></i>
                        </span>
                    </div>
                    <div class="wrap-input100 validate-input {{ $errors->has('agency_name') ? ' alert-validate' : '' }}" data-validate="Company is required">
                        <input class="input100" type="text" name="agency_name" id="agency_name" placeholder="Company" value="{{ old('agency_name') }}">
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-building" aria-hidden="true"></i>
                        </span>
                    </div>
                    <div class="container-login100-form-btn">
                        <button type="button" class="login100-form-btn" id="popUpPayment">Submit</button>
                    </div>
                    @if ($errors->any())
                        <div class="text-center p-t-20">
                            @foreach ($errors->all() as $error)
                                <p style="color: red">
                                    {{ $error }}
                                </p>
                            @endforeach
                        </div>
                    @endif
                    <div class="text-center p-t-20">
                    <a class="txt2" href="{{ url('/login') }}">
                        Login
                        <i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
                    </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
<script>
    $(document).ready(function(){
        // Remove Session Login
        sessionStorage.removeItem("currentloggedin");
    });

    var input = $('.validate-input .input100');
    $("#popUpPayment").click(function()
    {
        var check = true;
		for (var i = 0; i < input.length; i++) {
			if (validate(input[i]) == false) {
				showValidate(input[i]);
				check = false;
			}
        }
        // if validation success
        if(check)
        {
            console.log('validation success');

            // $('#form-login').submit();
            var name            = $('#name').val();
            var email           = $('#email').val();
            var phone           = $('#phone').val();
            var code_voucher    = $('#code_voucher').val();
            var agency_name     = $('#agency_name').val();

            $.ajax({
                type: 'post',
                url: "{{ url('/register') }}",
                data: {
                    "name"          : name,
                    "email"         : email,
                    "phone"         : phone,
                    "code_voucher"  : code_voucher,
                    "agency_name"   : agency_name,
                    "_token"        : '{{ csrf_token() }}',
                },
                dataType: 'json',
                success: function (data)
                {
                    console.log(data);
                    sweetAlert(data.type, data.message);
                    if(data.type == "success")
                    {
                        // login(data.data);
                        window.location.href = "{{ url('/success') }}";
                    }
                    else
                    {
                        sweetAlert(data.type, data.message);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    setTimeout(() => {
                        $("#popUpPayment").click();
                    }, 1000);
                },
            });

            return
        }
        console.log('validation error');
        return
    });

    function login(data)
    {
        var login_array = new Array();
        var data_ = new Object();
        data_ = {
            "name"          : data.name,
            "email"         : data.email,
            "phone"         : data.phone,
            "code_voucher"  : data.code_voucher,
            "agency_name"   : data.agency_name,
        };
        sessionStorage.setItem("currentloggedin", JSON.stringify(data_));
    }

    $('.validate-form .input100').each(function () {
		$(this).focus(function () {
			hideValidate(this);
		});
    });
    
    function validate(input) {
		if ($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
			if ($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
				return false;
			}
		} else {
			if ($(input).val().trim() == '') {
				return false;
			}
		}
        if($(input).attr('id') == 'confirm_email')
        {
            if($('#email').val() != $('#confirm_email').val())
            {
                return false;
            }
        }
	}

	function showValidate(input) {
		var thisAlert = $(input).parent();
		$(thisAlert).addClass('alert-validate');
	}

	function hideValidate(input) {
		var thisAlert = $(input).parent();
		$(thisAlert).removeClass('alert-validate');
	}

    $(document).on('keydown','.phoneNumber', function(e)
    {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
            (e.keyCode == 187 && e.shiftKey === true) || 
                // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $(document).on('keydown','.number', function(e)
    {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    // function alert using sweetalert2
    function sweetAlert(type = "success", message)
        {
            Swal.fire({
                text: message,
                type: type,
                allowOutsideClick: false,
                showConfirmButton: true,
                // timer: 3000,
            });
        }
</script>
@endpush
