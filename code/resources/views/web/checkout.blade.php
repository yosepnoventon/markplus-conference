@extends('web.layout.frame')

@section('title', '| Home')

@section('content')
<div class="new-container-color width-100">
    <div class="new-container width-100">
        <div class="new-container-inner">

            <div class="box-shoping-cart checkout-cart flex-wrap-center  width-100">
                <div class="box-nav-back flex-between width-100">
                    <button type="button" class="btn btn-outline-dark">< Lanjutkan belanja</button>
                    <h3>Checkout</h3>
                </div>
                <div class="box-shopping-list flex-between width-100">
                    <div class="shoping-cart-list">
                        <div class="card width-100">
                            <div class="card-header flex-between">
                                <div class="custom-checkbox">
                                    <label for="customCheck0">Course yang dibeli</label>
                                </div>
                            </div>
                            <ul class="list-group list-group-flush">

                                <li class="list-group-item flex-between">
                                    <div class="box-cart-image">
                                        <img src="{{ url('assets/web/img/training-img.jpg') }}" alt="" class="img-fluid">
                                    </div>
                                    <div class="box-cart-description">
                                        <p>HANDLING CUSTOMER COMPLAINT: MENGUASAI TEORI KOMPREHENSIF DALAM...</p>
                                        <h5>Rp100.000</h5>
                                    </div>
                                </li>
                                <li class="list-group-item flex-between">
                                    <div class="box-cart-image">
                                        <img src="{{ url('assets/web/img/training-img.jpg') }}" alt="" class="img-fluid">
                                    </div>
                                    <div class="box-cart-description">
                                        <p>HANDLING CUSTOMER COMPLAINT: MENGUASAI TEORI KOMPREHENSIF DALAM...</p>
                                        <h5>Rp100.000</h5>
                                    </div>
                                </li>
                                <li class="list-group-item flex-between">
                                    <div class="box-cart-image">
                                        <img src="{{ url('assets/web/img/training-img.jpg') }}" alt="" class="img-fluid">
                                    </div>
                                    <div class="box-cart-description">
                                        <p>HANDLING CUSTOMER COMPLAINT: MENGUASAI TEORI KOMPREHENSIF DALAM...</p>
                                        <h5>Rp100.000</h5>
                                    </div>
                                </li>
                                <li class="list-group-item flex-between">
                                    <div class="box-cart-image">
                                        <img src="{{ url('assets/web/img/training-img.jpg') }}" alt="" class="img-fluid">
                                    </div>
                                    <div class="box-cart-description">
                                        <p>HANDLING CUSTOMER COMPLAINT: MENGUASAI TEORI KOMPREHENSIF DALAM...</p>
                                        <h5>Rp100.000</h5>
                                    </div>
                                </li>


                            </ul>
                        </div>
                    </div>
                    <div class="shoping-cart-summary">
                        <div class="summary-title width-100">
                        <h5>Ringkasan</h5>
                        </div>
                        <div class="summary-content flex-between width-100">
                            <p>Sub-total</p>
                            <h6>Rp600.000</h6>
                        </div>
                        <div class="summary-content flex-between width-100">
                            <p>Kode Promo</p>
                            <div class="input-group promo-code mb-3">
                                <input type="text" class="form-control" placeholder="MARKPLUS" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                <div class="input-group-append">
                                    <button class="btn" type="button">
                                        <img src="{{ url('assets/web/icons/cross-red.svg') }}" alt="" class="img-fluid">
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="summary-content flex-between width-100">
                            <p>Total</p>
                            <h6>Rp550.000</h6>
                        </div>
                        <div class="summary-content flex-between width-100">
                            <button type="button" class="btn btn-warning width-100">Bayar</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@endsection
