@extends('web.layout.form.frame')

@section('title', '| Login')

@section('content')
    <div class="page-conference conference width-100">
        <div class="new-container width-100">
            <div class="new-container-inner">
                <div class="conference-auth width-100">

                    <div class="img-logo flex-center width-100">
                        <img class="img-fluid img-contain" src="{{ url('/assets/form/img/assets-1.png') }}" alt="IMG">
                    </div>
                    <div class="img-text flex-center width-100">
                        <img class="img-fluid img-contain" src="{{ url('/assets/form/img/assets-2.png') }}" alt="IMG">
                    </div>
                    <div class="form-conference flex-wrap-between width-100">
                        <div class="width-50">
                            <img class="img-fluid img-contain" src="{{ url('/assets/form/img/assets-3.png') }}" alt="IMG">
                        </div>
                        <div class="form-input login width-50">
                            <div class="form-table">
                            <form action="{{ url('/login') }}" method="POST" id="form-login" class="login100-form validate-form">
                                @csrf
                                <div class="wrap-input100 validate-input {{ $errors->has('email') ? ' alert-validate' : '' }}" data-validate="Valid email is required: ex@abc.xyz">
                                    <input class="input100 validation" type="email" name="email" placeholder="Your Email" value="{{ old('email') }}">
                                    <span class="focus-input100"></span>
                                    <span class="symbol-input100">
                                        {{-- <i class="fa fa-envelope" aria-hidden="true"></i> --}}
                                        EMAIL &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :
                                    </span>
                                </div>
                                <div class="wrap-input100 validate-input {{ $errors->has('phone') ? ' alert-validate' : '' }}" data-validate="Phone is required">
                                    <input class="input100 validation number" type="number" name="phone" placeholder="Your Phone" value="{{ old('phone') }}">
                                    <span class="focus-input100"></span>
                                    <span class="symbol-input100">
                                        {{-- <i class="fa fa-phone" aria-hidden="true"></i> --}}
                                        HANDPHONE &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                                    </span>
                                </div>
                                <div class="wrap-input100 validate-input {{ $errors->has('code_voucher') ? ' alert-validate' : '' }}" data-validate="Voucher is required">
                                    <input class="input100 validation" type="text" name="code_voucher" value="{{ old('code_voucher') }}">
                                    <span class="focus-input100"></span>
                                    <span class="symbol-input100">
                                        {{-- <i class="fa fa-gift" aria-hidden="true"></i> --}}
                                        VOCHER CODE &nbsp;&nbsp;&nbsp; :
                                    </span>
                                </div>
                                <div class="btn-action flex-end width-100">
                                    <button type="button" class="login100-form-btn btn-next" id="popUpPayment">Login</button>
                                </div>

                                <div class="btn-action flex-wrap-center width-100">
                                   <div class="line-horizontal width-100"></div>
                                   <p class="width-100">Don’t have an acount yet?</p>
                                    <a href="{{ url('/register') }}">
                                        <button type="button" class="login100-form-btn btn-back">Register</button>
                                    </a>
                                    
                                </div>
                                @if ($errors->any())
                                    <div class="text-center p-t-20">
                                        @foreach ($errors->all() as $error)
                                            <p style="color: red">
                                                {{ $error }}
                                            </p>
                                        @endforeach
                                    </div>
                                @endif
                               
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
@endsection

@push('script')
<script>
	$(document).ready(function(){
        // Remove Session Login
        localStorage.removeItem("user_name");
        sessionStorage.removeItem("currentloggedin");
        sessionStorage.removeItem("base_url");
        sessionStorage.removeItem("csrf-token");
        document.cookie = "base_url= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
        document.cookie = "csrf-token= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
    });

    var input = $('.validate-input .validation');
    $("#popUpPayment").click(function()
    {
        var check = true;
		for (var i = 0; i < input.length; i++) {
			if (validate(input[i]) == false) {
				showValidate(input[i]);
				check = false;
			}
        }
        // if validation success
        if(check)
        {
            console.log('validation success');

            $('#form-login').submit();

            return
        }
        console.log('validation error');
        return
    });

    $('.validate-form .input100').each(function () {
		$(this).focus(function () {
			hideValidate(this);
		});
    });
    
    function validate(input) {
		if ($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
			if ($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
				return false;
			}
		} else {
			if ($(input).val().trim() == '') {
				return false;
			}
		}
	}

	function showValidate(input) {
		var thisAlert = $(input).parent();
		$(thisAlert).addClass('alert-validate');
	}

	function hideValidate(input) {
		var thisAlert = $(input).parent();
		$(thisAlert).removeClass('alert-validate');
	}

    $(document).on('keydown','.phoneNumber', function(e)
    {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
            (e.keyCode == 187 && e.shiftKey === true) || 
                // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $(document).on('keydown','.number', function(e)
    {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
</script>
<script>
    @include('web.layout.form.notification')
</script>
@endpush
