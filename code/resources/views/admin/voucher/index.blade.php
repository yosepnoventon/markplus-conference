@extends('admin.layout.frame')

@push('breadcrumb')
    <nav class="hk-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Voucher</li>
        </ol>
    </nav>
@endpush

@section('title', '| About Us')

@section('content')

    <!-- Container -->
    <div class="container-fluid mt-xl-50 mt-sm-30 mt-15">
        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title" style="margin-bottom: 35px">
                        Voucher
                        <span>
                            <a href="{{ url('/admin/vouchers/create') }}" class="btn bg-success" style="color: white; float:right">
                                <i class="fa fa-plus" aria-hidden="true"></i> Create
                            </a>
                        </span>
                        
                        <span>
                            <a href="{{ url('/admin/vocuhers/export') }}" class="btn bg-info" style="color: white; float:right">
                                <i class="fa fa-download" aria-hidden="true"></i> Export
                            </a>
                        </span>
                    </h5>
                    <div class="row">
                        <div class="col-sm">
                            <div class="table-wrap table-responsive">
                                <table id="voucher" class="table table-bordered table-striped table-saw" cellspacing="0">
                                    <thead align="left">
                                        <tr>
                                            <th width="10%">No</th>
                                            <th width="25%">Voucher Code</th>
                                            <th width="25%">Email</th>
                                            <th width="10%">Quota</th>
                                            <th width="15%">Remaining Quota</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- /Row -->
    </div>
    <!-- /Container -->

@endsection

@push('script')
    <script>
        var oTable;
        $(document).ready(function()
        {
            oTable = $('#voucher').DataTable({
                processing: true,
                serverSide: true,
                order:  [[ 0, "asc" ]],
                searching: true,
                //dom: 'Bfrtip',
                //buttons: [
                //    'copy', 'csv', 'excel', 'pdf', 'print'
                //],
                responsive: true,
                ajax: '{!! route('vouchers.data') !!}',
                columns: [
                    { data: "rownum", name: "rownum", searchable: false },
                    { data: 'code_voucher', name: 'code_voucher'},
                    { data: 'email', name: 'email' },
                    { data: 'quota', name: 'quota' },
                    { data: 'remaining_quota', name: 'remaining_quota' }
                ]
            });
        });

        function deleteData(id)
        {
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this imaginary data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes!",
                cancelButtonText: "No!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: '{{ route("about-us.index") }}/' + id + '?' + $.param({"_token" : '{{ csrf_token() }}' }),
                        data: {_method: 'delete'},
                        complete: function (msg) {
                            oTable.draw();
                            swal("Success", "Your data already deleted", "success");
                        }
                    });
                } else {
                    swal("Cancelled", "Your imaginary data is safe :)", "error");
                }
            });
        }

        @include('admin.layout.notification')
    </script>
@endpush
