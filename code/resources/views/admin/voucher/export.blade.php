<!DOCTYPE html>
<html>
<head>
<title>Report Voucher</title>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th>No</th>
                <th>Code Voucher</th>
                <th>Email</th>
                <th>Quota</th>
            </tr>
        </thead>
        <tbody>
        @php $no = 1 @endphp
        @foreach($vouchers as $voucher)
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $voucher->code_voucher }}</td>
                <td>{{ $voucher->email }}</td>
                <td>{{ $voucher->quota }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</body>
</html>