@extends('admin.layout.frame')

@push('breadcrumb')
<nav class="hk-breadcrumb" aria-label="breadcrumb">
    <ol class="breadcrumb breadcrumb-light bg-transparent">
        <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ url('/admin/Promos') }}">Promos</a></li>
        <li class="breadcrumb-item active" aria-current="page">Show Promo</li>
    </ol>
</nav>
@endpush

@section('title', '| Show Promos')

@section('content')

<!-- Container -->
<div class="container-fluid mt-xl-50 mt-sm-30 mt-15">
    <!-- Row -->
    <div class="row">
        <div class="col-xl-12">
            <section class="hk-sec-wrapper">
                <h5 class="hk-sec-title" style="margin-bottom: 35px">
                    Promos
                    <span>
                        <a href="{{ url('/admin/promos') }}" class="btn bg-dark" style="color: white; float:right">
                            <i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h5>
                <div class="row">
                    <div class="col-sm">
                        <div class="table-wrap table-responsive">
                            <table class="table table-bordered table-saw">
                                <tr>
                                    <th colspan="2">
                                        @foreach($PromoDetails as $no => $PromoDetail)
                                        <table class="table table-bordered">
                                                <tr>
                                                    <!-- for learn method Webinar & In-Class rowspan 4, for E-Learning & Certification rowspan 2 -->
                                                    <th width="5%" rowspan="{{ ($PromoDetail->learn_method_id == 2 || $PromoDetail->learn_method_id == 4) ? 4 : 2 }}">{{ $no+1 }}</th>
                                                    <th width="20%">LEARNING METHOD</th>
                                                    @if($PromoDetail->learn_method_id == 0)
                                                        <!-- for All learn method -->
                                                        <th>All Learn Method</th>
                                                    @else
                                                        <!-- for learn method Certificate, Webinar & In-Class -->
                                                        <th>{{ $LearnMethods->where('id', $PromoDetail->learn_method_id)->first()->title }}</th>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <th>COURSE</th>
                                                    @if($PromoDetail->learn_method_id == 0)
                                                        <th>All Course</th>
                                                    @elseif($PromoDetail->learn_method_id == 1)
                                                        <th>All Course E-Learning</th>
                                                    @else
                                                        <th>{{ $PromoDetail->title }}</th>
                                                    @endif
                                                </tr>
                                                <!-- for learn method Webinar & In-Class -->
                                                @if($PromoDetail->learn_method_id == 2 || $PromoDetail->learn_method_id == 4)
                                                    <tr>
                                                        <th>COURSE DETAILS</th>
                                                        <th>{{ $PromoDetail->topik }}</th>
                                                    </tr>
                                                    <tr>
                                                        <th>TICKET CATEGORY</th>
                                                        <th>{{ $PromoDetail->name_tiket }}</th>
                                                    </tr>
                                                @endif
                                        </table>
                                        @endforeach

                                    </th>
                                </tr>

                                <tr>
                                    <th>CODE</th>
                                    <th>{{ $promo->code }}</th>
                                </tr>
                                <tr>
                                    <th>NAME</th>
                                    <th>{{ $promo->name }}</th>
                                </tr>
                                <tr>
                                    <th>PROMO TYPE</th>
                                    @if($promo->promo_type == 'DISCOUNT')
                                        <th>DISCOUNT</th>
                                    @else
                                        <th>FREE COURSES</th>
                                    @endif
                                </tr>
                                <!-- promo type Discount -->
                                @if($promo->promo_type == 'DISCOUNT')
                                    <tr>
                                        <th>TYPE AMOUNT / PRECENTAGE</th>
                                        @if($promo->value_type == 'AMOUNT')
                                            <th>AMOUNT</th>
                                        @else
                                            <th>PRECENTAGE</th>
                                        @endif
                                    </tr>
                                    <tr>
                                        <th>VALUE</th>
                                        @if($promo->value_type == 'AMOUNT')
                                            <th>{{ number_format($promo->value,2) }}</th>
                                        @else
                                            <th>{{ $promo->value }} %</th>
                                        @endif
                                    </tr>
                                @else
                                    <th colspan="2">
                                        @foreach($PromoDetailFrees as $no => $PromoDetail)
                                        <table class="table table-bordered">
                                                <tr>
                                                    <th width="20%">LEARNING METHOD</th>
                                                    @if($PromoDetail->learn_method_id == 0)
                                                        <!-- for All learn method -->
                                                        <th>All Learn Method</th>
                                                    @else
                                                        <!-- for learn method Certificate, Webinar & In-Class -->
                                                        <th>{{ $LearnMethods->where('id', $PromoDetail->learn_method_id)->first()->title }}</th>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <th>COURSE</th>
                                                    @if($PromoDetail->learn_method_id == 0)
                                                        <th>All Course</th>
                                                    @elseif($PromoDetail->learn_method_id == 1)
                                                        <th>All Course E-Learning</th>
                                                    @else
                                                        <th>{{ $PromoDetail->title }}</th>
                                                    @endif
                                                </tr>
                                                <!-- for learn method Webinar & In-Class -->
                                                @if($PromoDetail->learn_method_id == 2 || $PromoDetail->learn_method_id == 4)
                                                    <tr>
                                                        <th>COURSE DETAILS</th>
                                                        <th>{{ $PromoDetail->topik }}</th>
                                                    </tr>
                                                    <tr>
                                                        <th>TICKET CATEGORY</th>
                                                        <th>{{ $PromoDetail->name_tiket }}</th>
                                                    </tr>
                                                @endif
                                        </table>
                                        @endforeach

                                    </th>
                                @endif
                                <tr>
                                    <th>QUOTA</th>
                                    <th>{{ $promo->quota }}</th>
                                </tr>
                                <tr>
                                    <th>MAX USER PROMO</th>
                                    <th>{{ $promo->max_user_promo }}</th>
                                </tr>
                                <tr>
                                    <th>START DATE</th>
                                    <th>{{ \Carbon\Carbon::parse($promo->start_date)->format('d-F-Y H:i:s') }}</th>
                                </tr>
                                <tr>
                                    <th>START DATE</th>
                                    <th>{{ \Carbon\Carbon::parse($promo->end_date)->format('d-F-Y H:i:s') }}</th>
                                </tr>
                                <tr>
                                    <th>STATUS</th>
                                    @if($promo->status == 1)
                                        <th><p style="color: green">Active</p></th>
                                    @else
                                        <th><p style="color: red">Inactive</p></th>
                                    @endif
                                </tr>
                                <tr>
                                    <th>Note</th>
                                    <th>{{ $promo->note }}</th>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- /Row -->
</div>
<!-- /Container -->

@endsection

