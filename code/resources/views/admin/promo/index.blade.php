@extends('admin.layout.frame')

@push('breadcrumb')
    <nav class="hk-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Promos</li>
        </ol>
    </nav>
@endpush

@section('title', '| Promos')

@section('content')

    <!-- Container -->
    <div class="container-fluid mt-xl-50 mt-sm-30 mt-15">
        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title" style="margin-bottom: 35px">
                        Promos
                        <span>
                            <a href="{{ url('/admin/promos/create') }}" class="btn bg-success" style="color: white; float:right">
                                <i class="fa fa-plus" aria-hidden="true"></i> Create
                            </a>
                        </span>
                    </h5>
                    <div class="row">
                        <div class="col-sm">
                            <div class="table-wrap table-responsive">
                                <table id="promos" class="table table-bordered table-striped table-saw" width="100%">
                                    <thead align="left">
                                        <tr>
                                            <th width="5%">#</th>
                                            <th width="10%">Learn Method</th>
                                            <th width="10%">Code</th>
                                            <th width="10%">Value</th>
                                            <th width="15%">Start Date</th>
                                            <th width="15%">Expire Date</th>
                                            <th width="5%">Status</th>
                                            <th width="25%">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- /Row -->
    </div>
    <!-- /Container -->

@endsection

@push('script')
    <script>
        var oTable;
        $(document).ready(function()
        {
            oTable = $('#promos').DataTable({
                processing: true,
                serverSide: true,
                order:  [[ 0, "asc" ]],
                searching: true,
                //dom: 'Bfrtip',
                //buttons: [
                //    'copy', 'csv', 'excel', 'pdf', 'print'
                //],
                responsive: true,
                ajax: '{!! route('promos.data') !!}',
                columns: [
                    { data: "rownum", name: "rownum", searchable: false },
                    { data: 'learning_method', name: 'learning_method'},
                    { data: 'code', name: 'code'},
                    { data: 'value', name: 'value', className: 'text-right'},
                    { data: 'start_date', name: 'start_date'},
                    { data: 'end_date', name: 'end_date'},
                    { data: 'status', name: 'status'},
                    { data: 'action', name: 'action', orderable: false, searchable: false }
                ]
            });
        });

        function deleteData(id)
        {
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this imaginary data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes!",
                cancelButtonText: "No!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: '{{ route("promos.index") }}/' + id + '?' + $.param({"_token" : '{{ csrf_token() }}' }),
                        data: {_method: 'delete'},
                        complete: function (msg) {
                            oTable.draw();
                            swal("Success", "Your data already deleted", "success");
                        }
                    });
                } else {
                    swal("Cancelled", "Your imaginary data is safe :)", "error");
                }
            });
        }

        @include('admin.layout.notification')
    </script>
@endpush
