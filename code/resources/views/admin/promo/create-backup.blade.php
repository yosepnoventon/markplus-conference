@extends('admin.layout.frame')

@push('breadcrumb')
<nav class="hk-breadcrumb" aria-label="breadcrumb">
    <ol class="breadcrumb breadcrumb-light bg-transparent">
        <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ url('/admin/how-it-works') }}">Promos</a></li>
        <li class="breadcrumb-item active" aria-current="page">Create Promo</li>
    </ol>
</nav>
@endpush

@section('title', '| Create Promo')

@section('content')
    <!-- Container -->
    <div class="container-fluid mt-xl-50 mt-sm-30 mt-15">
        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title" style="margin-bottom: 15px">Create Promo
                        <span>
                            <a href="{{ url('/admin/promos') }}" class="btn bg-dark" style="color: white; float:right">
                                <i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back
                            </a>
                        </span>
                    </h5>
                    <div class="row">
                        <div class="col-sm">

                            @if ($errors->any())
                                <ul class="alert alert-danger" style="margin-top:15px">
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif

                            {!! Form::open([
                                'url' => '/admin/promos',
                                'class' => 'form-horizontal',
                                'files' => false,
                            ]) !!}

                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-md-4 col-sm-12">
                                                <div class="form-group">
                                                    <label for="instructor">Learn Method :</label>
                                                    <div class="input-group">
                                                        {!! Form::select('learning_method_id', $LearnMethod, null, ['class' => 'form-control custom-select', 'id' => 'learning_method_id']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-12">
                                                <div class="form-group">
                                                    <label for="instructor">Course :</label>
                                                    <div class="input-group">
                                                        <select name="course_id" id="course_id" class="form-control custom-select select2">
                                                            <option value="0">All Courses</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-12">
                                                <div class="form-group">
                                                    <label for="title">Name :</label>
                                                    <div class="input-group">
                                                        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-md-4 col-sm-12">
                                                <div class="form-group">
                                                    <label for="title">Code :</label>
                                                    <div class="input-group">
                                                        {!! Form::text('code', null, ['class' => 'form-control code', 'id' => 'code', 'required' => 'required']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-12">
                                                <div class="form-group">
                                                    <label for="instructor">Type :</label>
                                                    <div class="input-group">
                                                        {!! Form::select('value_type', ["AMOUNT" => "AMOUNT", "PRECENTAGE" => "PRECENTAGE"], null, ['class' => 'form-control custom-select', 'id' => 'value_type']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-12">
                                                <div class="form-group">
                                                    <label for="title">Value :</label>
                                                    <div class="input-group">
                                                        {!! Form::text('value', null, ['class' => 'form-control inputnumber', 'id' => 'amount', 'required' => 'required']) !!}
                                                        {!! Form::text('value', null, ['class' => 'form-control inputnumber', 'id' => 'precentage', 'required' => 'required']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-md-4 col-sm-12">
                                                <div class="form-group">
                                                    <label for="title">Quota :</label>
                                                    <div class="input-group">
                                                        {!! Form::text('quota', null, ['class' => 'form-control inputnumber', 'required' => 'required']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-12">
                                                <div class="form-group">
                                                    <label for="instructor">Max Peruser Promo :</label>
                                                    <div class="input-group">
                                                        {!! Form::text('max_user_promo', null, ['class' => 'form-control inputnumber', 'required' => 'required']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-12">
                                                <div class="form-group">
                                                    <label for="title">Status :</label>
                                                    <div class="input-group">
                                                        {!! Form::select('status', ["1" => "Active", "0" => "Inactive"], null, ['class' => 'form-control custom-select']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-md-4 col-sm-12">
                                                <div class="form-group">
                                                    <label for="title">Start & End Date  :</label>
                                                    <div class="input-group">
                                                        @if(isset($promo->start_date))
                                                            {!! Form::text('date', \Carbon\Carbon::parse($promo->start_date)->format('d-m-Y H:i:s').' - '.\Carbon\Carbon::parse($promo->end_date)->format('d-m-Y H:i:s'), ['class' => 'form-control', 'id' => 'date_', 'required' => 'required']) !!}
                                                        @else
                                                            {!! Form::text('date', 'null', ['class' => 'form-control', 'id' => 'date_', 'required' => 'required']) !!}
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8 col-sm-12">
                                                <div class="form-group">
                                                    <label for="preview_text">Note</label>
                                                    <div class="input-group">
                                                        {!! Form::textarea('note', null, ['class' => 'form-control', 'rows' => '2']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn bg-green', 'style' => 'color: white'])!!}
                                    @if(!isset($submitButtonText))
                                        <input type="reset" value="Clear" class="btn bg-grey" style="color: white">
                                    @endif
                                </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- /Row -->
    </div>
    <!-- /Container -->
@endsection


@push('script')
    <script>

        $('#learning_method_id').on('change',function()
        {
            var learn_method = $(this).val();
            if(learn_method == 3)
            {
                var url = "{{ url('/admin/promo/getCourse') }}";
                $.ajax({
                    type: "GET",
                    url: url,
                    dataType: "json",
                    data: {
                        "learn_method": learn_method,
                        "_token"     : '{{ csrf_token() }}'
                    },
                    success: function(datas)
                    {
                        // console.log(datas.data);
                        $('#course_id option').val('0').remove();
                        for(var i in datas.data) {
                            @if(isset($promo->course_id))
                                if(datas.data[i].id == '{{ $promo->course_id }}'){
                                    $('#course_id').append('<option value='+datas.data[i].id+' selected="selected">'+datas.data[i].title+'</option>');
                                }else{
                                    $('#course_id').append('<option value='+datas.data[i].id+'>'+datas.data[i].title+'</option>');
                                }
                            @else
                                $('#course_id').append('<option value='+datas.data[i].id+'>'+datas.data[i].title+'</option>');
                            @endif
                        }
                    },error: function(data)
                    {

                    }
                });
            }else if(learn_method == 2)
            {
                var url = "{{ url('/admin/promo/getCourse') }}";
                $.ajax({
                    type: "GET",
                    url: url,
                    dataType: "json",
                    data: {
                        "learn_method": learn_method,
                        "_token"     : '{{ csrf_token() }}'
                    },
                    success: function(datas)
                    {
                        // console.log(datas.data);
                        $('#course_id option').val('0').remove();
                        for(var i in datas.data) {
                            @if(isset($promo->course_id))
                                if(datas.data[i].id == '{{ $promo->course_id }}'){
                                    $('#course_id').append('<option value='+datas.data[i].id+' selected="selected">'+datas.data[i].title+'</option>');
                                }else{
                                    $('#course_id').append('<option value='+datas.data[i].id+'>'+datas.data[i].title+'</option>');
                                }
                            @else
                                $('#course_id').append('<option value='+datas.data[i].id+'>'+datas.data[i].title+'</option>');
                            @endif
                        }
                    },error: function(data)
                    {

                    }
                });
            }else if(learn_method == 1){
                $('#course_id option').val('0').remove();
                $('#course_id').append('<option value="0">All Courses E-Learning</option>');
            }else{
                $('#course_id option').val('0').remove();
                $('#course_id').append('<option value="0">All Courses</option>');
            }
        }).change();

        $('#value_type').on('change',function()
        {
            var value_type = $(this).val();
            if(value_type == 'amount'){
                $('#amount').removeAttr('disabled').css('display', '');
                $('#precentage').attr('disabled', 'disabled').css('display', 'none');
            }else{
                $('#amount').attr('disabled', 'disabled').css('display', 'none');
                $('#precentage').removeAttr('disabled').css('display', '');
            }
        }).change();

    </script>
    <script>
        $(document).on('keydown','.code', function(e){
            if (e.keyCode == 32) {
            // e.preventDefault();
            return false;
            }
            if (e.keyCode == 32) return false;
        });
        $(document).on('keydown','.inputnumber', function(e){
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) || 
                    // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                        // let it happen, don't do anything
                        return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
        
        $('#date_').daterangepicker({
            minDate: new Date(),
            timePicker: true,
            opens: 'center',
            timePicker24Hour: true,
            locale: {
                format: 'DD-MM-YYYY HH:mm:ss'
            }
        }, function(start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD HH:mm:ss') + ' to ' + end.format('YYYY-MM-DD HH:mm:ss'));
        });
        // select option search
        $('#course_id').select2();


        @include('admin.layout.notification')
    </script>
@endpush