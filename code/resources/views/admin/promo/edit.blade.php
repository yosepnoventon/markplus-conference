@extends('admin.layout.frame')

@push('breadcrumb')
<nav class="hk-breadcrumb" aria-label="breadcrumb">
    <ol class="breadcrumb breadcrumb-light bg-transparent">
        <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ url('/admin/how-it-works') }}">Promos</a></li>
        <li class="breadcrumb-item active" aria-current="page">Edit Promo</li>
    </ol>
</nav>
@endpush

@section('title', '| Edit Promo')

@section('content')
    <!-- Container -->
    <div class="container-fluid mt-xl-50 mt-sm-30 mt-15">
        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title" style="margin-bottom: 15px">Edit Promo
                        <span>
                            <a href="{{ url('/admin/promos') }}" class="btn bg-dark" style="color: white; float:right">
                                <i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back
                            </a>
                        </span>
                    </h5>
                    <div class="row">
                        <div class="col-sm">

                            @if ($errors->any())
                                <ul class="alert alert-danger" style="margin-top:15px">
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif

                            {!! Form::model($promo, [
                                'method' => 'PATCH',
                                'url' => ['/admin/promos', $promo->id],
                                'class' => 'form-horizontal',
                                'files' => false,
                            ]) !!}

                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-md-4 col-sm-12">
                                                <div class="form-group">
                                                    <label for="title">Name : </label>
                                                    <div class="input-group">
                                                        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-12">
                                                <div class="form-group">
                                                    <label for="title">Code : </label>&nbsp;<label class="code_required"></label>
                                                    <div class="input-group">
                                                        {!! Form::text('code', null, ['class' => 'form-control mr-1 code', 'id' => 'code', 'required' => 'required']) !!}
                                                        <button type="button" class="btn btn-primary" id="checkCode">Check</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div id="TextBoxesGroup">
                                            @php $no=1 @endphp
                                            <!-- looping Promo Detail non Free Course -->
                                            @foreach($PromoDetails as $PromoDetail)
                                                <div id="TextBox_{{ $no }}">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="row">
                                                                <div class="col-md-4 col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Learn Method :</label>
                                                                        <div class="input-group">
                                                                            {!! old('learning_method_id') !!}
                                                                            <select name="learning_method_id[]" id="learning_method_id_{{ $no }}" class="learning_method_id form-control custom-select select2" onchange="learning_method_id({{ $no }})">
                                                                                @if($no == 1)
                                                                                    <option value="0" @if($PromoDetail->learn_method_id == 0) selected @endif>ALL LEARN METHOD</option>
                                                                                @endif
                                                                                @foreach($LearnMethods as $LearnMethod)
                                                                                    <option value="{{ $LearnMethod->id }}" @if($PromoDetail->learn_method_id == $LearnMethod->id) selected @endif>{{ $LearnMethod->title }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4 col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Course : </label><label class="course_required" style="color:#d44950;"></label>
                                                                        <div class="input-group">
                                                                            <select name="course_id[]" id="course_id_{{ $no }}" class="course_id form-control custom-select select2" onchange="course_id({{ $no }})">
                                                                                @if($PromoDetail->learn_method_id == 0)
                                                                                    <!-- for All learn method -->
                                                                                    <option value="0">All Courses</option>
                                                                                @elseif($PromoDetail->learn_method_id == 1)
                                                                                    <!-- for learn method E-Learning -->
                                                                                    <option value="0">All Courses E-Learning</option>
                                                                                @elseif($PromoDetail->learn_method_id == 2 || $PromoDetail->learn_method_id == 3 || $PromoDetail->learn_method_id == 4)
                                                                                    <!-- for learn method Webinar, Certificate & In-Class -->
                                                                                    <option value="0">Choose Course</option>
                                                                                    <option value="{{ $PromoDetail->course_id }}" selected>{{ $PromoDetail->title }}</option>
                                                                                @endif
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                @if($PromoDetail->learn_method_id == 0 || $PromoDetail->learn_method_id == 1 || $PromoDetail->learn_method_id == 3)
                                                                    <!-- for All learn method, E-Learning & Certificate -->
                                                                    <div class="col-md-4 col-sm-12" id="detail_course_{{ $no }}" style="display:none;">
                                                                        <div class="form-group">
                                                                            <label>Detail Course : </label><label class="course_detail_required" style="color:#d44950;"></label>
                                                                            <div class="input-group">
                                                                                <select name="course_detail_id[]" id="course_detail_id_{{ $no }}" class="course_detail_id form-control custom-select select2" onchange="course_detail_id({{ $no }})">
                                                                                    <option value="0">Choose Detail Courses</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4 col-sm-12" id="ticket_{{ $no }}" style="display:none;">
                                                                        <div class="form-group">
                                                                            <label>Ticket : </label><label class="ticket_required" style="color:#d44950;"></label>
                                                                            <div class="input-group">
                                                                                <select name="ticket_id[]" id="ticket_id_{{ $no }}" class="ticket_id form-control custom-select select2">
                                                                                    <option value="0">Choose Ticket</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @elseif($PromoDetail->learn_method_id == 2 || $PromoDetail->learn_method_id == 4)
                                                                    <!-- for learn method Webinar & In-Class -->
                                                                    <div class="col-md-4 col-sm-12" id="detail_course_{{ $no }}" style="display:block;">
                                                                        <div class="form-group">
                                                                            <label>Detail Course : </label><label class="course_detail_required" style="color:#d44950;"></label>
                                                                            <div class="input-group">
                                                                                <select name="course_detail_id[]" id="course_detail_id_{{ $no }}" class="course_detail_id form-control custom-select select2" onchange="course_detail_id({{ $no }})">
                                                                                    <option value="0">Choose Detail Courses</option>
                                                                                    <option value="{{ $PromoDetail->detail_course_id }}" selected>{{ $PromoDetail->topik }}</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4 col-sm-12" id="ticket_{{ $no }}" style="display:block;">
                                                                        <div class="form-group">
                                                                            <label>Ticket : </label><label class="ticket_required" style="color:#d44950;"></label>
                                                                            <div class="input-group">
                                                                                <select name="ticket_id[]" id="ticket_id_{{ $no }}" class="ticket_id form-control custom-select select2">
                                                                                    <option value="0">Choose Ticket</option>
                                                                                    @if($PromoDetail->category_tiket_id == "all")
                                                                                        <option value="all" selected>All Ticket</option>
                                                                                    @else
                                                                                        <option value="all">All Ticket</option>
                                                                                        <option value="{{ $PromoDetail->category_tiket_id }}" selected>{{ $PromoDetail->name_tiket }}</option>
                                                                                    @endif
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @php $no++ @endphp
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <input type="button" class="btn btn-success" value="Add Course" id="addButton">
                                            <input type="button" class="btn btn-danger" value="Remove Course" id="removeButton">
                                        </div>
                                        <hr>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-md-4 col-sm-12">
                                                <div class="form-group">
                                                    <label for="instructor">Promo Type :</label>
                                                    <div class="input-group">
                                                        {!! Form::select('promo_type', ["DISCOUNT" => "DISCOUNT", "FREE_COURSE" => "FREE COURSE"], null, ['class' => 'form-control custom-select', 'id' => 'promo_type']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                        </div>
                                    </div>
                                    @if($promo->promo_type == "DISCOUNT")
                                        <div class="col-md-12 col-sm-12" id="promo_type_column">
                                            <!-- looping Promo Detail -->
                                            <div class="row" id="discount_type">
                                                <div class="col-md-4 col-sm-12">
                                                    <div class="form-group">
                                                        <label for="instructor">Type :</label>
                                                        <div class="input-group">
                                                            {!! Form::select('value_type', ["AMOUNT" => "AMOUNT", "PRECENTAGE" => "PRECENTAGE"], null, ['class' => 'form-control custom-select', 'id' => 'value_type']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-12">
                                                    <div class="form-group">
                                                        <label for="title">Value :</label>
                                                        <div class="input-group">
                                                            {!! Form::text('value', null, ['class' => 'form-control inputnumber', 'id' => 'amount', 'required' => 'required']) !!}
                                                            {!! Form::text('value', null, ['class' => 'form-control inputnumber', 'id' => 'precentage', 'required' => 'required']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <div class="col-md-12 col-sm-12" id="promo_type_column">
                                            <!-- looping Promo Detail -->
                                            <div class="row" id="discount_type" style="display:none;">
                                                <div class="col-md-4 col-sm-12">
                                                    <div class="form-group">
                                                        <label for="instructor">Type :</label>
                                                        <div class="input-group">
                                                            {!! Form::select('value_type', ["AMOUNT" => "AMOUNT", "PRECENTAGE" => "PRECENTAGE"], 0, ['class' => 'form-control custom-select', 'id' => 'value_type']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-12">
                                                    <div class="form-group">
                                                        <label for="title">Value :</label>
                                                        <div class="input-group">
                                                            {!! Form::text('value', null, ['class' => 'form-control inputnumber', 'id' => 'amount']) !!}
                                                            {!! Form::text('value', null, ['class' => 'form-control inputnumber', 'id' => 'precentage']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- looping Promo Detail Free Course -->
                                            @foreach($PromoDetailFrees as $PromoDetail)
                                                <div id="FreeCourse">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="row">
                                                                <div class="col-md-4 col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Learn Method :</label>
                                                                        <div class="input-group">
                                                                            <select name="free_learning_method_id" id="free_learning_method_id" class="form-control custom-select select2" onchange="free_learning_method()">
                                                                                @foreach($LearnMethods as $LearnMethod)
                                                                                    <option value="{{ $LearnMethod->id }}" @if($PromoDetail->learn_method_id == $LearnMethod->id) selected @endif>{{ $LearnMethod->title }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4 col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Course : </label><label class="free_course_required" style="color:#d44950;"></label>
                                                                        <div class="input-group">
                                                                            <select name="free_course_id" id="free_course_id" class="form-control custom-select select2" onchange="free_course()">
                                                                                @if($PromoDetail->learn_method_id == 1)
                                                                                    <!-- for learn method E-Learning -->
                                                                                    <option value="0">All Courses E-Learning</option>
                                                                                @elseif($PromoDetail->learn_method_id == 2 || $PromoDetail->learn_method_id == 3 || $PromoDetail->learn_method_id == 4)
                                                                                    <!-- for learn method Webinar, Certificate & In-Class -->
                                                                                    <option value="0">Choose Course</option>
                                                                                    <option value="{{ $PromoDetail->course_id }}" selected>{{ $PromoDetail->title }}</option>
                                                                                @endif
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                @if($PromoDetail->learn_method_id == 1 || $PromoDetail->learn_method_id == 3)
                                                                    <div class="col-md-4 col-sm-12" id="free_detail_course" style="display:none;">
                                                                        <div class="form-group">
                                                                            <label>Detail Course : </label><label class="free_course_detail_required" style="color:#d44950;"></label>
                                                                            <div class="input-group">
                                                                                <select name="free_course_detail_id" id="free_course_detail_id" class="form-control custom-select select2" onchange="free_course_detail()">
                                                                                    <option value="0">Choose Detail Courses</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4 col-sm-12" id="free_ticket" style="display:none;">
                                                                        <div class="form-group">
                                                                            <label>Ticket : </label><label class="free_ticket_required" style="color:#d44950;"></label>
                                                                            <div class="input-group">
                                                                                <select name="free_ticket_id" id="free_ticket_id" class="form-control custom-select select2">
                                                                                    <option value="0">Choose Ticket</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @elseif($PromoDetail->learn_method_id == 2 || $PromoDetail->learn_method_id == 4)
                                                                    <div class="col-md-4 col-sm-12" id="free_detail_course" style="display:block;">
                                                                        <div class="form-group">
                                                                            <label>Detail Course : </label><label class="free_course_detail_required" style="color:#d44950;"></label>
                                                                            <div class="input-group">
                                                                                <select name="free_course_detail_id" id="free_course_detail_id" class="form-control custom-select select2" onchange="free_course_detail()">
                                                                                    <option value="0">Choose Detail Courses</option>
                                                                                    <option value="{{ $PromoDetail->detail_course_id }}" selected>{{ $PromoDetail->topik }}</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4 col-sm-12" id="free_ticket" style="display:block;">
                                                                        <div class="form-group">
                                                                            <label>Ticket : </label><label class="free_ticket_required" style="color:#d44950;"></label>
                                                                            <div class="input-group">
                                                                                <select name="free_ticket_id" id="free_ticket_id" class="form-control custom-select select2">
                                                                                    <option value="0">Choose Ticket</option>
                                                                                    <option value="{{ $PromoDetail->category_tiket_id }}" selected>{{ $PromoDetail->name_tiket }}</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                    <div class="col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-md-4 col-sm-12">
                                                <div class="form-group">
                                                    <label for="title">Quota :</label>
                                                    <div class="input-group">
                                                        {!! Form::text('quota', null, ['class' => 'form-control inputnumber', 'required' => 'required']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-12">
                                                <div class="form-group">
                                                    <label for="instructor">Max Peruser Promo :</label>
                                                    <div class="input-group">
                                                        {!! Form::text('max_user_promo', null, ['class' => 'form-control inputnumber', 'required' => 'required']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-12">
                                                <div class="form-group">
                                                    <label for="title">Status :</label>
                                                    <div class="input-group">
                                                        {!! Form::select('status', ["1" => "Active", "0" => "Inactive"], null, ['class' => 'form-control custom-select']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-12">
                                                <div class="form-group">
                                                    <label for="title">Start & End Date  :</label>
                                                    <div class="input-group">
                                                        @if(isset($promo->start_date))
                                                            {!! Form::text('date', \Carbon\Carbon::parse($promo->start_date)->format('d-m-Y H:i:s').' - '.\Carbon\Carbon::parse($promo->end_date)->format('d-m-Y H:i:s'), ['class' => 'form-control', 'id' => 'date_', 'required' => 'required']) !!}
                                                        @else
                                                            {!! Form::text('date', 'null', ['class' => 'form-control', 'id' => 'date_', 'required' => 'required']) !!}
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8 col-sm-12">
                                                <div class="form-group">
                                                    <label for="preview_text">Note</label>
                                                    <div class="input-group">
                                                        {!! Form::textarea('note', null, ['class' => 'form-control', 'rows' => '2']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::button('Update', ['class' => 'btn bg-green', 'id' => 'btn_update', 'style' => 'color: white'])!!}
                                    <input type="submit" value="submit" id="btn_submit" hidden>
                                </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- /Row -->
    </div>
    <!-- /Container -->
@endsection


@push('script')
    <script type="text/javascript">
        // first load activetion select2 in Learn Method
        @foreach($PromoDetails as $no => $PromoDetail)
            $("#learning_method_id_{{ $no+1 }}").select2();
        @endforeach
        // var counter for div TextBox_(counter)
        var counter = {{ count($PromoDetails) }}+1;
        // if field Course 1 button remove course Disabled
        if(counter == 2)
        {
            $("#removeButton").prop("disabled", true);
        }
        // if field Course 1 button remove course unlock
        else
        {
            $("#removeButton").prop("disabled", false);
        }
        // check code can used or not
        $("#checkCode").click(function ()
        {
            var code = $("#code").val();
            $(".code_required").html(".....");
            // if code null
            if(code == "")
            {
                $(".code_required").css("color", "#d44950").html("Required");
            }
            else
            {
                var url = "{{ url('/admin/promo/cekCode') }}";
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    data: {
                        "id"    : "{{ $promo->id }}",
                        "code"  : code,
                        "_token": '{{ csrf_token() }}'
                    },
                    success: function(datas)
                    {
                    if(datas.status == 200)
                    {
                            $(".code_required").css("color", "#4159c3").html("Can Used");
                    }
                    else
                    {
                            $(".code_required").css("color", "#d44950").html("Already Exists");
                    }
                    },
                    error: function(data)
                    {
                        window.location.reload();
                    }
                }); 
            }
        });
        // add course field
        $("#addButton").click(function ()
        {
            if(counter>10){
                swal({
                    title: "Warning",
                    text: "Only 10 Course allow",
                    type: "warning",
                    showConfirmButton: false,
                    timer:2000
                });
                    return false;
            }
            // add text field and append to id TextBoxesGroup
            var newTextBox = $(document.createElement('div')).attr("id", 'TextBox_' + counter);
            var html = '<div class="row">'+
                            '<div class="col-sm-12">'+
                                '<div class="row">'+
                                    '<div class="col-md-4 col-sm-12">'+
                                        '<div class="form-group">'+
                                            '<label>Learn Method :</label>'+
                                            '<div class="input-group">'+
                                                '<select name="learning_method_id[]" id="learning_method_id_'+ counter + '" class="learning_method_id form-control custom-select select2" onchange="learning_method_id('+ counter +')">'+
                                                    @foreach($LearnMethods as $LearnMethod)
                                                    '<option value="{{ $LearnMethod->id }}">{{ $LearnMethod->title }}</option>'+
                                                    @endforeach
                                                '</select>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-md-4 col-sm-12">'+
                                        '<div class="form-group">'+
                                            '<label>Course : </label><label class="course_required" style="color:#d44950;"></label>'+
                                            '<div class="input-group">'+
                                                '<select name="course_id[]" id="course_id_'+ counter + '" class="course_id form-control custom-select select2" onchange="course_id('+ counter + ')">'+
                                                    '<option value="0">All Courses</option>'+
                                                '</select>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-md-4 col-sm-12" id="detail_course_'+ counter +'" style="display:none;">'+
                                        '<div class="form-group">'+
                                            '<label>Detail Course : </label><label class="course_detail_required" style="color:#d44950;"></label>'+
                                            '<div class="input-group">'+
                                                '<select name="course_detail_id[]" id="course_detail_id_'+ counter +'" class="course_detail_id form-control custom-select select2" onchange="course_detail_id('+ counter +')">'+
                                                    '<option value="0">Choose Detail Courses</option>'+
                                                '</select>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-md-4 col-sm-12" id="ticket_'+ counter +'" style="display:none;">'+
                                        '<div class="form-group">'+
                                            '<label>Ticket : </label><label class="ticket_required" style="color:#d44950;"></label>'+
                                            '<div class="input-group">'+
                                                '<select name="ticket_id[]" id="ticket_id_'+ counter +'" class="ticket_id form-control custom-select select2">'+
                                                    '<option value="0">Choose Ticket</option>'+
                                                '</select>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';

            newTextBox.after().html(html);
        
            // add columns in TextBox_(number) select, text to the div TextBoxesGroup 
            newTextBox.appendTo("#TextBoxesGroup");
            // activetion select2 in Learn Method
            $("#learning_method_id_"+counter).select2();

            // add a variable counter
            counter++;
            // if field Course 1 button remove course Disabled
            if(counter == 2)
            {
                $("#removeButton").prop("disabled", true);
            }
            // if field Course 1 button remove course unlock
            else
            {
                $("#removeButton").prop("disabled", false);
            }
        });

        // remove course field
        $("#removeButton").click(function ()
        {
            if(counter==2){
                swal({
                    title: "Warning",
                    text: "do not delete the first fields",
                    type: "warning",
                    showConfirmButton: false,
                    timer:2000
                });
                return false;
            }
        
            // add a variable counter
            counter--;
    
            $("#TextBox_" + counter).remove();
            // if field Course 1 button remove course Disabled
            if(counter == 2)
            {
                $("#removeButton").prop("disabled", true);
            }
            // if field Course 1 button remove course unlock
            else
            {
                $("#removeButton").prop("disabled", false);
            }
        });
    </script>
    <script>

        function learning_method_id(no)
        {
            var learn_method = $('#learning_method_id_'+no).val();
            // for learn method E-Learning
            if(learn_method == 1)
            {
                // remove all option
                $('#course_id_'+no+' option').remove();
                $('#course_id_'+no).append('<option value="0">All Courses E-Learning</option>');
                // reset detail course & hidden
                $("#detail_course_"+no).hide();
                $('#course_detail_id_'+no+' option').remove();
                $('#course_detail_id_'+no).append('<option value="0">Choose Detail Courses</option>');
                // reset ticket & hidden
                $("#ticket_"+no).hide();
                $('#ticket_id_'+no+' option').remove();
                $('#ticket_id_'+no).append('<option value="0">Choose Ticket</option>');

                // unlock button add Course
                $("#addButton").prop("disabled", false);
                // if field Course 1 button remove course Disabled
                if(counter == 2)
                {
                    $("#removeButton").prop("disabled", true);
                }
                // if field Course 1 button remove course unlock
                else
                {
                    $("#removeButton").prop("disabled", false);
                }
            }
            // for learn method Webinar & In-Class
            else if(learn_method == 2 || learn_method == 4)
            {

                var url = "{{ url('/admin/promo/getCourse') }}";
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    data: {
                        "learn_method": learn_method,
                        "_token"     : '{{ csrf_token() }}'
                    },
                    success: function(datas)
                    {
                        // remove all option
                        $('#course_id_'+no+' option').remove();
                        if(datas.data.length == 0)
                        {
                            $('#course_id_'+no).append('<option value="0">Choose Course</option>');
                        }
                        else
                        {
                            $('#course_id_'+no).append('<option value="0">Choose Course</option>');
                            for(var i in datas.data)
                            {
                                $('#course_id_'+no).append('<option value='+datas.data[i].id+'>'+datas.data[i].title+'</option>');
                            }
                            // activetion select2 in course
                            $("#course_id_"+no).select2();
                        }
                    },
                    error: function(data)
                    {
                        window.location.reload();
                    }
                });
                // show detail course
                $("#detail_course_"+no).show();
                $('#course_detail_id_'+no+' option').remove();
                $('#course_detail_id_'+no).append('<option value="0">Choose Detail Courses</option>');
                // reset ticket & hidden
                $("#ticket_"+no).show();
                $('#ticket_id_'+no+' option').remove();
                $('#ticket_id_'+no).append('<option value="0">Choose Ticket</option>');

                // unlock button add Course
                $("#addButton").prop("disabled", false);
                // if field Course 1 button remove course Disabled
                if(counter == 2)
                {
                    $("#removeButton").prop("disabled", true);
                }
                // if field Course 1 button remove course unlock
                else
                {
                    $("#removeButton").prop("disabled", false);
                }
            }
            // for learn method cetificate
            else if(learn_method == 3)
            {
                var url = "{{ url('/admin/promo/getCourse') }}";
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    data: {
                        "learn_method": learn_method,
                        "_token"     : '{{ csrf_token() }}'
                    },
                    success: function(datas)
                    {
                        $('#course_id_'+no+' option').val('0').remove();
                        $('#course_id_'+no).append('<option value="0">Choose Course</option>');
                        for(var i in datas.data)
                        {
                            $('#course_id_'+no).append('<option value='+datas.data[i].id+'>'+datas.data[i].title+'</option>');
                        }
                        // activetion select2 in course
                        $("#course_id_"+no).select2();
                    },
                    error: function(data)
                    {

                    }
                });
                // reset detail course & hidden
                $("#detail_course_"+no).hide();
                $('#course_detail_id_'+no+' option').remove();
                $('#course_detail_id_'+no).append('<option value="0">Choose Detail Courses</option>');
                // reset ticket & hidden
                $("#ticket_"+no).hide();
                $('#ticket_id_'+no+' option').remove();
                $('#ticket_id_'+no).append('<option value="0">Choose Ticket</option>');

                // unlock button add Course
                $("#addButton").prop("disabled", false);
                // if field Course 1 button remove course Disabled
                if(counter == 2)
                {
                    $("#removeButton").prop("disabled", true);
                }
                // if field Course 1 button remove course unlock
                else
                {
                    $("#removeButton").prop("disabled", false);
                }
            }
            // for Learn Method All
            else
            {
                // disabled button add Course
                $("#addButton").prop("disabled", true);
                $("#removeButton").prop("disabled", true);
                // remove all option
                $('#course_id_'+no+' option').remove();
                $('#course_id_'+no).append('<option value="0">All Courses</option>');
                // reset detail course & hidden
                $("#detail_course_"+no).hide();
                $('#course_detail_id_'+no+' option').remove();
                $('#course_detail_id_'+no).append('<option value="0">Choose Detail Courses</option>');
                // reset ticket & hidden
                $("#ticket_"+no).hide();
                $('#ticket_id_'+no+' option').remove();
                $('#ticket_id_'+no).append('<option value="0">Choose Ticket</option>');
                
                // delete div all id TextBox_(number) except TextBox_1
                delete_all_course();
            }
        }
        
        function delete_all_course()
        {
            for (var a=2; a < counter; a++)
            {
                $("#TextBox_"+ a).remove();
            }
            // reduce the variable counter to be 1
            counter = 2;
        }

        function course_id(no)
        {
            var learn_method = $('#learning_method_id_'+no).val();
            var course_id = $('#course_id_'+no).val();
            if(learn_method == 2 || learn_method == 4)
            {
                var url = "{{ url('/admin/promo/getDetailCourse') }}";
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    data: {
                        "course_id"  : course_id,
                        "_token"     : '{{ csrf_token() }}'
                    },
                    success: function(datas)
                    {
                        $('#course_detail_id_'+no+' option').remove();
                        
                        $("#detail_course_"+no).show();
                        // if data empty
                        if(datas.data.length == 0)
                        {
                            $('#course_detail_id_'+no).append('<option value="0">Choose Detail Course</option>');
                        }
                        // if data not empty
                        else
                        {
                            $('#course_detail_id_'+no).append('<option value="0">Choose Detail Course</option>');
                            // loop detail course
                            for(var i in datas.data) {
                                $('#course_detail_id_'+no).append('<option value='+datas.data[i].id+'>'+datas.data[i].topik+'</option>');
                            }
                            // activetion select2 in course
                            $("#course_detail_id_"+no).select2();
                        }
                        
                        // reset ticket & hidden
                        $("#ticket_"+no).show();
                        $('#ticket_id_'+no+' option').remove();
                        $('#ticket_id_'+no).append('<option value="0">Choose Ticket</option>');
                    },
                    error: function(data)
                    {
                        window.location.reload();
                    }
                });
            }
        }

        function course_detail_id(no)
        {
            var detail_course_id = $('#course_detail_id_'+no).val();
            var url = "{{ url('/admin/promo/getTicket') }}";
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    "detail_course_id"  : detail_course_id,
                    "_token"            : '{{ csrf_token() }}'
                },
                success: function(datas)
                {
                    $('#ticket_id_'+no+' option').remove();
                    if(datas.data.length == 0)
                    {
                        $('#ticket_id_'+no).append('<option value="0">Choose Ticket</option>');
                    }
                    else
                    {
                        $('#ticket_id_'+no).append('<option value="0">Choose Ticket</option>');
                        $('#ticket_id_'+no).append('<option value="all">All Ticket</option>');
                        for(var i in datas.data) {
                            $('#ticket_id_'+no).append('<option value='+datas.data[i].ticket_id+'>'+datas.data[i].name_tiket+'</option>');
                        }
                    }
                },error: function(data)
                {
                    window.location.reload();
                }
            });
        }

        $('#promo_type').on('change',function()
        {
            var promo_type = $(this).val();
            if(promo_type == 'DISCOUNT')
            {
                $("#FreeCourse").remove();
                $("#discount_type").show();
                $("#amount").prop('required',true);
                $("#precentage").prop('required',true);
            }
            else
            {
                // add text field and append to id promo_type_column
                var newTextBox = $(document.createElement('div')).attr("id", 'FreeCourse');
                var html = '<div class="row">'+
                                '<div class="col-sm-12">'+
                                    '<div class="row">'+
                                        '<div class="col-md-4 col-sm-12">'+
                                            '<div class="form-group">'+
                                                '<label>Learn Method :</label>'+
                                                '<div class="input-group">'+
                                                    '<select name="free_learning_method_id" id="free_learning_method_id" class="form-control custom-select select2" onchange="free_learning_method()">'+
                                                        @foreach($LearnMethods as $LearnMethod)
                                                        '<option value="{{ $LearnMethod->id }}">{{ $LearnMethod->title }}</option>'+
                                                        @endforeach
                                                    '</select>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-4 col-sm-12">'+
                                            '<div class="form-group">'+
                                                '<label>Course : </label><label class="free_course_required" style="color:#d44950;"></label>'+
                                                '<div class="input-group">'+
                                                    '<select name="free_course_id" id="free_course_id" class="form-control custom-select select2" onchange="free_course()">'+
                                                        '<option value="0">All Courses E-Learning</option>'+
                                                    '</select>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-4 col-sm-12" id="free_detail_course" style="display:none;">'+
                                            '<div class="form-group">'+
                                                '<label>Detail Course : </label><label class="free_course_detail_required" style="color:#d44950;"></label>'+
                                                '<div class="input-group">'+
                                                    '<select name="free_course_detail_id" id="free_course_detail_id" class="form-control custom-select select2" onchange="free_course_detail()">'+
                                                        '<option value="0">Choose Detail Courses</option>'+
                                                    '</select>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-4 col-sm-12" id="free_ticket" style="display:none;">'+
                                            '<div class="form-group">'+
                                                '<label>Ticket : </label><label class="free_ticket_required" style="color:#d44950;"></label>'+
                                                '<div class="input-group">'+
                                                    '<select name="free_ticket_id" id="free_ticket_id" class="form-control custom-select select2">'+
                                                        '<option value="0">Choose Ticket</option>'+
                                                    '</select>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>';

                newTextBox.after().html(html);
            
                // add columns in FreeCourse select, text to the div promo_type_column 
                newTextBox.appendTo("#promo_type_column");

                // activetion select2 in Learn Method
                $("#free_learning_method_id").select2();
                $("#discount_type").hide();
                $("#amount").prop('required',false).val(0);
                $("#precentage").prop('required',false).val(0);
            }
        });

        $('#value_type').on('change',function()
        {
            var value_type = $(this).val();
            if(value_type == 'AMOUNT'){
                $('#amount').removeAttr('disabled').css('display', '');
                $('#precentage').attr('disabled', 'disabled').css('display', 'none');
            }else{
                $('#amount').attr('disabled', 'disabled').css('display', 'none');
                $('#precentage').removeAttr('disabled').css('display', '');
            }
        }).change();

        function free_learning_method()
        {
            var learn_method = $("#free_learning_method_id").val();
            // for learn method E-Learning
            if(learn_method == 1)
            {
                // remove all option
                $('#free_course_id option').remove();
                $('#free_course_id').append('<option value="0">All Courses E-Learning</option>');
                // reset detail course & hidden
                $("#free_detail_course").hide();
                $('#free_course_detail_id option').remove();
                $('#free_course_detail_id').append('<option value="0">Choose Detail Courses</option>');
                // reset ticket & hidden
                $("#free_ticket").hide();
                $('#free_ticket_id option').remove();
                $('#free_ticket_id').append('<option value="0">Choose Ticket</option>');
            }
            // for learn method Webinar & In-Class
            else if(learn_method == 2 || learn_method == 4)
            {
                var url = "{{ url('/admin/promo/getCourse') }}";
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    data: {
                        "learn_method": learn_method,
                        "_token"     : '{{ csrf_token() }}'
                    },
                    success: function(datas)
                    {
                        // remove all option
                        $('#free_course_id option').remove();
                        if(datas.data.length == 0)
                        {
                            $('#free_course_id').append('<option value="0">Choose Course</option>');
                        }
                        else
                        {
                            $('#free_course_id').append('<option value="0">Choose Course</option>');
                            for(var i in datas.data)
                            {
                                $('#free_course_id').append('<option value='+datas.data[i].id+'>'+datas.data[i].title+'</option>');
                            }
                            // activetion select2 in course
                            $("#free_course_id").select2();
                        }
                        // show detail course
                        $("#free_detail_course").show();
                        $('#free_course_detail_id option').remove();
                        $('#free_course_detail_id').append('<option value="0">Choose Detail Courses</option>');
                        // reset ticket & hidden
                        $("#free_ticket").show();
                        $('#free_ticket_id option').remove();
                        $('#free_ticket_id').append('<option value="0">Choose Ticket</option>');
                    },error: function(data)
                    {
                        window.location.reload();
                    }
                });
            }
            // for learn method cetificate
            else if(learn_method == 3)
            {
                var url = "{{ url('/admin/promo/getCourse') }}";
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    data: {
                        "learn_method": learn_method,
                        "_token"     : '{{ csrf_token() }}'
                    },
                    success: function(datas)
                    {
                        $('#free_course_id option').val('0').remove();
                        $('#free_course_id').append('<option value="0">Choose Course</option>');
                        for(var i in datas.data)
                        {
                            $('#free_course_id').append('<option value='+datas.data[i].id+'>'+datas.data[i].title+'</option>');
                        }
                        // activetion select2 in course
                        $("#free_course_id").select2();
                    },error: function(data)
                    {

                    }
                });
                // reset detail course & hidden
                $("#free_detail_course").hide();
                $('#free_course_detail_id option').remove();
                // reset ticket & hidden
                $("#free_ticket").hide();
                $('#free_ticket_id option').remove();
                $('#free_ticket_id').append('<option value="0">Choose Ticket</option>');
            }
        }

        function free_course()
        {
            var learn_method = $('#free_learning_method_id').val();
            var course_id = $('#free_course_id').val();
            if(learn_method == 2 || learn_method == 4)
            {
                var url = "{{ url('/admin/promo/getDetailCourse') }}";
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    data: {
                        "course_id"  : course_id,
                        "_token"     : '{{ csrf_token() }}'
                    },
                    success: function(datas)
                    {
                        $('#free_course_detail_id option').remove();
                        
                        $("#free_detail_course").show();
                        // if data empty
                        if(datas.data.length == 0)
                        {
                            $('#free_course_detail_id').append('<option value="0">Choose Detail Course</option>');
                        }
                        // if data not empty
                        else
                        {
                            $('#free_course_detail_id').append('<option value="0">Choose Detail Course</option>');
                            // loop detail course
                            for(var i in datas.data) {
                                $('#free_course_detail_id').append('<option value='+datas.data[i].id+'>'+datas.data[i].topik+'</option>');
                            }
                            // activetion select2 in course
                            $("#free_course_detail_id").select2();
                        }
                        
                        // reset ticket & hidden
                        $("#free_ticket").show();
                        $('#free_ticket_id option').remove();
                        $('#free_ticket_id').append('<option value="0">Choose Ticket</option>');
                    },
                    error: function(data)
                    {
                        window.location.reload();
                    }
                });
            }
        }

        function free_course_detail()
        {
            var detail_course_id = $('#free_course_detail_id').val();
            var url = "{{ url('/admin/promo/getTicket') }}";
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    "detail_course_id"  : detail_course_id,
                    "_token"            : '{{ csrf_token() }}'
                },
                success: function(datas)
                {
                    $('#free_ticket_id option').remove();
                    if(datas.data.length == 0)
                    {
                        $('#free_ticket_id').append('<option value="0">Choose Ticket</option>');
                    }
                    else
                    {
                        $('#free_ticket_id').append('<option value="0">Choose Ticket</option>');
                        for(var i in datas.data) {
                            $('#free_ticket_id').append('<option value='+datas.data[i].ticket_id+'>'+datas.data[i].name_tiket+'</option>');
                        }
                    }
                },error: function(data)
                {
                    window.location.reload();
                }
            });
        }
        // button update
        $("#btn_update").click(function ()
        {
            // initials for field is required flag = 1
            var error_flag = 0;
            // initials class select option learning_method_id
            var learning_method_id = $(".learning_method_id");
            // looping select option learning_method_id
            for (var i = 0; i < learning_method_id.length; i++)
            {
                // if option learning_method_id select E-Learning
                if(learning_method_id[i].value == 1)
                {
                    // nothing
                }
                // if option learning_method_id select Certificate
                else if(learning_method_id[i].value == 3)
                {
                    // if course id not selected
                    if($(".course_id")[i].value == 0)
                    {
                        $(".course_required")[i].innerHTML = "Required";
                        error_flag = 1;
                    }
                    // if course id selected
                    else
                    {
                        $(".course_required")[i].innerHTML = "";
                    }
                }
                // if option learning_method_id select Webinar & In-Class
                else if(learning_method_id[i].value == 2 || learning_method_id[i].value == 4)
                {
                    // if course id not selected
                    if($(".course_id")[i].value == 0)
                    {
                        $(".course_required")[i].innerHTML = "Required";
                        error_flag = 1;
                    }
                    // if course id selected
                    else
                    {
                        $(".course_required")[i].innerHTML = "";
                    }
                    // if detail course id not selected
                    if($(".course_detail_id")[i].value == 0)
                    {
                        $(".course_detail_required")[i].innerHTML = "Required";
                        error_flag = 1;
                    }
                    // if detail course id selected
                    else
                    {
                        $(".course_detail_required")[i].innerHTML = "";
                    }

                    // if Ticket id not selected
                    if($(".ticket_id")[i].value == 0)
                    {
                        $(".ticket_required")[i].innerHTML = "Required";
                        error_flag = 1;
                    }
                    // if Ticket id selected
                    else
                    {
                        $(".ticket_required")[i].innerHTML = "";
                    }
                }
                // if option learning_method_id All
                else{

                    // nothing
                }
            }

            if($('#promo_type').val() == "FREE_COURSE")
            {
                // if option learning_method_id select E-Learning
                if($("#free_learning_method_id").val() == 1)
                {
                    // nothing
                }
                // if option learning_method_id select Certificate
                else if($("#free_learning_method_id").val() == 3)
                {
                    // if course id not selected
                    if($("#free_course_id").val() == 0)
                    {
                        $(".free_course_required").html("Required");
                        error_flag = 1;
                    }
                    // if course id selected
                    else
                    {
                        $(".free_course_required").html("");
                    }
                }
                // if option learning_method_id select Webinar & In-Class
                else if($("#free_learning_method_id").val() == 2 || $("#free_learning_method_id").val() == 4)
                {
                    // if course id not selected
                    if($("#free_course_id").val() == 0)
                    {
                        $(".free_course_required").html("Required");
                        error_flag = 1;
                    }
                    // if course id selected
                    else
                    {
                        $(".free_course_required").html("");
                    }
                    // if detail course id not selected
                    if($("#free_course_detail_id").val() == 0)
                    {
                        $(".free_course_detail_required").html("Required");
                        error_flag = 1;
                    }
                    // if detail course id selected
                    else
                    {
                        $(".free_course_detail_required").html("");
                    }

                    // if Ticket id not selected
                    if($("#free_ticket_id").val() == 0)
                    {
                        $(".free_ticket_required").html("Required");
                        error_flag = 1;
                    }
                    // if Ticket id selected
                    else
                    {
                        $(".free_ticket_required").html("");
                    }
                }
                // if option learning_method_id All
                else{
                // nothing
                }
            }

            // if error flag 0 or not any field required
            if(error_flag == 0)
            {
            $("#btn_submit").click();
            }
        });
    </script>
    <script>
        $(document).on('keydown','.code', function(e){
            if (e.keyCode == 32) {
            // e.preventDefault();
            return false;
            }
            if (e.keyCode == 32) return false;
        });
        $(document).on('keydown','.inputnumber', function(e){
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) || 
                    // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                        // let it happen, don't do anything
                        return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
        
        $('#date_').daterangepicker({
            minDate: new Date(),
            timePicker: true,
            opens: 'center',
            timePicker24Hour: true,
            locale: {
                format: 'DD-MM-YYYY HH:mm:ss'
            }
        }, function(start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD HH:mm:ss') + ' to ' + end.format('YYYY-MM-DD HH:mm:ss'));
        });
        // select option search
        $('#course_id').select2();


        @include('admin.layout.notification')
    </script>
@endpush