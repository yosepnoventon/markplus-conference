@extends('admin.layout.frame')

@push('breadcrumb')
    <nav class="hk-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Report Promo Used</li>
        </ol>
    </nav>
@endpush

@section('title', '| Report Promo Used')

@section('content')

    <!-- Container -->
    <div class="container-fluid mt-xl-50 mt-sm-30 mt-15">
        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title" style="margin-bottom: 35px">
                        Report Promo Used
                    </h5>
                    <div class="row">
                        <div class="col-sm">
                            <div class="table-wrap table-responsive">
                                <table id="promos" class="table table-bordered table-striped table-saw" width="100%">
                                    <thead align="left">
                                        <tr>
                                            <th width="5%">#</th>
                                            <th width="30%">Code Promo</th>
                                            <th width="10%">Quota</th>
                                            <th width="10%">Used</th>
                                            <th width="20%">Status</th>
                                            <th width="25%">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- /Row -->
    </div>
    <!-- /Container -->

@endsection

@push('script')
    <script>
        var oTable;
        $(document).ready(function()
        {
            oTable = $('#promos').DataTable({
                processing: true,
                serverSide: true,
                order:  [[ 0, "asc" ]],
                searching: true,
                //dom: 'Bfrtip',
                //buttons: [
                //    'copy', 'csv', 'excel', 'pdf', 'print'
                //],
                responsive: true,
                ajax: '{!! route('report.promo-used.data') !!}',
                columns: [
                    { data: "rownum", name: "rownum", searchable: false },
                    { data: 'code', name: 'code'},
                    { data: 'quota', name: 'quota', className: 'text-center'},
                    { data: 'quota_used', name: 'quota_used', className: 'text-center'},
                    { data: 'status', name: 'status'},
                    { data: 'action', name: 'action', orderable: false, searchable: false }
                ]
            });
        });

        @include('admin.layout.notification')
    </script>
@endpush
