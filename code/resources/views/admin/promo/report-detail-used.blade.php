@extends('admin.layout.frame')

@push('breadcrumb')
    <nav class="hk-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Report Promo Details Used</li>
        </ol>
    </nav>
@endpush

@section('title', '| Report Promo Details Used')

@section('content')

    <!-- Container -->
    <div class="container-fluid mt-xl-50 mt-sm-30 mt-15">
        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title" style="margin-bottom: 35px">
                        Report Promo Details Used
                        @if(\Request::get('code'))
                            <span>
                                <a href="{{ url('/admin/report/promo-used') }}" class="btn bg-red" style="color: white; float:right;">
                                    <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                                </a>
                                <a href="{{ url('/admin/report/promo-detail-used') }}" class="btn bg-blue" style="color: white; margin-right: 10px;">
                                    <i class="fa fa-refresh" aria-hidden="true"></i> Refresh
                                </a>
                            </span>
                        @endif
                    </h5>
                    <div class="row">
                        <div class="col-sm">
                            <div class="table-wrap table-responsive">
                                <table id="promos" class="table table-bordered table-striped table-saw" width="100%">
                                    <thead align="left">
                                        <tr>
                                            <th width="5%">#</th>
                                            <th width="10%">Code</th>
                                            <th width="10%">Name</th>
                                            <th width="10%">Email</th>
                                            <th width="10%">Learn Method</th>
                                            <th width="10%">Course</th>
                                            <th width="10%">Transaction Date</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- /Row -->
    </div>
    <!-- /Container -->

@endsection

@push('script')
    <script>
        var oTable;
        $(document).ready(function()
        {
            oTable = $('#promos').DataTable({
                processing: true,
                serverSide: true,
                order:  [[ 0, "asc" ]],
                searching: true,
                //dom: 'Bfrtip',
                //buttons: [
                //    'copy', 'csv', 'excel', 'pdf', 'print'
                //],
                responsive: true,
                ajax: '{!! route('report.promo-detail-use.data', ['code' => (\Request::get('code') ? \Request::get('code') : "")]) !!}',
                columns: [
                    { data: "rownum", name: "rownum", searchable: false },
                    { data: 'promo_code', name: 'promo_code'},
                    { data: 'name', name: 'name'},
                    { data: 'email', name: 'email'},
                    { data: 'learning_method', name: 'learning_method'},
                    { data: 'course', name: 'course'},
                    { data: 'created_at', name: 'created_at'},
                ]
            });
        });

        @include('admin.layout.notification')
    </script>
@endpush
