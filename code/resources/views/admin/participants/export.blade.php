<!DOCTYPE html>
<html>
<head>
<title>Report Voucher</title>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th>No</th>
                <th>Code Voucher</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Agency Name</th>
            </tr>
        </thead>
        <tbody>
        @php $no = 1 @endphp
        @foreach($vouchers as $voucher)
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $voucher->code_voucher }}</td>
                <td>{{ $voucher->name }}</td>
                <td>{{ $voucher->email }}</td>
                <td>{{ $voucher->phone }}</td>
                <td>{{ $voucher->agency_name }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</body>
</html>