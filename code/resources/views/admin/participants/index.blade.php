@extends('admin.layout.frame')

@push('breadcrumb')
    <nav class="hk-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Participants</li>
        </ol>
    </nav>
@endpush

@section('title', '| About Us')

@section('content')

    <!-- Container -->
    <div class="container-fluid mt-xl-50 mt-sm-30 mt-15">
        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title" style="margin-bottom: 35px">
                        Participants
                        <span>
                            <a href="{{ url('/admin/participants/export') }}" class="btn bg-info" style="color: white; float:right">
                                <i class="fa fa-download" aria-hidden="true"></i> Export
                            </a>
                        </span>
                    </h5>
                    <div class="row">
                        <div class="col-sm">
                            <div class="table-wrap table-responsive">
                                <table id="participans" class="table table-bordered table-striped table-saw" cellspacing="0">
                                    <thead align="left">
                                        <tr>
                                            <th width="10%">No</th>
                                            <th width="15%">Voucher Code</th>
                                            <th width="20%">Name</th>
                                            <th width="20%">Email</th>
                                            <th width="10%">Phone</th>
                                            <th width="25%">Agency Name</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- /Row -->
    </div>
    <!-- /Container -->

@endsection

@push('script')
    <script>
        var oTable;
        $(document).ready(function()
        {
            oTable = $('#participans').DataTable({
                processing: true,
                serverSide: true,
                order:  [[ 0, "asc" ]],
                searching: true,
                //dom: 'Bfrtip',
                //buttons: [
                //    'copy', 'csv', 'excel', 'pdf', 'print'
                //],
                responsive: true,
                ajax: '{!! route('participants.data') !!}',
                columns: [
                    { data: "rownum", name: "rownum", searchable: false },
                    { data: 'code_voucher', name: 'code_voucher'},
                    { data: 'name', name: 'name' },
                    { data: 'email', name: 'email' },
                    { data: 'phone', name: 'phone' },
                    { data: 'agency_name', name: 'agency_name' }
                ]
            });
        });

        @include('admin.layout.notification')
    </script>
@endpush
