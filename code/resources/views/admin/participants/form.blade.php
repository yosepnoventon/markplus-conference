<div class="row">
    <div class="col-md-6 col-sm-12">
        <div class="form-group">
            <label for="title">Code Voucher : </label>&nbsp;<label class="code_required"></label>
            <div class="input-group">
                {{-- <div class="input-group-prepend">
                    <label class="input-group-text" for="code_voucher">C</label>
                </div> --}}
                {!! Form::text('code_voucher', null, ['class' => 'form-control mr-1 code_voucher', 'id' => 'code_voucher', 'required' => 'required']) !!}
                <button type="button" class="btn btn-warning" id="randomCode">Random</button>
                <button type="button" class="btn btn-primary" id="checkCode">Check</button>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="form-group">
            <label for="email">Email :</label>
            <div class="input-group">
                {!! Form::text('email', null, ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="form-group">
            <label for="quota">Quota :</label>
            <div class="input-group">
                {!! Form::text('quota', null, ['class' => 'form-control inputnumber']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn bg-green', 'style' => 'color: white'])!!}
            <input type="reset" value="Clear" class="btn bg-grey" style="color: white">
        </div>
    </div>
</div>

@push('script')
    <script>
        $( document ).ready(function() {
            var url = "{{ url('/admin/vocuhers/randomCode') }}";
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    "_token": '{{ csrf_token() }}'
                },
                success: function(datas)
                {
                    $("#code_voucher").val(datas.code);
                },
                error: function(data)
                {
                    window.location.reload();
                }
            });
        });
        $("#randomCode").click(function ()
        {
            var url = "{{ url('/admin/vocuhers/randomCode') }}";
            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: {
                    "_token": '{{ csrf_token() }}'
                },
                success: function(datas)
                {
                    $("#code_voucher").val(datas.code);
                },
                error: function(data)
                {
                    window.location.reload();
                }
            });
        });

        $("#checkCode").click(function ()
        {
            var code = $("#code_voucher").val();
            $(".code_required").html(".....");
            // if code null
            if(code == "")
            {
                $(".code_required").css("color", "#d44950").html("Required");
            }
            else
            {
                var url = "{{ url('/admin/vocuhers/cekCode') }}";
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    data: {
                        "code"  : code,
                        "_token": '{{ csrf_token() }}'
                    },
                    success: function(datas)
                    {
                        if(datas.status == 200)
                        {
                                $(".code_required").css("color", "#4159c3").html("Can Used");
                        }
                        else
                        {
                                $(".code_required").css("color", "#d44950").html("Already Exists");
                        }
                    },
                    error: function(data)
                    {
                        // window.location.reload();
                    }
                }); 
            }
        });

        $(document).on('keydown','.inputnumber', function(e){
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) || 
                    // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                        // let it happen, don't do anything
                        return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        var maxWords = 5;
		$('.code_voucher').on('keypress', function(event) {
            $(".code_required").html("");
			var $this, wordcount;
			$this = $(this);
			wordcount = $this.val().length;
            console.log(wordcount);
			if (event.which == 13) {
				event.preventDefault();
				var s = $(this).val();
				$(this).val(s+"\n");
			}
			if (wordcount > maxWords) {
                $(".code_required").css("color", "#d44950").html("Code Voucher maximum 6 characters");
				return false;
			}
		});

    </script>
@endpush
