@extends('admin.layout.frame')

@push('breadcrumb')
<nav class="hk-breadcrumb" aria-label="breadcrumb">
    <ol class="breadcrumb breadcrumb-light bg-transparent">
        <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
        <li class="breadcrumb-item active" aria-current="page">Transactions</li>
    </ol>
</nav>
@endpush

@section('title', '| Dashboard')

@section('content')

<!-- Container -->
<div class="container-fluid mt-xl-50 mt-sm-30 mt-15">
    <!-- Row -->
    <div class="row">
        <div class="col-xl-12">
            <section class="hk-sec-wrapper">
                <h5 class="hk-sec-title">
                    Transactions
                    
                    <span>&nbsp;&nbsp;
                        <a onclick="syncExpired()" id="courses-subscriptions" class="btn bg-success" style="color: white; float:right;">
                            <i class="fa fa-refresh" aria-hidden="true"></i> Sync Expired E-Learning & Certificate
                        </a>
                    </span>
                    <span>&nbsp;&nbsp;
                        <a onclick="syncCertificate()" id="refresh-courses" class="btn bg-success" style="color: white; float:right; margin-right: 10px;">
                            <i class="fa fa-refresh" aria-hidden="true"></i> Sync Certificate
                        </a>
                    </span>
                </h5>
                <br>
                <a href="{{ url('admin/transaction') }}" class="btn btn-{{ Request::is('admin/transaction') ? 'success' : 'primary'}}">All</a>
                @foreach($learnmethods as $learnmethod)
                    <a href="{{ url('admin/transaction/'.$learnmethod->slug) }}" class="btn btn-{{ Request::is('admin/transaction/'.$learnmethod->slug) ? 'success' : 'primary'}}">{{ $learnmethod->title }}</a>
                @endforeach
                <br>
                <br>
                <div class="row">
                    <div class="col-sm">
                        <div class="table-wrap table-responsive">
                            <table id="transaction" class="table table-bordered table-striped table-saw" width="100%">
                                <thead align="left">
                                    <tr>
                                        <th width="5%">#</th>
                                        <th width="15%">Order ID</th>
                                        <th width="15%">Customer</th>
                                        <th width="15%">Learn Method</th>
                                        <th width="15%">price</th>
                                        <th width="15%">Status</th>
                                        <th width="10%">Created</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- /Row -->
</div>
<!-- /Container -->
@endsection

@push('script')
<script>
    var oTable;
        $(document).ready(function()
        {
            oTable = $('#transaction').DataTable({
                processing: true,
                serverSide: true,
                order:  [[ 0, "desc" ]],
                searching: true,
                //dom: 'Bfrtip',
                //buttons: [
                //    'copy', 'csv', 'excel', 'pdf', 'print'
                //],
                responsive: true,
                    @if(Request::is('admin/transaction'))
                        ajax: '{!! url('admin/transaction/datatable') !!}'+'?method=all',
                    @endif
                    @foreach($learnmethods as $learnmethod)
                        @if(\Request::is('admin/transaction/'.$learnmethod->slug))
                            ajax: '{!! url('admin/transaction/datatable') !!}'+'?method={{ $learnmethod->id }}',
                        @endif
                    @endforeach
                columns: [
                    { data: "rownum", name: "rownum", searchable: false },
                    { data: 'invoice', name: 'invoice' },
                    { data: 'first_name', name: 'first_name' },
                    { data: 'learn_method', name: 'learn_method' },
                    { data: 'price', name: 'price', className: 'text-right'},
                    { data: 'status', name: 'status' },
                    { data: 'is_admin', name: 'is_admin' },
                    { data: 'action', name: 'action', orderable: false, searchable: false }
                ]
            });
        });

        $('#time').daterangepicker({
            minDate: new Date(),
            timePicker: true,
            opens: 'center',
            timePicker24Hour: true,
            locale: {
                format: 'DD-MM-YYYY HH:mm:ss'
            }
        }, function(start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD HH:mm:ss') + ' to ' + end.format('YYYY-MM-DD HH:mm:ss'));
        });

        function syncCertificate()
        {
            $.ajax({
                type: "POST",
                url: "{{ url('admin/synco_certificate') }}",
                dataType: "json",
                data: {
                    cache: "false",
                    _token: '{{ csrf_token() }}'
                },
                beforeSend: function() {
                    swal({
                        title: "Wait...",
                        text: "this may take a few moments",
                        showConfirmButton: false,
                        allowOutsideClick: false,
                    });
                },
                success: function (msg)
                {
                    console.log(msg);
                    swal({
                        type: msg.alert_type,
                        title: msg.title,
                        text: msg.message,
                        timer: 2000,
                        showConfirmButton: false
                    });
                    oTable.draw();
                }
            });
        }

        function syncExpired()
        {
            $.ajax({
                type: "POST",
                url: "{{ url('admin/remove_students_from_class') }}",
                dataType: "json",
                data: {
                    cache: "false",
                    _token: '{{ csrf_token() }}'
                },
                beforeSend: function() {
                    swal({
                        title: "Wait...",
                        text: "this may take a few moments",
                        showConfirmButton: false,
                        allowOutsideClick: false,
                    });
                },
                success: function (msg)
                {
                    console.log(msg);
                    swal({
                        type: msg.alert_type,
                        title: msg.title,
                        text: msg.message,
                        timer: 2000,
                        showConfirmButton: false
                    });
                    oTable.draw();
                }
            });
        }
        
        @include('admin.layout.notification')
</script>
@endpush