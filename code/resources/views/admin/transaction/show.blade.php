@extends('admin.layout.frame')

@push('breadcrumb')
<nav class="hk-breadcrumb" aria-label="breadcrumb">
    <ol class="breadcrumb breadcrumb-light bg-transparent">
        <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
        <li class="breadcrumb-item active" aria-current="page">Transaction Detail</li>
    </ol>
</nav>
@endpush

@section('title', '| Customers')

@section('content')

<!-- Container -->
<div class="container-fluid mt-xl-50 mt-sm-30 mt-15">
    <!-- Row -->
    <div class="row">
        <div class="col-xl-12">
            <section class="hk-sec-wrapper">
                <h5 class="hk-sec-title" style="margin-bottom: 35px">
                    Transaction Detail
                </h5>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-wrap table-responsive">
                            <table id="customers" class="table table-bordered table-striped table-saw">
                                <tr>
                                    <th>Customer</th>
                                    <th>{{ $order->first_name.' '.$order->last_name }}</th>
                                </tr>
                                <tr>
                                    <th>Order ID</th>
                                    <th>{{ $order->invoice }}</th>
                                </tr>
                                <tr>
                                    <th>Learn Method</th>
                                        <th>{{ $learnmethod }}</th>
                                </tr>
                                @if($order->course != null)
                                    <tr>
                                        <th>Category Course</th>
                                        <th>{{ $order->category_matrix }}</th>
                                    </tr>
                                    <tr>
                                        <th>Course</th>
                                        <th>{{ $order->title }}</th>
                                    </tr>
                                    <tr>
                                        <th>Booking Code</th>
                                        <th>{{ $order->booking_code }}</th>
                                    </tr>
                                @else
                                    <tr>
                                        <th>Course</th>
                                        <th>All Course E-LEARNING</th>
                                    </tr>
                                @endif
                                <tr>
                                    <th>Price</th>
                                    <th>{{ $order->price ? number_format($order->price,2,",",".") : 0 }}</th>
                                </tr>
                                @if($order->course == null)
                                    <tr>
                                        <th>Start Date</th>
                                        @if($order->start_date)
                                            <th>{{ \Carbon\Carbon::parse($order->start_date)->format('d-F-Y H:i:s') }}</th>
                                        @else
                                            <th>-</th>
                                        @endif
                                    </tr>
                                    <tr>
                                        <th>Expire date </th>
                                        @if($order->expire_date)
                                            <th>{{ \Carbon\Carbon::parse($order->expire_date)->format('d-F-Y H:i:s') }}</th>
                                        @else
                                            <th>-</th>
                                        @endif
                                    </tr>
                                @elseif($order->course != null && $order->detail_course == null)
                                    <tr>
                                        <th>Start Date</th>
                                        @if($order->start_date)
                                            <th>{{ \Carbon\Carbon::parse($order->start_date)->format('d-F-Y H:i:s') }}</th>
                                        @else
                                            <th>-</th>
                                        @endif
                                    </tr>
                                    <tr>
                                        <th>Expire date </th>
                                        @if($order->expire_date)
                                            <th>{{ \Carbon\Carbon::parse($order->expire_date)->format('d-F-Y H:i:s') }}</th>
                                        @else
                                            <th>-</th>
                                        @endif
                                    </tr>
                                @endif
                                <tr>
                                    <th>Status</th>
                                    <th>{{ $order->status_code.' - '.$order->status }}</th>
                                </tr>
                                <tr>
                                    <th>Created</th>
                                    @if($order->created_by != null)
                                        <th>By Admin</th>
                                    @else
                                        <th>By Customer</th>
                                    @endif
                                </tr>
                                <tr>
                                    <th>Doku Response</th>
                                    <th>
                                        @if($order->arrayPayment)
                                            @foreach(json_decode($order->arrayPayment) as $title => $value)
                                                {{ $title.' : '.$value }} <br>
                                            @endforeach
                                        @endif
                                    </th>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h5 class="hk-sec-title" style="margin: 15px 0 15px 0;">
                            Transaction Logs
                        </h5>
                        <div class="table-wrap table-responsive">
                            <table id="customers" class="table table-bordered table-striped table-saw">
                                <tr>
                                    <th>No</th>
                                    <th>type</th>
                                    <th>status</th>
                                    <th>status code</th>
                                    <th>message</th>
                                </tr>
                                @php $no=1 @endphp
                                @foreach($order_logs as $ol)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $ol->type }}</td>
                                        <td>{{ $ol->status }}</td>
                                        <td>{{ $ol->status_code }}</td>
                                        <td>{{ $ol->description }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- /Row -->
</div>
<!-- /Container -->

@endsection

