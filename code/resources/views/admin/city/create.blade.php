@extends('admin.layout.frame')

@push('breadcrumb')
<nav class="hk-breadcrumb" aria-label="breadcrumb">
    <ol class="breadcrumb breadcrumb-light bg-transparent">
        <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ url('/admin/cities') }}">Cities</a></li>
        <li class="breadcrumb-item active" aria-current="page">Create City</li>
    </ol>
</nav>
@endpush

@section('title', '| Create City')

@section('content')
    <!-- Container -->
    <div class="container-fluid mt-xl-50 mt-sm-30 mt-15">
        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title" style="margin-bottom: 15px">
                        Create City
                        <span>
                            <a href="{{ url('/admin/cities') }}" class="btn bg-dark" style="color: white; float:right">
                                <i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back
                            </a>
                        </span>
                    </h5>
                    <div class="row">
                        <div class="col-sm">

                            @if ($errors->any())
                                <ul class="alert alert-danger" style="margin-top:15px">
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif

                            {!! Form::open(['url' => '/admin/cities', 'class' => 'form-horizontal', 'files' => true]) !!}

                            @include ('admin.city.form')

                            {!! Form::close() !!}
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- /Row -->
    </div>
    <!-- /Container -->
@endsection

@push('script')
    <script>
        @include('admin.layout.notification')
    </script>
@endpush
