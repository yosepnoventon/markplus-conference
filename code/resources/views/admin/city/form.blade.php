
<div class="form-group">
    <label for="name">Name</label>
    <div class="input-group">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn bg-green', 'style' => 'color: white'])!!}
    <input type="reset" value="Clear" class="btn bg-grey" style="color: white">
</div>
