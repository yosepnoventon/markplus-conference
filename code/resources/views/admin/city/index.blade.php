@extends('admin.layout.frame')

@push('breadcrumb')
    <nav class="hk-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Cities</li>
        </ol>
    </nav>
@endpush

@section('title', '| Cities')

@section('content')

    <!-- Container -->
    <div class="container-fluid mt-xl-50 mt-sm-30 mt-15">
        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title" style="margin-bottom: 35px">
                        Cities
                        <span>
                            <a href="{{ url('/admin/cities/create') }}" class="btn bg-success" style="color: white; float:right">
                                <i class="fa fa-plus" aria-hidden="true"></i> Create
                            </a>
                        </span>
                    </h5>
                    <div class="row">
                        <div class="col-sm">
                            <div class="table-wrap table-responsive">
                                <table id="cities" class="table table-bordered table-striped table-saw">
                                    <thead align="left">
                                        <tr>
                                            <th width="20%">#</th>
                                            <th width="60%">Name</th>
                                            <th width="20%">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- /Row -->
    </div>
    <!-- /Container -->

@endsection

@push('script')
    <script>
        var oTable;
        $(document).ready(function()
        {
            oTable = $('#cities').DataTable({
                processing: true,
                serverSide: true,
                order:  [[ 0, "asc" ]],
                searching: true,
                //dom: 'Bfrtip',
                //buttons: [
                //    'copy', 'csv', 'excel', 'pdf', 'print'
                //],
                responsive: true,
                ajax: '{!! route('cities.data') !!}',
                columns: [
                    { data: "rownum", name: "rownum", searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'action', name: 'action', orderable: false, searchable: false }
                ]
            });
        });

        function deleteData(id)
        {
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this imaginary data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes!",
                cancelButtonText: "No!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: '{{ route("cities.index") }}/' + id + '?' + $.param({"_token" : '{{ csrf_token() }}' }),
                        data: {_method: 'delete'},
                        complete: function (msg) {
                            oTable.draw();
                            swal("Success", "Your data already deleted", "success");
                        }
                    });
                } else {
                    swal("Cancelled", "Your imaginary data is safe :)", "error");
                }
            });
        }

        @include('admin.layout.notification')
    </script>
@endpush
