<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <title>{{ config('app.name', 'Admin CMS') }} Admin @yield('title')</title>
        {{--  <meta name="description" content="A responsive bootstrap 4 admin dashboard template by hencework" />  --}}

        <!-- Favicon -->
        <link rel="icon" href="{{ url('/assets/favicon.png') }}" type="image/png">

       @include('admin.layout.style')

    </head>

    <body>
        <!-- Preloader -->
        <div class="preloader-it" style="margin-top: -21px;">
            <div class="loader-pendulums"></div>
        </div>
        <!-- /Preloader -->

        <!-- HK Wrapper -->
        <div class="hk-wrapper hk-vertical-nav hk-nav-toggle">

            <!-- Top Navbar -->
            <nav class="navbar navbar-expand-xl navbar-light fixed-top hk-navbar" style="z-index:100 !important">
                <a id="navbar_toggle_btn" class="navbar-toggle-btn nav-link-hover" href="javascript:void(0);">
                    <i class="ion ion-ios-menu"></i></a>
                <a class="navbar-brand" href="{{ url('/admin') }}">
                    <img class="brand-img d-inline-block" src="{{ url('/assets/web//img/logo-white.png') }}" width="120" alt="brand" />
                </a>
                <ul class="navbar-nav hk-navbar-content">
                    <li class="nav-item dropdown dropdown-authentication">
                        <a class="nav-link dropdown-toggle no-caret" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="media">
                                <div class="media-img-wrap">
                                    <div class="avatar">
                                        <img src="{{ url('/assets/admin/dist/img/avatar12.jpg') }}" alt="user" class="avatar-img rounded-circle">
                                    </div>
                                    <span class="badge badge-success badge-indicator"></span>
                                </div>
                                <div class="media-body">
                                    <span>{{ Auth::guard('admin')->user()->name }}<i class="zmdi zmdi-chevron-down"></i></span>
                                </div>
                            </div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                            <a class="dropdown-item" href="{{ url('/admin/profile') }}"><i class="dropdown-icon zmdi zmdi-account"></i><span>Profile</span></a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ url('/admin/logout') }}"><i class="dropdown-icon zmdi zmdi-power"></i><span>Log out</span></a>
                        </div>
                    </li>
                </ul>
            </nav>
            <!-- /Top Navbar -->

            <!-- Vertical Nav -->
            <nav class="hk-nav hk-nav-dark">
                <a href="javascript:void(0);" id="hk_nav_close" class="hk-nav-close">
                    <span class="feather-icon"><i data-feather="x"></i></span>
                </a>
                <div class="nicescroll-bar">
                    <div class="navbar-nav-wrap">

                        @include('admin.layout.menu')

                    </div>
                </div>
            </nav>
            <div id="hk_nav_backdrop" class="hk-nav-backdrop"></div>
            <!-- /Vertical Nav -->

            <!-- Main Content -->
            <div class="hk-pg-wrapper">

                @stack('breadcrumb')

                @yield('content')

                @include('admin.layout.footer')

            </div>
            <!-- /Main Content -->

        </div>
        <!-- /HK Wrapper -->

       @include('admin.layout.script')

       @stack('script')

    </body>

</html>
