<!-- jQuery -->
<script src="{{ url('/assets/admin/vendors/jquery/dist/jquery.min.js') }}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ url('/assets/admin/vendors/popper.js/dist/umd/popper.min.js') }}"></script>
<script src="{{ url('/assets/admin/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>

<!-- Bootstrap Tagsinput JavaScript -->
{{--  <script src="{{ url('/assets/admin/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>  --}}

<!-- Bootstrap Input spinner JavaScript -->
<script src="{{ url('/assets/admin/vendors/bootstrap-input-spinner/src/bootstrap-input-spinner.js') }}"></script>

<!-- Counter Animation JavaScript -->
<script src="{{ url('/assets/admin/vendors/waypoints/lib/jquery.waypoints.min.js') }}"></script>
<script src="{{ url('/assets/admin/vendors/jquery.counterup/jquery.counterup.min.js') }}"></script>

<!-- Data Table JavaScript -->
<script src="{{ url('/assets/admin/vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="{{ url('/assets/admin/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ url('/assets/admin/vendors/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
<script src="{{ url('/assets/admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ url('/assets/admin/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ url('/assets/admin/vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ url('/assets/admin/vendors/jszip/dist/jszip.min.js') }}"></script>
<script src="{{ url('/assets/admin/vendors/pdfmake/build/pdfmake.min.js') }}"></script>
<script src="{{ url('/assets/admin/vendors/pdfmake/build/vfs_fonts.js') }}"></script>
<script src="{{ url('/assets/admin/vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ url('/assets/admin/vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ url('/assets/admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>

<!-- Daterangepicker JavaScript -->
<script src="{{ url('/assets/admin/vendors/moment/min/moment.min.js') }}"></script>
<script src="{{ url('/assets/admin/vendors/daterangepicker/daterangepicker.js') }}"></script>

<!-- Fancy Dropdown JS -->
<script src="{{ url('/assets/admin/dist/js/dropdown-bootstrap-extended.js') }}"></script>

<!-- FeatherIcons JavaScript -->
<script src="{{ url('/assets/admin/dist/js/feather.min.js') }}"></script>

<!-- Ion JavaScript -->
<script src="{{ url('/assets/admin/vendors/ion-rangeslider/js/ion.rangeSlider.min.js') }}"></script>

<!-- Jasny-bootstrap  JavaScript -->
<script src="{{ url('/assets/admin/vendors/jasny-bootstrap/dist/js/jasny-bootstrap.min.js') }}"></script>

<!-- Owl JavaScript -->
<script src="{{ url('/assets/admin/vendors/owl.carousel/dist/owl.carousel.min.js') }}"></script>

<!-- Select2 JavaScript -->
<script src="{{ url('/assets/admin/vendors/select2/dist/js/select2.full.min.js') }}"></script>

<!-- Slimscroll JavaScript -->
<script src="{{ url('/assets/admin/dist/js/jquery.slimscroll.js') }}"></script>

<!-- Sparkline JavaScript -->
<script src="{{ url('/assets/admin/vendors/jquery.sparkline/dist/jquery.sparkline.min.js') }}"></script>

<!-- Modaal popup -->
<script src="{{ url('/assets/admin/js/modaal.min.js') }}"></script>

<!-- Sweetalert -->
<script src="{{ url('/assets/admin/js/sweetalert.min.js') }}"></script>
@include('sweet::alert')

<!-- Pickr JavaScript -->
<script src="{{ url('/assets/admin/vendors/pickr-widget/dist/pickr.min.js') }}"></script>

<!-- Toastr JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>

<!-- Toggles JavaScript -->
<script src="{{ url('/assets/admin/vendors/jquery-toggles/toggles.min.js') }}"></script>
<script src="{{ url('/assets/admin/dist/js/toggle-data.js') }}"></script>

<!-- Tinymce JavaScript -->
<script src="{{ url('/assets/admin/vendors/tinymce/tinymce.min.js') }}"></script>

<!-- Vector Maps JavaScript -->
<script src="{{ url('/assets/admin/vendors/vectormap/jquery-jvectormap-2.0.3.min.js') }}"></script>
<script src="{{ url('/assets/admin/vendors/vectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<script src="{{ url('/assets/admin/vendors/vectormap/jquery-jvectormap-de-merc.js') }}"></script>
<script src="{{ url('/assets/admin/dist/js/vectormap-data.js') }}"></script>

<!-- Init JavaScript -->
<script src="{{ url('/assets/admin/dist/js/init.js') }}"></script>

