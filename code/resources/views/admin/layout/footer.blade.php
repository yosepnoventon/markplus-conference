<!-- Footer -->
<div class="hk-footer-wrap container">
    <footer class="footer">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <p>Pampered by {{ config('app.name') }} © {{ date('Y') }}</p>
            </div>
        </div>
    </footer>
</div>
<!-- /Footer -->
