<!-- vector map CSS -->
<link href="{{ url('/assets/admin/vendors/vectormap/jquery-jvectormap-2.0.3.css') }}" rel="stylesheet" type="text/css" />

<!-- Toggles CSS -->
<link href="{{ url('/assets/admin/vendors/jquery-toggles/css/toggles.css') }}" rel="stylesheet" type="text/css">
<link href="{{ url('/assets/admin/vendors/jquery-toggles/css/themes/toggles-light.css') }}" rel="stylesheet" type="text/css">

<!-- Toastr CSS -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css">

<!-- Toggle Checkbox -->
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

<!-- ION CSS -->
<link href="{{ url('/assets/admin/vendors/ion-rangeslider/css/ion.rangeSlider.css') }}" rel="stylesheet" type="text/css">
<link href="{{ url('/assets/admin/vendors/ion-rangeslider/css/ion.rangeSlider.skinHTML5.css') }}" rel="stylesheet" type="text/css">

<!-- select2 CSS -->
<link href="{{ url('/assets/admin/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />

<!-- Pickr CSS -->
<link href="{{ url('/assets/admin/vendors/pickr-widget/dist/pickr.min.css') }}" rel="stylesheet" type="text/css" />

<!-- Daterangepicker CSS -->
<link href="{{ url('/assets/admin/vendors/daterangepicker/daterangepicker.css') }}" rel="stylesheet" type="text/css" />

<!-- Custom CSS -->
<link href="{{ url('/assets/admin/dist/css/style.css') }}" rel="stylesheet" type="text/css">

<!-- Data Table CSS -->
<link href="{{ url('/assets/admin/vendors/datatables.net-dt/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('/assets/admin/vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css') }}" rel="stylesheet" type="text/css" />

<!-- Modaal popup -->
<link rel="stylesheet" href="{{ url('/assets/admin/css/modaal.min.css') }}">

<!-- SweatAlert -->
<link rel="stylesheet" href="{{ url('/assets/admin/css/sweetalert.css') }}">
