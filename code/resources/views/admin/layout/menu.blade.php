
<ul class="navbar-nav flex-column">
    <li class="nav-item {{ Request::is('admin') ? 'active' : ''}}">
        <a class="nav-link" href="{{ url('/admin') }}">
            <i class="ion ion-ios-keypad"></i>
            <span class="nav-link-text">Dashboard</span>
        </a>
    </li>
</ul>

<hr class="nav-separator">

<ul class="navbar-nav flex-column">
    <li class="nav-item {{ Request::is('admin/vouchers*') ? 'active' : ''}}">
        <a class="nav-link" href="{{ url('/admin/vouchers') }}">
            <i class="fa fa-gift"></i>
            <span class="nav-link-text">Voucher</span>
        </a>
    </li>
</ul>

<ul class="navbar-nav flex-column">
    <li class="nav-item {{ Request::is('admin/participants*') ? 'active' : ''}}">
        <a class="nav-link" href="{{ url('/admin/participants') }}">
            <i class="fa fa-users"></i>
            <span class="nav-link-text">Participan</span>
        </a>
    </li>
</ul>

{{-- <div class="nav-header">
    <span>Master Data</span>
    <span>MD</span>
</div>
<ul class="navbar-nav flex-column">
    <li class="nav-item {{ (Request::is('admin/users*') ||  Request::is('admin/vouchers*')) ? 'active' : ''}}">
        <a class="nav-link" href="javascript:void(0);" data-toggle="collapse" data-target="#master">
            <i class="ion ion-ios-wallet"></i>
            <span class="nav-link-text">Master</span>
        </a>
        <ul id="master" class="nav flex-column collapse collapse-level-1 {{ (Request::is('admin/vouchers*') || Request::is('admin/users*')) ? 'show' : ''}}">
            <li class="nav-item">
                <ul class="nav flex-column">
                    <li class="nav-item {{ Request::is('admin/users*') ? 'active' : ''}}">
                        <a class="nav-link" href="{{ url('/admin/users') }}">Users</a>
                    </li>
                </ul>
            </li>
        </ul>
    </li>
</ul> --}}