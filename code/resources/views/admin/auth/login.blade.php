@extends('admin.layout.auth')

@section('title', '| Admin Login')

@section('content')

<div class="row">
    <div class="col-xl-12 pa-0">
        <div class="auth-form-wrap pt-xl-0 pt-70">
            <div class="auth-form w-xl-30 w-lg-55 w-sm-75 w-100">
                <a class="auth-brand text-center d-block mb-20" href="#">
                    <img class="brand-img" src="{{ url('/assets/web//img/logo-white.png') }}" width="300" alt="logo" />
                </a>

                <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/login') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <input class="form-control" placeholder="Email" type="email" value="{{ old('email') }}" name="email">

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif

                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <input class="form-control" placeholder="Password" type="password" name="password">

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif

                        </div>
                    </div>

                    <div class="custom-control custom-checkbox mb-25">
                        <input class="custom-control-input" id="remember" type="checkbox" name="remember">
                        <label class="custom-control-label font-14" for="remember">Keep me logged in</label>
                    </div>

                    <button class="btn btn-indigo btn-block" type="submit">Login</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
