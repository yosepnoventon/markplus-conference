@extends('admin.layout.frame')

@push('breadcrumb')
<nav class="hk-breadcrumb" aria-label="breadcrumb">
    <ol class="breadcrumb breadcrumb-light bg-transparent">
        <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ url('/admin/customers') }}">Customers</a></li>
        <li class="breadcrumb-item active" aria-current="page">Create Customers</li>
    </ol>
</nav>
@endpush

@section('title', '| Create Customer')

@section('content')
    <!-- Container -->
    <div class="container-fluid mt-xl-50 mt-sm-30 mt-15">
        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title" style="margin-bottom: 15px">Create Course
                        <span>
                            <a href="{{ url('/admin/customers') }}" class="btn bg-dark" style="color: white; float:right">
                                <i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back
                            </a>
                        </span>
                    </h5>
                    <div class="row">
                        <div class="col-sm-12">

                            @if ($errors->any())
                                <ul class="alert alert-danger" style="margin-top:15px">
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif

                            {!! Form::open([
                                'url' => '/admin/customers',
                                'class' => 'form-horizontal',
                            ]) !!}

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-group">
                                                <label for="instructor">First Name :</label>
                                                <div class="input-group">
                                                    {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-group">
                                                <label for="instructor">Last Name :</label>
                                                <div class="input-group">
                                                    {{-- <div class="input-group-prepend">
                                                        <label class="input-group-text" for="instructor">Last Name</label>
                                                    </div> --}}
                                                    {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-group">
                                                <label for="instructor">Email :</label>
                                                <div class="input-group">
                                                    {!! Form::text('email', null, ['class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-group">
                                                <label for="instructor">Phone :</label>
                                                <div class="input-group">
                                                    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-group">
                                                <label for="instructor">Company :</label>
                                                <div class="input-group">
                                                    {!! Form::text('company', null, ['class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-group">
                                                <label for="instructor">Sales :</label>
                                                <div class="input-group">
                                                    {!! Form::select('sales_id', $sales, Request::old('sales'), ['class' => 'form-control custom-select']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-group">
                                                <label for="instructor">Password :</label>
                                                <div class="input-group">
                                                    {!! Form::text('password', null, ['class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-group">
                                                <label for="instructor">password confirmation :</label>
                                                <div class="input-group">
                                                    {!! Form::text('password_confirm', null, ['class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn bg-green', 'style' => 'color: white'])!!}
                                        <input type="reset" value="Clear" class="btn bg-grey" style="color: white">
                                    </div>
                                </div>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- /Row -->
    </div>
    <!-- /Container -->
@endsection

@push('script')
    <script>
        @include('admin.layout.notification')
    </script>
@endpush
