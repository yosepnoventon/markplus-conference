@extends('admin.layout.frame')

@push('breadcrumb')
<nav class="hk-breadcrumb" aria-label="breadcrumb">
    <ol class="breadcrumb breadcrumb-light bg-transparent">
        <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ url('/admin/customers') }}">Customers</a></li>
        <li class="breadcrumb-item active" aria-current="page">Transaction</li>
    </ol>
</nav>
@endpush

@section('title', '| Transactions')

@section('content')

<!-- Container -->
<div class="container-fluid mt-xl-50 mt-sm-30 mt-15">
    <!-- Row -->
    <div class="row">
        <div class="col-xl-12">
            <section class="hk-sec-wrapper">
                <h5 class="hk-sec-title" style="margin-bottom: 35px">
                    Customers
                    <span style="float:right">
                        <a href="javascript:void(0);" id="e-learning" class="btn bg-success"
                            style="color: white;"> E-Learning
                        </a>
                        <a href="javascript:void(0);" id="webinar" class="btn bg-success"
                            style="color: white;"> Webinar Learning
                        </a>
                        <a href="javascript:void(0);" id="certificate" class="btn bg-success"
                            style="color: white;"> Certificate
                        </a>
                    </span>
                </h5>
                <div class="row">
                    <div class="col-sm">
                        <div class="table-wrap table-responsive">
                            <table id="transaction" class="table table-bordered table-striped table-saw" width="100%">
                                <thead align="left">
                                    <tr>
                                        <th width="5%">#</th>
                                        <th width="5%">Course ID</th>
                                        <th width="20%">Title</th>
                                        <th width="10%">Category</th>
                                        <th width="15%">Learn Method</th>
                                        <th width="10%">Status</th>
                                        <th width="25%">Message</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- /Row -->
</div>
<!-- /Container -->

<div style="display:none;">
    <div id="e-learning-form">
        <div class="row">
            <div class="col-md-12">
                <h2>Create Order E Learning</h2>
                <label class="input-text">Start Date & Expire Date</label>
                <input type="text" id="time" class="form-control" value="">
            </div>
            <div class="col-md-12">
                <br>
                <input type="button" class="btn bg-success" onclick="eLearning()" value="Submit" style="color:#ffffff;">
            </div>
        </div>
    </div>
    <div id="webinar-form">
        <h2>Create Order Webinar Learning</h2>
        <select name="" id="select-webinar" class="form-control">
            <option value="">Choice Course Webinar</option>
            @foreach($courses as $course)
                @if($course->learn_method == \App\LearnMethod::WEBINAR)
                    <option value="{{ $course->id }}">{{ $course->category_matrix }} - {{ $course->title }}</option>
                @endif
            @endforeach
        </select>
        <br>
        <br>
        <div id="webinar-detail">
            <div class="table-wrap table-responsive">
                <table id="transaction" class="table table-bordered table-striped table-saw" style="border: 1px solid #000000;" width="100%">
                    <thead>
                        <tr>
                            <th width="35%">Kursus</th>
                            <th width="10%">Learn Method</th>
                            <th width="15%">Mulai</th>
                            <th width="15%">Selesai</th>
                            <th width="15%">Tiket</th>
                            <th width="10%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody id="table-webinar-detail">
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    
    <div id="certificate-form">
        <div class="row">
            <div class="col-md-12">
                <h2>Create Order Certificate</h2>
                <select name="" id="select-certificate" class="form-control">
                    <option value="" disabled>Choice Course Certificate</option>
                    @foreach($courses as $course)
                        @if($course->learn_method == \App\LearnMethod::CERTIFICATION)
                            <option value="{{ $course->id }}">{{ $course->category_matrix }} - {{ $course->title }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="col-md-12">
                <br>
                <input type="button" class="btn bg-success" id="btn-certificate" onclick="certificate()" disabled value="Submit" style="color:#ffffff;">
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
    $('#e-learning').click(function(){

    });

    $('#e-learning').modaal({
        content_source: '#e-learning-form'
    });    

    function eLearning(){
        var user_id = "{{ $customer->id }}";
        var time    = $('#time').val();
        var url     = "{{ url('/admin/customers/transaction/order') }}";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                "learn_method"              : {{ \App\LearnMethod::ELEARNING }},
                "user_id"                   : user_id,
                "course_id"                 : '',
                "course_detail_id"          : '',
                "course_ticket_price_id"    : '',
                "time"                      : time,
                "_token"                    : '{{ csrf_token() }}'

            },
            beforeSend: function() {
                swal({
                    title: 'Please Wait...',
                    showConfirmButton: false
                });
            },
            success: function(data){
                console.log(data)
                if(data.code == 200){
                    oTable.draw();
                    $('#e-learning').modaal('close');
                    swal({
                        type: 'success',
                        title: 'success',
                        text: data.message,
                        timer: 2000,
                        showConfirmButton: false
                    });
                }else if(data.code == 202){
                    $('#e-learning').modaal('close');
                    swal({
                        type: 'warning',
                        title: data.message,
                        timer: 2000,
                        showConfirmButton: false
                    });
                }
            },error: function(data){

            }
        });
    }

    $('#webinar').click(function(){
        $('#select-webinar').val('').prop('selected');
        $('#table-webinar-detail').html('');
    });

    $("#webinar").modaal({
        content_source: '#webinar-form',
    });

    $('#select-webinar').on('change', function() {
        var course_id   = this.value;
        var url         = "{{ url('/admin/courses/getDetailCourses') }}/"+course_id;
        var html        = '';
        $.ajax({
            type: "get",
            url: url,
            dataType : 'html',
            data: { "_token" : '{{ csrf_token() }}' },
            success: function(data)
            {
                $('#table-webinar-detail').html(data);
            },
            error: function(data){

            }
        });
    });

    function webinar(course_id, course_detail_id)
    {
        var course_ticket_price_id = $("#select-webinar-"+course_detail_id).val();
        if(course_ticket_price_id == '' || course_ticket_price_id == null)
        {
            swal({
                type: 'warning',
                title: 'Pilih Tiket',
                timer: 1500,
                showConfirmButton: false
            });
            return
        }
        var user_id = "{{ $customer->id }}";
        var url = "{{ url('/admin/customers/transaction/order') }}";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                "learn_method"              : {{ \App\LearnMethod::WEBINAR }},
                "user_id"                   : user_id,
                "course_id"                 : course_id,
                "course_detail_id"          : course_detail_id,
                "course_ticket_price_id"    : course_ticket_price_id,
                "time"                      : '',
                "_token"                    : '{{ csrf_token() }}'
            },
            beforeSend: function() {
                swal({
                    title: 'Please Wait...',
                    showConfirmButton: false
                });
            },
            success: function(data){
                console.log(data);
                if(data.code == 200){
                    oTable.draw();
                    $('#webinar').modaal('close');
                    swal({
                        type: 'success',
                        title: 'success',
                        text: data.message,
                        timer: 2000,
                        showConfirmButton: false
                    });
                }else if(data.code == 202){
                    $('#webinar').modaal('close');
                    swal({
                        type: 'warning',
                        title: data.message,
                        timer: 2000,
                        showConfirmButton: false
                    });
                }
            },error: function(data){

            }
        });
    }

    $('#certificate').click(function(){
        $('#select-certificate').val('').prop('selected');
    });

    $("#certificate").modaal({
        content_source: '#certificate-form',
    });

    $('#select-certificate').on('change', function() {
        $('#btn-certificate').removeAttr('disabled');
    });

    function certificate(){
        var course_id   = $('#select-certificate').val();
        var user_id = "{{ $customer->id }}";
        var url = "{{ url('/admin/customers/transaction/order') }}";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                "learn_method"              : {{ \App\LearnMethod::CERTIFICATION }},
                "user_id"                   : user_id,
                "course_id"                 : course_id,
                "course_detail_id"          : '',
                "course_ticket_price_id"    : '',
                "time"                      : '',
                "_token"                    : '{{ csrf_token() }}'
            },
            beforeSend: function() {
                swal({
                    title: "Wait...",
                    text: "this may take a few moments",
                    showConfirmButton: false,
                    allowOutsideClick: false,
                });
            },
            success: function(data){
                console.log(data);
                if(data.code == 200){
                    oTable.draw();
                    $('#certificate').modaal('close');
                    swal({
                        type: 'success',
                        title: 'success',
                        text: data.message,
                        timer: 2000,
                        showConfirmButton: false
                    });
                }else if(data.code == 202){
                    $('#certificate').modaal('close');
                    swal({
                        type: 'warning',
                        title: data.message,
                        timer: 2000,
                        showConfirmButton: false
                    });
                }
            },error: function(data){

            }
        });
    }
    
    var oTable;
        $(document).ready(function()
        {
            oTable = $('#transaction').DataTable({
                processing: true,
                serverSide: true,
                order:  [[ 0, "asc" ]],
                searching: true,
                //dom: 'Bfrtip',
                //buttons: [
                //    'copy', 'csv', 'excel', 'pdf', 'print'
                //],
                responsive: true,
                ajax: '{!! route('customer-transaction.data', ['id' => $id ]) !!}',
                columns: [
                    { data: "rownum", name: "rownum", searchable: false },
                    { data: 'course_id_matrix', name: 'course_id_matrix' },
                    { data: 'course_title', name: 'course_title' },
                    { data: 'course_category', name: 'course_category' },
                    { data: 'course_learn_method', name: 'course_learn_method' },
                    { data: 'status', name: 'status' },
                    { data: 'message', name: 'message' },
                    { data: 'action', name: 'action', orderable: false, searchable: false }
                ]
            });
        });

        $('#time').daterangepicker({
            minDate: new Date(),
            timePicker: true,
            opens: 'center',
            timePicker24Hour: true,
            locale: {
                format: 'DD-MM-YYYY HH:mm:ss'
            }
        }, function(start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD HH:mm:ss') + ' to ' + end.format('YYYY-MM-DD HH:mm:ss'));
        });
        @include('admin.layout.notification')
</script>
@endpush
