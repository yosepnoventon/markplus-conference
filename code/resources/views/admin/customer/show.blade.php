@extends('admin.layout.frame')

@push('breadcrumb')
<nav class="hk-breadcrumb" aria-label="breadcrumb">
    <ol class="breadcrumb breadcrumb-light bg-transparent">
        <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ url('/admin/customers') }}">Customers</a></li>
        <li class="breadcrumb-item active" aria-current="page">Show Customer</li>
    </ol>
</nav>
@endpush

@section('title', '| Customers')

@section('content')

<!-- Container -->
<div class="container-fluid mt-xl-50 mt-sm-30 mt-15">
    <!-- Row -->
    <div class="row">
        <div class="col-xl-12">
            <section class="hk-sec-wrapper">
                <h5 class="hk-sec-title" style="margin-bottom: 35px">
                    Customers
                </h5>
                <div class="row">
                    <div class="col-sm">
                        <div class="table-wrap table-responsive">
                            <table id="customers" class="table table-bordered table-striped table-saw">
                                <tr>
                                    <th>ID MATRIX</th>
                                    <th>{{ $customer->user_id_matrix }}</th>
                                </tr>
                                <tr>
                                    <th>USERID MATRIX</th>
                                    <th>{{ $customer->userid }}</th>
                                </tr>
                                <tr>
                                    <th>FIRST NAME</th>
                                    <th>{{ $customer->first_name }}</th>
                                </tr>
                                <tr>
                                    <th>LAST NAME</th>
                                    <th>{{ $customer->last_name }}</th>
                                </tr>
                                <tr>
                                    <th>COMPANY</th>
                                    <th>{{ $customer->company }}</th>
                                </tr>
                                <tr>
                                    <th>SALES</th>
                                    <th>{{ $customer->sales_name }}</th>
                                </tr>
                                <tr>
                                    <th>PHONE</th>
                                    <th>{{ $customer->phone }}</th>
                                </tr>
                                <tr>
                                    <th>EMAIL</th>
                                    <th>{{ $customer->email }}</th>
                                </tr>
                                <tr>
                                    <th>VERIFIED</th>
                                    @if($customer->verified == 1)
                                        <th><p style="color: green">Yes</p></th>
                                    @else
                                        <th><p style="color: red">No</p></th>
                                    @endif
                                </tr>
                                <tr>
                                    <th>Created</th>
                                    <th>{{ $customer->created_by ? 'By Admin' : 'By Customer'  }}</th>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- /Row -->
</div>
<!-- /Container -->

@endsection

