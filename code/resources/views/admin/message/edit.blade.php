@extends('admin.layout.frame')

@push('breadcrumb')
<nav class="hk-breadcrumb" aria-label="breadcrumb">
    <ol class="breadcrumb breadcrumb-light bg-transparent">
        <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{ url('/admin/messages') }}">Messages</a></li>
        <li class="breadcrumb-item active" aria-current="page">Edit Message</li>
    </ol>
</nav>
@endpush

@section('title', '| Edit Message')

@section('content')

    <!-- Container -->
    <div class="container-fluid mt-xl-50 mt-sm-30 mt-15">
        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title" style="margin-bottom: 15px">Edit Message
                        <span>
                            <a href="{{ url('/admin/messages') }}" class="btn bg-dark" style="color: white; float:right">
                                <i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back
                            </a>
                        </span>
                    </h5>
                    <div class="row">
                        <div class="col-sm">

                            @if ($errors->any())
                                <ul class="alert alert-danger" style="margin-top:15px">
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif

                            {!! Form::model($message, [
                                'method' => 'PATCH',
                                'url' => ['/admin/messages', $message->id],
                                'class' => 'form-horizontal',
                                'files' => true,
                                'enctype' => 'multipart/form-data'
                            ]) !!}

                            @include ('admin.message.form', ['submitButtonText' => 'Update'])

                            {!! Form::close() !!}
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- /Row -->
    </div>
    <!-- /Container -->

@endsection

@push('script')
    <script>
        @include('admin.layout.notification')
    </script>
@endpush
