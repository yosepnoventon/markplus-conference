<div class="form-group">
    <label for="image">Image</label>
    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
        <div class="input-group-prepend">
            <span class="input-group-text">Upload</span>
        </div>
        <div class="form-control text-truncate" data-trigger="fileinput">
            <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span>
        </div>
        <span class="input-group-append">
            <span class=" btn btn-primary btn-file"><span class="fileinput-new">Select file</span>
            <span class="fileinput-exists">Change</span>
                <input type="file" name="images" onchange="preview_image(event)">
            </span>
            <a href="#" class="btn btn-secondary fileinput-exists" data-dismiss="fileinput" style="margin-left: 5px;">Remove</a>
        </span>
    </div>
    @if(isset($message))
        <img src="{{ url('/upload/message/'.$message->image) }}" id="image-existing" style="width: 150px; margin-top: 20px" />
        <img id="output-image" style="width: 150px; margin-top: 15px" />
        @push('script')
            <script>
                $('#output_image').css('display', 'none');
            </script>
        @endpush
    @else
        <img id="output-image" style="width: 150px; margin-top: 15px"/>
    @endif
</div>

<div class="form-group">
    <label for="title">Title</label>
    <div class="input-group">
        {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
    </div>
</div>

<div class="form-group">
    <label for="preview_title">Preview Title</label>
    <div class="input-group">
        {!! Form::textarea('preview_title', null, ['class' => 'form-control', 'rows' => '5']) !!}
    </div>
</div>

<div class="form-group">
    <label for="description">Description</label>
    <div class="tinymce-wrap">
        {!! Form::textarea('description', null, ['class' => 'tinymce']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn bg-green', 'style' => 'color: white'])!!}
    <input type="reset" value="Clear" class="btn bg-grey" style="color: white">
</div>

@push('script')
    <script>
        tinymce.init({
            selector: '.tinymce',
            height: 220,
            paste_as_text: true,
            branding: false,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
            ],
            toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        });

        function preview_image(event)
        {
            var reader = new FileReader();
            reader.onload = function()
            {
                var output = document.getElementById('output-image');
                $('#output-image').css('display', 'block');
                $('#image-existing').css('display', 'none');
                output.src = reader.result;
            }
            reader.readAsDataURL(event.target.files[0]);
        }

        $('.fileinput-exists').click(function()
        {
            $('#output-image').css('display', 'none');
        });

    </script>
@endpush
