<?php

namespace App\Providers;

use App\Category;
use App\LearnMethod;
use App\Repository\UserRepository;
use App\SocialMedia;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        //\URL::forceScheme('https');

        Blade::directive('price', function ($expression) {
            return "<?php echo number_format($expression, 0, '.', ','); ?>";
        });
    }
}
