<?php

namespace App\Notifications;

use App\User;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use DB;

class UserResetPassword extends Notification
{
    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * Create a new notification instance.
     *
     * @param $token
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        // return (new MailMessage)
        //     ->from(config('mail.username'), 'Markplus Admin')
        //     ->subject('Forgot Password')
        //     ->line('You are receiving this email because we received a password reset request for your account.')
        //     // ->action('Reset Password', url('user/password/reset', $this->token))
        //     ->markdown('web/layout/email/verification', ['token' => $this->token, 'button_title' => 'Reset Password' ])
        //     ->line('If you did not request a password reset, no further action is required.');
        $get = DB::table('user_password_resets')->where('email', $notifiable->email)->first();

        $user = User::where('email', $get->email)->first();

        if($user)
        {
            return app('App\Http\Controllers\Config\EmailFunction')->forgot_password($this->token, $user);
        }
        else
        {
            $notification = array(
                'message' => 'Email tidak ditemukan',
                'alert-type' => 'error'
            );

            return back()->with($notification);
        }
    }
}
