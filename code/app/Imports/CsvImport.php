<?php

namespace App\Imports;

use App\Voucher;
use Maatwebsite\Excel\Concerns\ToModel;

class CsvImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Voucher([
            'code_voucher'  =>  $row["code_voucher"],
            'email'         =>  $row["email"],
            'quota'         =>  $row["quota"]
        ]);
    }
}
