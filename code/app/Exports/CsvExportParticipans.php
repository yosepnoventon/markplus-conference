<?php

namespace App\Exports;

use App\Login;
use App\Voucher;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class CsvExportParticipans implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Login::select('code_voucher', 'name', 'email', 'phone', 'agency_name')->get();
    }

    public function view(): View
    {
        return view('admin.participants.export', [
            'vouchers' => Login::all()
        ]);
    }
}
