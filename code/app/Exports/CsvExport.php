<?php

namespace App\Exports;

use App\Voucher;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class CsvExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Voucher::select('code_voucher', 'email', 'quota')->get();
    }

    public function view(): View
    {
        return view('admin.voucher.export', [
            'vouchers' => Voucher::all()
        ]);
    }
}
