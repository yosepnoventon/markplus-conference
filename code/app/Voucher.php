<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    const STATUS_CONFIRM_PAYMENT = "CONFIRM PAYMENT";
    const STATUS_PENDING         = "PENDING";
    const STATUS_ACCEPTED        = "ACCEPTED";
    const STATUS_PAID            = "PAID";
    const STATUS_DENIED          = "DENIED";
    const STATUS_EXPIRE          = "EXPIRED";
    const STATUS_CANCEL          = "CANCEL";
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'conf_vouchers';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['code_voucher', 'email', 'quota', 'remaining_quota', 'arrayPayment', 'status_code', 'status'];

    protected $casts = [
        'arrayPayment' => 'array'
    ];
}
