<?php

namespace App\Console;

use Illuminate\Support\Facades\Log;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Course;
use App\User;
use Carbon\Carbon;
use Exception;
use DB;

class Kernel extends ConsoleKernel
{
    const DELIMITER     = "_";
    const PREFIX        = "MARKPLUS" . self::DELIMITER;
    const SUFIX_START   = self::DELIMITER . "START";
    const SUFIX_END     = self::DELIMITER . "END";
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        // Commands\remember_subscribe::class,
        // Commands\remove_students_from_class::class,
        // Commands\start_certificate::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('remember_subscribe:cron')
        //          ->daily('00:01');
        // $schedule->command('remove_students_from_class:cron')
        //          ->daily('00:01');
        // $schedule->command('start_certificate:cron')
        //          ->daily('00:01');

        Log::channel('daily')->info('Scheduler is running');

        //scheduler remember subscribe every day at 00:01
        $schedule->call(function() {
            Log::info(self::PREFIX . 'Remember Subscribe' . self::SUFIX_START);
            $this->rememberSubscribe();
            Log::info(self::PREFIX . 'Remember Subscribe' . self::SUFIX_END);
        })->dailyAt('00:01');

        //scheduler remove student from class every day at 00:01
        $schedule->call(function() {
            Log::info(self::PREFIX . 'Remove Student From Class' . self::SUFIX_START);
            $this->removeStudentFromClass();
            Log::info(self::PREFIX . 'Remove Student From Class' . self::SUFIX_END);
        })->dailyAt('00:01');

        //scheduler start certificate every day at 00:01
        $schedule->call(function() {
            Log::info(self::PREFIX . 'Start Certificate' . self::SUFIX_START);
            $this->startCertificate();
            Log::info(self::PREFIX . 'Start Certificate' . self::SUFIX_END);
        })->dailyAt('00:01');
    }

    public function rememberSubscribe(){
        // order expired h -1 E-Learning
        $orders = DB::table('orders')
                ->select('orders.*', 'users.email')
                ->leftJoin('users', 'users.id', '=', 'orders.user')
                ->where('status_code', '200')
                ->where('expire_date', '>=', Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s'))
                ->get();

        foreach($orders as $order)
        {
            if(round(Carbon::now('Asia/Jakarta')->floatDiffInDays($order->expire_date)) == 1 || round(Carbon::now('Asia/Jakarta')->floatDiffInDays($order->expire_date)) == 0)
            {
                app('App\Http\Controllers\Config\EmailFunction')->remember_subscribe($order);
            }
        }
    }

    public function removeStudentFromClass(){
        $orders = DB::table('orders')
                ->select('orders.*', 'users.email', 'users.user_id_matrix')
                ->leftJoin('users', 'users.id', '=', 'orders.user')
                ->where('status_code', '200')
                ->where('expire_date', '<=', Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s'))
                ->get();
        try {
            foreach($orders as $no => $order){
                $orders_active = DB::table('orders')
                                ->select('orders.*', 'users.email', 'users.user_id_matrix')
                                ->leftJoin('users', 'users.id', '=', 'orders.user')
                                ->where('status_code', '200')
                                ->where('expire_date', '>=', Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s'))
                                ->where('user', $order->user)
                                ->first();

                if(!isset($orders_active)){
                    $courses = DB::table('added_courses_matrix')
                        ->select('added_courses_matrix.*', 'courses.id_matrix')
                        ->leftJoin('courses', 'courses.id', '=', 'added_courses_matrix.course_id')
                        ->where('learn_method_matrix', Course::LEARNING)
                        ->where('user_id', $order->user)
                        ->get();

                    foreach($courses as $course){
                        app('App\Http\Controllers\Matrix\MatrixController')->remove_students_from_class($course->id_matrix, $order->user_id_matrix);
                        DB::table('added_courses_matrix')
                        ->where('user_id', $order->user)
                        ->where('course_id', $course->course_id)
                        ->delete();
                    }

                    DB::table('orders')
                        ->select('orders.*', 'users.email', 'users.user_id_matrix')
                        ->leftJoin('users', 'users.id', '=', 'orders.user')
                        ->where('status_code', '200')
                        ->where('expire_date', '<=', Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s'))
                        ->where('user', $order->user)
                        ->update(['order_status' => 0]);
                }
            }
            
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function startCertificate()
    {
        $orders = DB::table('orders')
        ->select('orders.*', 'users.email', 'users.user_id_matrix')
        ->leftJoin('users', 'users.id', '=', 'orders.user')
        ->where('orders.subscription', null)
        ->where('orders.course','!=', null)
        ->where('orders.detail_course', null)
        ->where('orders.status_code', '200')
        ->where('start_date', '<=', Carbon::now('Asia/Jakarta')->format('Y-m-d'))
        ->get();

        try {
            foreach($orders as $no => $order)
            {
                $course = Course::where('id', $order->course)->first();
                $user = User::where('id', $order->user)->first();
                $added = app('App\Http\Controllers\Matrix\MatrixController')->add_students_to_class($course->id_matrix, $user->user_id_matrix);
                if ($added)
                {
                    //create logs addes course and user to matrix
                    $status_code = 200;
                    $message = "Success Add User and Course";

                    app('App\Http\Controllers\Web\Log\PaymentController')->create_user_course_matrix($course->id, $user->id, $status_code, $message);
                }
            }
            
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
