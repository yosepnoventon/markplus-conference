<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Course;
use App\User;
use Carbon\Carbon;
use DB;
use Exception;

class start_certificate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'start_certificate:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mulai Sertificate';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Order Start Certificate
        $orders = DB::table('orders')
                ->select('orders.*', 'users.email', 'users.user_id_matrix')
                ->leftJoin('users', 'users.id', '=', 'orders.user')
                ->where('orders.subscription', null)
                ->where('orders.course','!=', null)
                ->where('orders.detail_course', null)
                ->where('orders.status_code', '200')
                ->where('start_date', '<=', Carbon::now('Asia/Jakarta')->format('Y-m-d'))
                ->get();
        try {
            foreach($orders as $no => $order)
            {
                $course = Course::where('id', $order->course)->first();
                $user = User::where('id', $order->user)->first();
                $added = app('App\Http\Controllers\Matrix\MatrixController')->add_students_to_class($course->id_matrix, $user->user_id_matrix);
                if ($added)
                {
                    //create logs addes course and user to matrix
                    $status_code = 200;
                    $message = "Success Add User and Course";

                    app('App\Http\Controllers\Web\Log\PaymentController')->create_user_course_matrix($course->id, $user->id, $status_code, $message);
                }
            }
            $this->info('start_certificate:Cron Command Run successfully!');
        } catch (Exception $e) {
            $this->info('start_certificate:Cron Command Run failed!');
        }
    }
}
