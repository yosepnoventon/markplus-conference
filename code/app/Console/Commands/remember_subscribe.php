<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use DB;

class remember_subscribe extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remember_subscribe:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'success remove students to class';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // order expired h -1 E-Learning
        $orders = DB::table('orders')
                ->select('orders.*', 'users.email')
                ->leftJoin('users', 'users.id', '=', 'orders.user')
                ->where('status_code', '200')
                ->where('expire_date', '>=', Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s'))
                ->get();

        foreach($orders as $order)
        {
            if(round(Carbon::now('Asia/Jakarta')->floatDiffInDays($order->expire_date)) == 1 || round(Carbon::now('Asia/Jakarta')->floatDiffInDays($order->expire_date)) == 0)
            {
                app('App\Http\Controllers\Config\EmailFunction')->remember_subscribe($order);
            }
        }

        $this->info('remember_subscribe:Cron Command Run successfully!');
    }
}
