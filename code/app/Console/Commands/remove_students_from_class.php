<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Course;
use App\User;
use Carbon\Carbon;
use DB;
use Exception;

class remove_students_from_class extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remove_students_from_class:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'success remove students to class';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // order expired E-Learning
        $orders = DB::table('orders')
                ->select('orders.*', 'users.email', 'users.user_id_matrix')
                ->leftJoin('users', 'users.id', '=', 'orders.user')
                ->where('status_code', '200')
                ->where('expire_date', '<=', Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s'))
                ->get();
        try {
            foreach($orders as $no => $order)
            {
                $orders_active = DB::table('orders')
                                ->select('orders.*', 'users.email', 'users.user_id_matrix')
                                ->leftJoin('users', 'users.id', '=', 'orders.user')
                                ->where('status_code', '200')
                                ->where('expire_date', '>=', Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s'))
                                ->where('user', $order->user)
                                ->first();
                if(!isset($orders_active))
                {
                    $courses = DB::table('added_courses_matrix')
                        ->select('added_courses_matrix.*', 'courses.id_matrix')
                        ->leftJoin('courses', 'courses.id', '=', 'added_courses_matrix.course_id')
                        ->where('learn_method_matrix', Course::LEARNING)
                        ->where('user_id', $order->user)
                        ->get();
                    foreach($courses as $course)
                    {
                        app('App\Http\Controllers\Matrix\MatrixController')->remove_students_from_class($course->id_matrix, $order->user_id_matrix);
                        DB::table('added_courses_matrix')
                        ->where('user_id', $order->user)
                        ->where('course_id', $course->course_id)
                        ->delete();
                    }
                    DB::table('orders')
                        ->select('orders.*', 'users.email', 'users.user_id_matrix')
                        ->leftJoin('users', 'users.id', '=', 'orders.user')
                        ->where('status_code', '200')
                        ->where('expire_date', '<=', Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s'))
                        ->where('user', $order->user)
                        ->update(['order_status' => 0]);
                }
            }
            $this->info('remove_students_from_class:Cron Command Run successfully!');
        } catch (Exception $e) {
            $this->info('remove_students_from_class:Cron Command Run failed!');
        }
    }
}
