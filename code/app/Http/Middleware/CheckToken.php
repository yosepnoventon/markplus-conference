<?php

namespace App\Http\Middleware;

use Closure;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->header('api-key') != config('services.api_key')) {
			return response()->json([
				'status' => 400,
				'message' => 'token not Valid.'
			], 400);
		}
		
        return $next($request);
    }
}
