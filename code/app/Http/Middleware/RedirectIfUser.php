<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Session;
use Illuminate\Support\Facades\Redirect;

class RedirectIfUser
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  string|null  $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = 'user')
	{	
        if (Auth::guard($guard)->check())
        {
			if(isset($_COOKIE['sso_user']))
			{
				$cookie = app(globalFunction::class)->encrypt_decrypt('decrypt', $_COOKIE['sso_user']);
				if($cookie)
				{
					return Redirect::to(Session::get('url.intended'));
				}
				else
				{
					Auth::guard('user')->logout();
					return redirect('/user/login');
				}
			}
			else
			{
				Auth::guard('user')->logout();
				return redirect('/user/login');
			}
	    }

	    return $next($request);
	}
}
