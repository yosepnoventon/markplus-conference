<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Voucher;
use App\Orders;
use App\Login;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use DB;
use Yajra\Datatables\Datatables;

use Session;

class WebController extends Controller
{
    public function __construct()
    {
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
    }

    public function index()
    {
        return view('web.introduction');
    }

    public function virtualReality()
    {
        return view('web.virtual-reality');
    }

    public function chat()
    {
        return view('web.chat');
    }

    public function login()
    {
        return view('web.homepage');
    }

    public function submit_login(Request $request, Login $logins)
    {
        $email          = $request->email;
        $phone          = $request->phone;
        $code_voucher   = $request->code_voucher;
        
        // cek voucher
        $voucher = Voucher::where('code_voucher', $code_voucher)->first();
        if(is_null($voucher))
        {
            // $response = array();
            // $response['message'] = "Voucher Not Found";
            // $response['type'] = "error";

            // return response()->json($response, 200);
            $notification = array(
                'message' => 'Voucher Not Found',
                'alert-type' => 'error'
            );

            return back()->with($notification)->withInput($request->only('name', 'email', 'phone', 'code_voucher', 'agency_name'));
        }

        $login = Login::where('email', $email)->where('phone', $phone)->where('code_voucher', $code_voucher)->first();
        // if user not found and create user
        if(is_null($login))
        {
            // $response = array();
            // $response['message'] = "user does't exist";
            // $response['type'] = "error";

            // return response()->json($response, 200);
            $notification = array(
                'message' => 'Unregistered user or email not registered on the voucher',
                'alert-type' => 'error'
            );

            return back()->with($notification)->withInput($request->only('name', 'email', 'phone', 'code_voucher', 'agency_name'));
        }
        // login create session
        else if(Auth::guard('login')->loginUsingId($login->id))
        {
            $logged = Auth::guard('login')->user();
            
            $updated = Login::findOrFail($logged->id);
            $updated->update(['updated_at' => Carbon::now()->format('Y-m-d H:i:s')]);
            

            $notification = array(
                'message' => 'Login Success',
                'alert-type' => 'success'
            );
            return redirect('/introduction')->with($notification);
        }
        else
        {
            $notification = array(
                'message' => 'Error some parameter',
                'alert-type' => 'error'
            );
            return back()->with($notification)->withInput($request->only('email', 'remember'));
        }
    }

    public function register(Request $request, Login $logins)
    {
        return view('web.register');
    }

    public function submit_register(Request $request, Login $login)
    {
        $name           = $request->name;
        $email          = $request->email;
        $phone          = $request->phone;
        $code_voucher   = $request->code_voucher;
        $agency_name    = $request->agency_name;
        
        // cek voucher
        $vouchers = Voucher::where('code_voucher', $code_voucher)->first();
        if(is_null($vouchers))
        {
            $response = array();
            $response['message'] = "Voucher Not Found";
            $response['type'] = "error";

            return response()->json($response, 200);
        }

        $logins = Login::where('email', $email)->where('phone', $phone)->where('code_voucher', $code_voucher)->first();
        // if user not found and create user
        if(is_null($logins))
        {
            $emails = Login::where('email', $email)->first();
            // if email found
            if(!is_null($emails))
            {
                $response = array();
                $response['message'] = "Email already exists";
                $response['type'] = "error";
    
                return response()->json($response, 200);
            }

            $phones = Login::where('phone', $phone)->first();
            // if phone found
            if(!is_null($phones))
            {
                $response = array();
                $response['message'] = "Phone Number already exists";
                $response['type'] = "error";
    
                return response()->json($response, 200);
            }

            // if quota maximum
            if($vouchers->remaining_quota <= 0)
            {
                $response = array();
                $response['message'] = "Quota has been reached";
                $response['type'] = "error";
    
                return response()->json($response, 200);
            }
            $data = array();
            $data['name']           = $name;
            $data['email']          = $email;
            $data['phone']          = $phone;
            $data['code_voucher']   = $code_voucher;
            $data['agency_name']    = $agency_name;
            $create = $login->create($data);

            // update voucher remining quota
            $count_voucher = Login::where('code_voucher', $code_voucher)->count();
            $vouchers->remaining_quota = $vouchers->quota - $count_voucher;
            $vouchers->save();

            // login create session
            $response = array();
            $response['message'] = "Login Success";
            $response['type'] = "success";
            $response['data'] = $data;

             // login create session
            Auth::guard('login')->loginUsingId($create->id);

            return response()->json($response, 200);
        }
        else
        {
            $response = array();
            $response['message'] = "You are already registered";
            $response['type'] = "error";

            return response()->json($response, 200);
        }
    }

    public function logout(Request $request)
    {
        $this->guard('login')->logout();

        return redirect('/');
    }

    protected function guard()
    {
        return Auth::guard('login');
    }
}
