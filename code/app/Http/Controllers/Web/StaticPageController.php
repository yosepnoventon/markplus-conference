<?php

namespace App\Http\Controllers\Web;

use App\AboutUs;
use App\ContactUs;
use App\Faq;
use App\Http\Controllers\Controller;
use App\TermAndCondition;
use Illuminate\Http\Request;

class StaticPageController extends Controller
{
    public function __construct()
    {
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
    }

    public function about_us()
    {
        $aboutus = AboutUs::first();

        return view('web.static-page.about-us', compact('aboutus'));
    }

    public function contact_us()
    {
        $contactus = ContactUs::first();

        return view('web.static-page.contact-us', compact('contactus'));
    }

    public function faq()
    {
        $faqs = Faq::get();

        return view('web.static-page.faq', compact('faqs'));
    }

    public function term_and_condition()
    {
        $tnc = TermAndCondition::first();

        return view('web.static-page.term-and-condition', compact('tnc'));
    }
}
