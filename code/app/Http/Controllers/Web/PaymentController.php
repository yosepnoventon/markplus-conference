<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Config\EmailFunction;
use Carbon\Carbon;
use Session;
use App\Orders;
use App\Voucher;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class PaymentController extends Controller
{
    public function __construct()
    {
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        // $this->middleware('user');

    }

    public function payment2()
    {
        $param = $_GET['param'];

        $url = $this->dokuURL;

        $decrypted = app('App\Http\Controllers\Config\globalFunction')->encrypt_decrypt('decrypt', $param);

        Session::put('param', $param);

        $old_param = Session::get('p');

        if ($param != $old_param)
        {
            return redirect('/');
        }

        if(is_object(json_decode($decrypted)))
        {
            $decrypt = json_decode($decrypted)->subscription_id;

            $payment = Orders::where('subscription', $decrypt)->where('user', Auth::guard('user')->user()->id)->where(function ($query)
            {
                $query->Where('status_code', null)
                    ->orWhere('status_code', 400);
            })->first();

            // if payment 0 or diskon 100% automatic payment complete
            if($payment->price <= 0)
            {
                $callback = $this->paymentAutoComplete($payment);
                return redirect($callback['redirect'])->with($callback['status']);
            }
            $arrayPayment = json_decode($payment->arrayPayment);

            return view('web.payment.payment', compact('arrayPayment', 'url'));
        }
        else
        {
            $decrypt = $decrypted;

            $course = Course::where('id_matrix', $decrypt)->first();

            $learnmethod = LearnMethod::where('id', $course->learn_method)->first();

            if ($course->is_detail == 1 && $learnmethod->id == 2)
            {
                $payment = Orders::select(['orders.*'])
                    ->leftJoin('detail_courses', 'orders.detail_course', '=', 'detail_courses.id')
                    ->leftJoin('courses', 'orders.course', '=', 'courses.id')
                    ->where('orders.user', Auth::guard('user')->user()->id)
                    ->where('orders.course', $course->id)
                    ->where('orders.detail_course', Session::get('detail_id'))
                    ->where(function ($query)
                    {
                        $query->Where('orders.status_code', null)
                            ->orWhere('orders.status_code', 400);
                    })
                    ->first();
                // if payment 0 or diskon 100% automatic payment complete
                if($payment->price <= 0)
                {
                    $callback = $this->paymentAutoComplete($payment);
                    return redirect($callback['redirect'])->with($callback['status']);
                }
            }else if ($learnmethod->id == 3)
            {
                $payment = Orders::select(['orders.*'])
                    ->leftJoin('courses', 'orders.course', '=', 'courses.id')
                    ->where('orders.user', Auth::guard('user')->user()->id)
                    ->where('orders.course', $course->id)
                    ->where(function ($query)
                    {
                        $query->Where('orders.status_code', null)
                            ->orWhere('orders.status_code', 400);
                    })
                    ->first();
                // if payment 0 or diskon 100% automatic payment complete
                if($payment->price <= 0)
                {
                    $callback = $this->paymentAutoComplete($payment);
                    return redirect($callback['redirect'])->with($callback['status']);
                }
            }
            else
            {
                $payment = Orders::where('course', $course->id)->first();

                // if payment 0 or diskon 100% automatic payment complete
                if($payment->price <= 0)
                {
                    $callback = $this->paymentAutoComplete($payment);
                    return redirect($callback['redirect'])->with($callback['status']);
                }
            }

            $arrayPayment = json_decode($payment->arrayPayment);

            return view('web.payment.payment', compact('course', 'arrayPayment', 'url'));
        }
    }

    public function do_payment(Request $request, Voucher $voucher)
    {
        $status_payment = $request->status_payment;
        $person = json_decode($request->person, true);
        $data = json_decode($request->data, true);
        // dd($status_payment, $person, $data);
        // if status not paid
        $voucher->code_voucher      = $data['order_id'];
        $voucher->name              = $person['name'];
        $voucher->email             = $person['email'];
        $voucher->quota             = $person['quota'];
        $voucher->remaining_quota   = $person['quota'];
        if($voucher->status != Voucher::STATUS_PAID)
        {
            if($status_payment == "success")
            {
                $voucher->status_code = $data['status_code'];
                $voucher->status = Voucher::STATUS_PAID;
                $voucher->arrayPayment = $data;
            }
            else if($status_payment == "pending")
            {
                $voucher->status_code = $data['status_code'];
                $voucher->status = Voucher::STATUS_PENDING;
                $voucher->arrayPayment = $data;
            }else
            {
                $voucher->status_code = $data['status_code'];
                $voucher->status = $data['status_message'];
                $voucher->arrayPayment = $data;
            }
            $voucher->save();

            // send voucher
            app(EmailFunction::class)->send_vouchers($voucher);
        }
        
        $notification = array(
            'message' => 'Success Registration, Please cek your Email!',
            'alert-type' => 'success'
        );

        return redirect('register')->with($notification);
    }

    function randomString($length)
    {
        $str = "";
        $characters = array_merge(range('A', 'Z'), range('0', '9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }
}
