<?php

namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;
use App\InterestPromo;
use Illuminate\Http\Request;

class InterestPromoController extends Controller
{
    public function __construct()
    {
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
    }

    public function add(Request $request)
    {
        $interest = InterestPromo::where('interest',$request->interest)->whereEmail($request->email)->first();
        if(is_null($interest))
        {
            $this->validate($request, [
                'interest' => 'required',
                'email' => 'required'
            ]);
    
            $input = $request->all();
    
            $interest = InterestPromo::create($input);
        }
        
		return response(['data' => $interest], 200);
    }
}
