<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use App\Orders;
use App\Voucher;
use App\User;
use Mail;
use Session;
use View;

use App\Api\MidtransPaymentApi;


use Midtrans\Config as MidtransConfig;
use Midtrans\Snap as MidtransSnap;
use Midtrans\Notification as MidtransNotif;
use Midtrans\CoreApi as MidtransCoreApi;

class MidtransController extends Controller
{

    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');

        // Set your Merchant Server Key
        MidtransConfig::$serverKey = config('services.midtrans.serverKey');
        // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
        MidtransConfig::$isProduction = config('services.midtrans.isProduction');
        // Set sanitization on (default)
        MidtransConfig::$isSanitized = config('services.midtrans.isSanitized');
        // Set 3DS transaction for credit card to true
        MidtransConfig::$is3ds = config('services.midtrans.is3ds');
    }
    
    public function vtweb(Request $request)
    {
        $name = $request->name;
        $email = $request->email;
        $phone = $request->phone;
        $quota = $request->quota;

        // idr Rp.500.000
        if($quota <= 4)
        {
            $per_price = 500000;
            $price = $per_price * $quota;
        }
        // idr Rp.400.000
        else if($quota <= 9)
        {
            $per_price = 400000;
            $price = $per_price * $quota;
        }
        // idr Rp.250.000
        else
        {
            $per_price = 250000;
            $price = $per_price * $quota;

        }
        //check booking code exsist
        do {
            $voucher = $this->randomString(5);
            $result = Voucher::where('code_voucher', $voucher)->count();
        } while ($result > 0);
        if(!is_null($quota))
        {
            $item_details[0]['id']         = 1;
            $item_details[0]['price']      = $per_price;
            $item_details[0]['quantity']   = $quota;
            $item_details[0]['name']       = "Voucher Conference";

            $transaction_details = array(
                'order_id' => $voucher,
                'gross_amount' => $price, // no decimal allowed for creditcard
            );

            $billing_address = array(
                'first_name'    => "Markplus",
                'last_name'     => "Conference",
                'phone'         => "0987654321",
                'country_code'  => 'IDN'
            );


            // // Optional
            $shipping_address = array(
                'first_name'    => "Markplus",
                'last_name'     => "Conference",
                'phone'         => "0987654321",
                'country_code'  => 'IDN'
            );

            // Optional
            $customer_details = array(
                'first_name'        => $name,
                'last_name'         => "",
                'email'             => $email,
                'phone'             => $phone,
                'billing_address'   => $billing_address,
                'shipping_address'  => $shipping_address
            );

            $transaction = array(
                'transaction_details' => $transaction_details,
                'customer_details' => $customer_details,
                'item_details' => $item_details,
            );
            
            // Log::info('transaction: '. print_r($transaction, true));
            $snapToken = MidtransSnap::getSnapToken($transaction);
            $data_token['snap_token'] = $snapToken;

            // Beri response snap token
            $this->response['snap_token'] = $snapToken;

            return response()->json($this->response);

        } else {
            echo "The order has removed, please";
            die();
        }
    }

    public function notification()
    {
        $request = new MidtransNotif();
        // Get invoice order and check for validation
        $order = Orders::where('invoice', $request->order_id)->first();
        if (is_null($order))
        {
            return response()->json([
                'status' => 404,
                'message' => 'order is not found.'
            ], 404);
        }

        $user = User::where('id', $order->user)->first();

        // Update midtrans response to its order
        $order->arrayPayment = json_encode($request);

        $transaction = $request->transaction_status;
        $type        = $request->payment_type;
        $order_id    = $request->order_id;
        $fraud       = $request->fraud_status;
        $status      = $request->status_code;

        if($transaction == 'capture')
        {
            // For credit card transaction, we need to check whether transaction is challenge by FDS or not
            if ($type == 'credit_card')
            {
                if($fraud == 'challenge')
                {
                    // TODO set payment status in merchant's database to 'Challenge by FDS'
                    // TODO merchant should decide whether this transaction is authorized or not in MAP
                }
                else if($fraud == 'deny')
                {
                    // TODO set payment status in merchant's database to 'Challenge by FDS'
                    // TODO merchant should decide whether this transaction is authorized or not in MAP
                }
                else if($fraud == 'accept')
                {
                    // TODO set payment status in merchant's database to 'Success'


                    // Payment paid! open the assign product
                    $order->status_code = $status;
                    $order->status = Orders::STATUS_PAID;
                }
            }
        }
        else if ($transaction == 'settlement')
        {
            // TODO set payment status in merchant's database to 'Settlement'
            if ($type == 'credit_card')
            {
                // Payment paid! open the assign product

                if($fraud == 'challenge')
                {
                    // TODO set payment status in merchant's database to 'Challenge by FDS'
                    // TODO merchant should decide whether this transaction is authorized or not in MAP
                }
                else if($fraud == 'deny')
                {
                    // TODO set payment status in merchant's database to 'Challenge by FDS'
                    // TODO merchant should decide whether this transaction is authorized or not in MAP
                }
                else if($fraud == 'accept')
                {

                    // Payment paid! open the assign product
                    $order->status_code = $status;
                    $order->status = Orders::STATUS_PAID;
                }
            }
        }
        else if($transaction == 'pending')
        {
            // TODO set payment status in merchant's database to 'Pending'
            $order->status_code = $status;
            $order->status = Orders::STATUS_PENDING;
        }
        else if ($transaction == 'deny')
        {
            // TODO set payment status in merchant's database to 'Denied'

            // Deny payment
            $order->status_code = $status;
            $order->status = Orders::STATUS_DENIED;
        }
        else if ($transaction == 'expire')
        {
            // TODO set payment status in merchant's database to 'expire'

            // Expire payment
            $order->status_code = $status;
            $order->status = Orders::STATUS_EXPIRE;

        }
        else if ($transaction == 'cancel')
        {
            // TODO set payment status in merchant's database to 'Denied'

            // Cancel payment
            $order->status_code = $status;
            $order->status = Orders::STATUS_CANCEL;
        }

        // Save order
        $order->save();

        exit;

        return response()->json([
            'status' => 200,
            'message' => 'Ok'
        ], 200);
    }

    public function paymentfinish(Request $request, Orders $order) {
        $order_id = $request->order_id;
        Log::info('paymentfinish: '. $order_id);

        $status_code = $request->status_code;
        $transaction_status = $request->transaction_status;

        if( $status_code == 200) {
            $order = $order->where('invoice', $order_id)->firstOrFail();
            
            $order->status_code = $status_code;
            $order->status = Orders::STATUS_ACCEPTED;

            //sent email to User
            app('App\Http\Controllers\Config\EmailFunction')->user_confirm_payment($order->users, $order);

            //sent email to Admin
            app('App\Http\Controllers\Config\EmailFunction')->admin_confirm_payment($order->users, $order);

            $notification = array(
                'message' => 'Checkout and Payment was successful!',
                'alert-type' => 'success',
            );

            return redirect('/order-history/'.$order->invoice)->with($notification);
            $notification = array(
                'message' => 'Checkout and Payment was unsuccessful!',
                'alert-type' => 'error',
            );
            return redirect('/checkoutsuccess_veritrans');
        }
    }

    public function paymentunfinish(Request $request, Orders $order) {
        $order_id = $request->order_id;
        Log::info('paymentunfinish: '. $order_id);

        $notification = array(
            'message' => 'Unfinish!',
            'alert-type' => 'warning',
        );
        return redirect('/order-history')->with($notification);
    }

    public function errorhandle(Request $request, Orders $order) {
        $order_id = $request->order_id;
        Log::info('errorhandled: '. $order_id);

        $notification = array(
            'message' => 'error handled!',
            'alert-type' => 'warning',
        );
        return redirect('/order-history')->with($notification);
    }

    function randomString($length)
    {
        $str = "";
        $characters = range('0', '9');
        $max = count($characters) - 1;
        $str .= 'E';
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }
}
