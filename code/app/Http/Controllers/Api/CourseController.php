<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Libraries\FormatConverter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Course;
use App\Category;
use App\LearnMethod;

class CourseController extends Controller
{
    public function save(Request $request)
    {
        // get Learn Method by learn method matrix id
        $learnMethod = LearnMethod::where('id_matrix', $request->learn_method_matrix_id)->first();
        if(is_null($learnMethod))
        {
            return response()->json([
                'status' => 404,
                'message' => 'The learn method matrix id Not Found',
            ], 404);
        }
        else
        {
            // validation for learning method E-Learning & Certification
            if($learnMethod->id == LearnMethod::ELEARNING || $learnMethod->id == LearnMethod::CERTIFICATION)
            {
                $validator = Validator::make($request->all(), [
                    'id_matrix'              => 'required|unique:courses,id_matrix',
                    'category_matrix_id'     => 'required',
                    'learn_method_matrix_id' => 'required',
                    'image'                  => 'required',
                    'title'                  => 'required',
                ]);
            }
            // if learning method not E-Learning & Certification
            else
            {
                return response()->json([
                    'status' => 400,
                    'message' => 'Learn Method Must be E-Learning & Certification',
                ], 400);
            }

            $request['learn_method']        = $learnMethod->id;
            $request['learn_method_matrix'] = $learnMethod->title;
        }

        if ($validator->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some Parameters is required',
				'validators' => FormatConverter::parseValidatorErrors($validator),
			], 400);
        }
        // get category by category matrix id
        $category = Category::where('category_matrix_id', $request->category_matrix_id)->first();
        if(is_null($category))
        {
            return response()->json([
                'status' => 404,
                'message' => 'The category matrix id Not Found',
            ], 404);
        }
        else
        {
            $request['category']        = $category->id;
            $request['category_matrix'] = $category->name;
        }
        $request['finish_date'] = $request->expired_date;
        $request['is_detail'] = Course::NOTDETAIL;
        $request['slug'] = str_slug($request->title, '-');
        
        if(is_null($request->is_active))
        {
            $request['is_active'] = Course::ACTIVE;
        }
        else
        {
            $request['is_active'] = ($request->is_active == true) ? Course::ACTIVE : Course::INACTIVE;
        }

        $course = new Course();
        $course->fill($request->only([
            'id_matrix',
            'category',
            'category_matrix',
            'learn_method',
            'learn_method_matrix',
            'image',
            'banner',
            'preview_text',
            'title',
            'description',
            'price',
            'thumbnail_video',
            'source_video',
            'description_video',
            'start_date',
            'finish_date',
            'description_image',
            'is_detail',
            'slug',
            'is_active',
        ]));

        $course->save();

        return response()->json([
            'status' => 200,
            'message' => 'Success Created',
        ], 200);
    }

    public function update(Request $request, $id_matrix)
    {
        $course = Course::where('id_matrix', $id_matrix)->first();

        if(is_null($course))
        {
            return response()->json([
				'status'  => 404,
				'message' => 'The matrix id Not Found',
			], 404);
        }
        
        // get Learn Method by learn method matrix id
        $learnMethod = LearnMethod::where('id_matrix', $request->learn_method_matrix_id)->first();
        if(is_null($learnMethod))
        {
            return response()->json([
                'status' => 404,
                'message' => 'The learn method matrix id Not Found',
            ], 404);
        }
        else
        {
            // validation for learning method E-Learning & Certification
            if($learnMethod->id == LearnMethod::ELEARNING || $learnMethod->id == LearnMethod::CERTIFICATION)
            {
                $validator = Validator::make($request->all(), [
                    'category_matrix_id'     => 'required',
                    'learn_method_matrix_id' => 'required',
                    'image'                  => 'required',
                    'title'                  => 'required',
                ]);
            }
            // if learning method not E-Learning & Certification
            else
            {
                return response()->json([
                    'status' => 400,
                    'message' => 'Learn Method Must be E-Learning & Certification',
                ], 400);
            }

            $course['learn_method']        = $learnMethod->id;
            $course['learn_method_matrix'] = $learnMethod->title;
        }

        if ($validator->fails()) {
			return response()->json([
				'status'     => 400,
				'message'    => 'Some Parameters is required',
				'validators' => FormatConverter::parseValidatorErrors($validator),
			], 400);
        }

        // get category by category matrix id
        $category = Category::where('category_matrix_id', $request->category_matrix_id)->first();
        if(is_null($category))
        {
            return response()->json([
                'status' => 404,
                'message' => 'The category matrix id Not Found',
            ], 404);
        }
        else
        {
            $course['category']        = $category->id;
            $course['category_matrix'] = $category->name;
        }

        $course['image']            = $request->image;
        $course['banner']           = $request->banner;
        $course['preview_text']     = $request->preview_text;
        $course['title']            = $request->title;
        $course['description']      = $request->description;
        $course['price']            = $request->price;
        $course['thumbnail_video']  = $request->thumbnail_video;
        $course['source_video']     = $request->source_video;
        $course['description_video']= $request->description_video;
        $course['start_date']       = $request->start_date;
        $course['finish_date']      = $request->expired_date;
        $course['description_image']= $request->description_image;
        $course['is_detail']        = Course::NOTDETAIL;
        $course['slug']             = str_slug($request->title, '-');

        if(is_null($request->is_active))
        {
            $course['is_active'] = Course::ACTIVE;
        }
        else
        {
            $course['is_active'] = ($request->is_active == true) ? Course::ACTIVE : Course::INACTIVE;
        }
        $course->save();

        return response()->json([
            'status'  => 200,
            'message' => 'Success Updated',
        ]);
    }
}
