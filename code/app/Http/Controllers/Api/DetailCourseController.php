<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Libraries\FormatConverter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\DetailCourse;
use App\Course;
use App\Instructor;

class DetailCourseController extends Controller
{
    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_matrix'              => 'required|unique:detail_courses,id_matrix',
            'course_matrix_id'       => 'required',
            'title'                  => 'required',
            'start_date'             => 'required',
            'expired_date'           => 'required',
            'duration'               => 'required',
        ]);

        if ($validator->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some Parameters is required',
				'validators' => FormatConverter::parseValidatorErrors($validator),
			], 400);
        }

        // get course
        $course = Course::where('id_matrix', $request->course_matrix_id)->first();
        if(is_null($course))
        {
            return response()->json([
                'status' => 404,
                'message' => 'The course matrix id Not Found',
            ], 404);
        }
        else
        {
            $request['parent_id_matrix'] = $course->id_matrix;
            $request['course'] = $course->id;
            $request['city'] = $course->learnMethod->slug;
        }

        // get instructors
        if(!is_null($request->instructor_matrix_id))
        {
            $instructor = Instructor::where('id_matrix', $request->instructor_matrix_id)->first();
            if(is_null($instructor))
            {
                return response()->json([
                    'status' => 404,
                    'message' => 'The instructor matrix id Not Found',
                ], 404);
            }
            else
            {
                $request['instructor'] = $instructor->id;
            }
        }

        $request['area'] = $request->title;
        $request['finish_date'] = $request->expired_date;
        $request['slug'] = str_slug($request->title, '-');
        $request['is_approve'] = DetailCourse::APPROVE;
        $request['link'] = $request->zoom_link;
        
        if(is_null($request->is_active))
        {
            $request['is_active'] = DetailCourse::ACTIVE;
        }
        else
        {
            $request['is_active'] = ($request->is_active == true) ? DetailCourse::ACTIVE : DetailCourse::INACTIVE;
        }

        $detailCourse = new DetailCourse();
        $detailCourse->fill($request->only([
            'id_matrix',
            'parent_id_matrix',
            'course',
            'city',
            'instructor',
            'area',
            'date',
            'time',
            'start_date',
            'finish_date',
            'duration',
            'slug',
            'banner',
            'thumbnail_video',
            'source_video',
            'description_video',
            'is_approve',
            'link',
            'description_image',
            'is_active',
        ]));

        $detailCourse->save();

        return response()->json([
            'status' => 200,
            'message' => 'Success Created',
        ], 200);
    }

    public function update(Request $request, $id_matrix)
    {
        $detailCourse = DetailCourse::where('id_matrix', $id_matrix)->first();

        if(is_null($detailCourse))
        {
            return response()->json([
				'status'  => 404,
				'message' => 'The matrix id Not Found',
			], 404);
        }
        
        $validator = Validator::make($request->all(), [
            'course_matrix_id'       => 'required',
            'title'                  => 'required',
            'start_date'             => 'required',
            'expired_date'           => 'required',
            'duration'               => 'required',
        ]);

        if ($validator->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some Parameters is required',
				'validators' => FormatConverter::parseValidatorErrors($validator),
			], 400);
        }

        // get course
        $course = Course::where('id_matrix', $request->course_matrix_id)->first();
        if(is_null($course))
        {
            return response()->json([
                'status' => 404,
                'message' => 'The course matrix id Not Found',
            ], 404);
        }
        else
        {
            $detailCourse['parent_id_matrix'] = $course->id_matrix;
            $detailCourse['course'] = $course->id;
            $detailCourse['city'] = $course->learnMethod->slug;
        }

        // get instructors
        if(!is_null($request->instructor_matrix_id))
        {
            $instructor = Instructor::where('id_matrix', $request->instructor_matrix_id)->first();
            if(is_null($instructor))
            {
                return response()->json([
                    'status' => 404,
                    'message' => 'The instructor matrix id Not Found',
                ], 404);
            }
            else
            {
                $detailCourse['instructor'] = $instructor->id;
            }
        }

        $detailCourse['area']             = $request->title;
        $detailCourse['date']             = $request->date;
        $detailCourse['time']             = $request->time;
        $detailCourse['start_date']       = $request->start_date;
        $detailCourse['finish_date']      = $request->expired_date;
        $detailCourse['duration']         = $request->duration;
        $detailCourse['slug']             = str_slug($request->title, '-');
        $detailCourse['banner']           = $request->banner;
        $detailCourse['thumbnail_video']  = $request->thumbnail_video;
        $detailCourse['source_video']     = $request->source_video;
        $detailCourse['description_video']= $request->description_video;
        $detailCourse['link']             = $request->zoom_link;
        $detailCourse['description_image']= $request->description_image;
        if(is_null($request->is_active))
        {
            $detailCourse['is_active'] = Course::ACTIVE;
        }
        else
        {
            $detailCourse['is_active'] = ($request->is_active == true) ? detailCourse::ACTIVE : detailCourse::INACTIVE;
        }
        $detailCourse->save();

        return response()->json([
            'status'  => 200,
            'message' => 'Success Updated',
        ]);
    }
}
