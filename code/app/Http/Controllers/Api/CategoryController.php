<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Libraries\FormatConverter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Category;

class CategoryController extends Controller
{
    /**
	 * @param $from_id
     * @param $to_id
	 * @return type
	 */
    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_matrix_id' => 'required|unique:categories,category_matrix_id',
            'name'               => 'required',
        ]);

        if ($validator->fails()) {
			return response()->json([
				'status'     => 400,
				'message'    => 'Some Parameters is required',
				'validators' => FormatConverter::parseValidatorErrors($validator),
			], 400);
        }
        $request['slug'] = str_slug($request->name, '-');
        $categorys = new Category();
        $categorys->fill($request->only([
            'category_matrix_id',
            'name',
            'slug'
        ]));

        $categorys->save();
        
        return response()->json([
            'status'  => 200,
            'message' => 'Success Created',
        ], 200);
    }

    public function update(Request $request, $category_matrix_id)
    {
        $category = Category::where('category_matrix_id', $category_matrix_id)->first();

        if(is_null($category))
        {
            return response()->json([
				'status'  => 404,
				'message' => 'The category matrix id Not Found',
			], 404);
        }
        
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
			return response()->json([
				'status'     => 400,
				'message'    => 'Some Parameters is required',
				'validators' => FormatConverter::parseValidatorErrors($validator),
			], 400);
        }
        $category['name'] = $request->name;
        $category['slug'] = str_slug($request->name, '-');
        $category->save();

        return response()->json([
            'status'  => 200,
            'message' => 'Success Updated',
        ]);
    }
}
