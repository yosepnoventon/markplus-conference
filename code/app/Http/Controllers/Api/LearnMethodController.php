<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Libraries\FormatConverter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\LearnMethod;

class LearnMethodController extends Controller
{
    /**
	 * @param $from_id
     * @param $to_id
	 * @return type
	 */
    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_matrix' => 'required|unique:learning_methods,id_matrix',
            'image'     => 'required',
            'title'     => 'required',
        ]);

        if ($validator->fails()) {
			return response()->json([
				'status'     => 400,
				'message'    => 'Some Parameters is required',
				'validators' => FormatConverter::parseValidatorErrors($validator),
			], 400);
        }

        $request['slug'] = str_slug($request->title, '-');
        if(is_null($request->is_active))
        {
            $request['is_active'] = LearnMethod::ACTIVE;
        }
        else
        {
            $request['is_active'] = ($request->is_active == true) ? LearnMethod::ACTIVE : LearnMethod::INACTIVE;
        }
        $learnMethods = new LearnMethod();
        $learnMethods->fill($request->all());

        $learnMethods->save();
        
        return response()->json([
            'status'  => 200,
            'message' => 'Success Created',
        ], 200);
    }

    public function update(Request $request, $id_matrix)
    {
        $learnMethod = LearnMethod::where('id_matrix', $id_matrix)->first();

        if(is_null($learnMethod))
        {
            return response()->json([
				'status'  => 404,
				'message' => 'The matrix id Not Found',
			], 404);
        }
        
        $validator = Validator::make($request->all(), [
            'title' => 'required',
        ]);

        if ($validator->fails()) {
			return response()->json([
				'status'     => 400,
				'message'    => 'Some Parameters is required',
				'validators' => FormatConverter::parseValidatorErrors($validator),
			], 400);
        }
        $learnMethod['image']          = $request->image;
        $learnMethod['banner']         = $request->banner;
        $learnMethod['icon']           = $request->icon;
        $learnMethod['title']          = $request->title;
        $learnMethod['slug']           = str_slug($request->title, '-');
        $learnMethod['preview_text']   = $request->preview_text;
        $learnMethod['description']    = $request->description;
        if(is_null($request->is_active))
        {
            $learnMethod['is_active'] = LearnMethod::ACTIVE;
        }
        else
        {
            $learnMethod['is_active'] = ($request->is_active == true) ? LearnMethod::ACTIVE : LearnMethod::INACTIVE;
        }
        $learnMethod->save();

        return response()->json([
            'status'  => 200,
            'message' => 'Success Updated',
        ]);
    }
}
