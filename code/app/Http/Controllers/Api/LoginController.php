<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Config\globalFunction;
use App\Http\Controllers\Controller;
use App\Libraries\FormatConverter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;
use App\User;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $data = [];
        if(isset($_COOKIE['sso_user']))
        {
            $cookie = app(globalFunction::class)->encrypt_decrypt('decrypt', $_COOKIE['sso_user']);
            if($cookie)
            {
                if($user = User::where('email', $cookie)->first())
                {
                    $data = [
                        'id' => $user->user_id_matrix,
                        'userid' => $user->userid
                    ];
                    return response()->json([
                        'data'      => $data,
                        'status'    => 200,
                        'message'   => 'success',
                    ], 200);
                }
            }
            else
            {
                return response()->json([
                    'status'  => 400,
                    'redirectUrl' => url('/user/login'),
                    'message' => 'Unavailable',
                ], 400);
            }
        }
        else
        {
            return response()->json([
                'status'  => 400,
                'redirectUrl' => url('/user/login'),
                'message' => 'Unavailable',
            ], 400);
        }
    }
    public function logout(Request $request)
    {
        setcookie('sso_user', null, 1, '/');
        $this->guard('user')->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect('/');
    }

    public function returnPassword($email)
    {
        $user = User::where('email', $email)->first();

        return $user->password;
    }

    protected function guard()
    {
        return Auth::guard('user');
    }
}
