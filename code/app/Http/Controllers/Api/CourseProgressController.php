<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Libraries\FormatConverter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\CourseProgress;
use App\Course;
use App\User;

class CourseProgressController extends Controller
{
    public function set(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_matrix_id'   => 'required',
            'course_matrix_id' => 'required',
            'progress_name'    => 'required',
            'progress'         => 'required',
        ]);

        if ($validator->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some Parameters is required',
				'validators' => FormatConverter::parseValidatorErrors($validator),
			], 400);
        }
        
        // get course
        $user = User::where('user_id_matrix', $request->user_matrix_id)->first();
        if(is_null($user))
        {
            return response()->json([
                'status' => 404,
                'message' => 'The user matrix id Not Found',
            ], 404);
        }

        // get course
        $course = Course::where('id_matrix', $request->course_matrix_id)->first();
        if(is_null($course))
        {
            return response()->json([
                'status' => 404,
                'message' => 'The course matrix id Not Found',
            ], 404);
        }
        // check progress course
        $courseProgress = CourseProgress::where('user_matrix_id', $request->user_matrix_id)->where('course_matrix_id', $request->course_matrix_id)->first();
        // create new progress
        if(is_null($courseProgress))
        {
            $courseProgress = new CourseProgress();
            $courseProgress->fill($request->only([
                'user_matrix_id',
                'course_matrix_id',
                'progress_name',
                'progress',
            ]));

            $courseProgress->save();

            return response()->json([
                'status' => 200,
                'message' => 'Success Created',
            ], 200);
        }
        // update progress
        else
        {
            $requestData = $request->all();
            $courseProgress->update($requestData);

            return response()->json([
                'status'  => 200,
                'message' => 'Success Updated',
            ]);
        }
    }
}
