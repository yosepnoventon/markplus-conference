<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Message;
use Illuminate\Http\Request;
use DB;
use File;

use Yajra\Datatables\Datatables;

class MessageController extends Controller
{
    public function __construct()
    {
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $this->middleware('auth:admin');
    }

    public function index()
    {
        return view('admin.message.index');
    }

    public function create()
    {
        return view('admin.message.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'images' => 'mimes:jpeg,jpg,png',
        ]);

        $files = $request['images'];

        if ($files != "")
        {
            $path = 'upload/message';
            $name = date('YmdHis') . '_' . $files->getClientOriginalName();
            $files->move($path, $name);
            $request['image'] = $name;
        }

        $request['slug'] = str_slug($request['title'], '-');
        $requestData = $request->all();

        $created = Message::create($requestData);

        if ($created)
        {
            $notification = array(
                'message' => 'Your data added!',
                'alert-type' => 'success'
            );

            return redirect('admin/messages')->with($notification);
        }
        else
        {
            $notification = array(
                'message' => 'Failed create data!',
                'alert-type' => 'error'
            );

            return back()->with($notification);
        }
    }

    public function show($id)
    {
        $message = Message::findOrFail($id);

        return view('admin.message.show', compact('message'));
    }

    public function edit($id)
    {
        $message = Message::findOrFail($id);

        return view('admin.message.edit', compact('message'));
    }

    public function update($id, Request $request)
    {
        $updated = Message::findOrFail($id);

        $request->validate([
            'title' => 'required',
            'images' => 'mimes:jpeg,jpg,png',
        ]);

        $files = $request['images'];

        $path = 'upload/message';

        if ($files != "")
        {
            if (File::exists($path . '/' . $updated->image))
            {
                File::delete($path . '/' . $updated->image);
                $name = date('YmdHis') . '_' . $files->getClientOriginalName();
                $files->move($path, $name);
                $request['image'] = $name;
            }
            else
            {
                $name = date('YmdHis') . '_' . $files->getClientOriginalName();
                $files->move($path, $name);
                $request['image'] = $name;
            }
        }

        $request['slug'] = str_slug($request['title'], '-');

        $requestData = $request->all();

        $updated->update($requestData);

        if ($updated)
        {
            $notification = array(
                'message' => 'Your data updated!',
                'alert-type' => 'success'
            );

            return redirect('admin/messages')->with($notification);
        }
        else
        {
            $notification = array(
                'message' => 'Failed to update data!',
                'alert-type' => 'error'
            );

            return back()->with($notification);
        }
    }

    public function destroy($id)
    {
        Message::destroy($id);

        $notification = array(
            'message' => 'Success delete',
            'alert-type' => 'success'
        );

        return redirect('admin/messages')->with($notification);
    }

    public function anyData()
    {
        DB::statement(DB::raw('set @rownum=0'));
        $message = Message::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'messages.*']);

        return Datatables::of($message)
            ->editColumn('image', function ($message)
            {
                if ($message->image != '')
                {
                    return '<img src="' . url('/upload/message/') . '/' . $message->image . '" width="150px">';
                }
                else
                {
                    return '<span>NO IMAGE</span>';
                }
            })
            ->addColumn('action', function ($message)
            {
                return '<a href="messages/' . $message->id . '/edit" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i> Edit </a>
                        <a onclick="deleteData(' . $message->id . ')" class="btn btn-danger" style="color: #fff !important"><i class="fa fa-trash"></i> Delete </a>';
            })
            ->rawColumns(['action', 'image'])
            ->make(true);
    }
}
