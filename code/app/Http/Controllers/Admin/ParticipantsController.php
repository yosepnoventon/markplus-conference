<?php

namespace App\Http\Controllers\Admin;

use App\Voucher;
use App\Login;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Config\EmailFunction;
use Illuminate\Http\Request;
use App\Exports\CsvExportParticipans;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Illuminate\Support\Facades\Log;
use Yajra\Datatables\Datatables;

class ParticipantsController extends Controller
{
    public function __construct()
    {
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $this->middleware('auth:admin');
    }

    public function index()
    {
        return view('admin.participants.index');
    }

    public function create()
    {
        return view('admin.participants.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'code_voucher' => 'required|unique:conf_vouchers,code_voucher|max:10',
            'email' => 'required',
            'quota' => 'required'
        ]);

        $requestData = $request->all();
        $requestData['remaining_quota'] = $request->quota;

        $voucher = Login::create($requestData);

        if ($voucher)
        {
            // send voucher
            app(EmailFunction::class)->send_vouchers($voucher);

            $notification = array(
                'message' => 'Success Send Voucher to '.$voucher->email,
                'alert-type' => 'success'
            );

            return redirect('admin/vouchers')->with($notification);
        }
        else
        {
            $notification = array(
                'message' => 'Failed create data!',
                'alert-type' => 'error'
            );

            return back()->with($notification);
        }
    }

    public function show($id)
    {
        $Voucher = Login::findOrFail($id);

        return view('admin.participants.show', compact('Voucher'));
    }

    public function edit($id)
    {
        $voucher = Login::findOrFail($id);

        return view('admin.participants.edit', compact('voucher'));
    }

    public function update($id, Request $request)
    {
        $updated = Log::findOrFail($id);

        $request->validate([
            'address' => 'required',
            'url' => 'required',
            'phone' => 'required'
        ]);

        $requestData = $request->all();

        $updated->update($requestData);

        if ($updated)
        {
            $notification = array(
                'message' => 'Your data updated!',
                'alert-type' => 'success'
            );

            return redirect('admin/vouchers')->with($notification);
        }
        else
        {
            $notification = array(
                'message' => 'Failed to update data!',
                'alert-type' => 'error'
            );

            return back()->with($notification);
        }
    }

    public function destroy($id)
    {
        Login::destroy($id);

        $notification = array(
            'message' => 'Success delete',
            'alert-type' => 'success'
        );

        return redirect('admin/vouchers')->with($notification);
    }

    public function anyData()
    {
        DB::statement(DB::raw('set @rownum=0'));
        $voucher = Login::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'conf_logins.*'
        ]);

        return Datatables::of($voucher)
            ->make(true);
    }

    function csv_export()
    {
		return Excel::download(new CsvExportParticipans, 'report-participans.xlsx');
    }
}
