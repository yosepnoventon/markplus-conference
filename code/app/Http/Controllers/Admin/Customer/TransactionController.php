<?php

namespace App\Http\Controllers\Admin\Customer;

use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Http\Request;
use App\Orders;
use App\Course;
use App\DetailCourse;
use App\DetailOrders;
use App\CourseTicketPrices;
use App\LearnMethod;
use App\Http\Controllers\Config\CodemiFunction;
use App\Http\Controllers\Web\Log\PaymentController;
use App\Http\Controllers\Config\EmailFunction;
use Session;
use DB;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;

class TransactionController extends Controller
{
    public function __construct()
    {
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        Carbon::setLocale('id');

        $this->middleware('auth:admin');
    }

    public function transaction($id)
    {
        $courses = Course::select('id', 'category_matrix', 'learn_method', 'title')->where('is_active', 1)->get();
        $customer = User::findOrFail($id);

        return view('admin.customer.transaction', compact('customer', 'courses', 'id'));
    }

    public function transaction_data($id)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $transaction = DB::table('courses')
                    ->leftJoin('added_courses_matrix', 'courses.id', '=', 'added_courses_matrix.course_id')
                    ->select([DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'courses.title as course_title', 'courses.id_matrix as course_id_matrix', 'courses.category_matrix as course_category', 'courses.learn_method_matrix as course_learn_method', 'added_courses_matrix.detail_course_id', 'added_courses_matrix.course_ticket_price_id', 'added_courses_matrix.user_id', 'added_courses_matrix.status_code', 'added_courses_matrix.id as id_added', 'added_courses_matrix.message'])
                    ->where('added_courses_matrix.user_id', $id)
                    ->get();

        return Datatables::of($transaction)
        ->editcolumn('status', function ($transaction)
        {
            if($transaction->status_code == 200 || $transaction->status_code == 202)
            {
                return '<p style="color: green"> Added Matrix</p>';
            }
        })
        ->editcolumn('course_id_matrix', function ($transaction)
        {
            if(!is_null($transaction->course_id_matrix))
            {
                return $transaction->course_id_matrix;
            }
            else
            {
                return '';
            }
        })
        ->addColumn('action', function ($transaction)
        {
            $user = User::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'users.email', 'users.user_id_matrix'])->where('id', $transaction->user_id)->first();
            if($transaction->status_code == 200 || $transaction->status_code == 202)
            {
                if(is_null($transaction->course_ticket_price_id))
                {
                    return '<a href="deactivate/'. $user->email.'/'. $transaction->course_id_matrix.'" class="btn btn-danger"><i class="fa fa-remove"></i> Deactivate </a>';
                }
                else
                {
                    return '<a href="deactivate/'. $user->email.'/'. $transaction->course_id_matrix.'/'. $transaction->detail_course_id.'/'. $transaction->course_ticket_price_id.'" class="btn btn-danger"><i class="fa fa-remove"></i> Deactivate </a>';
                }
            }
        })
        ->rawColumns(['action', 'course_id_matrix', 'status'])
        ->make(true);
    }

    public function order(Request $request){
        $learn_method           = $request->learn_method;
        $user_id                = $request->user_id;
        $course_id              = $request->course_id;
        $course_detail_id       = $request->course_detail_id;
        $course_ticket_price_id = $request->course_ticket_price_id;
        $time                   = $request->time;
        $data                   = array();

        $user = User::findOrFail($user_id);

        //create Orders
        $data['invoice'] = 'INV-' . date('YmdHis') . '-' . str_pad($user_id, 10, '0', STR_PAD_LEFT);
        $data['user']    = $user_id;








        // Webinar Order
        if($learn_method == LearnMethod::WEBINAR)
        {
            $check = DetailOrders::where('learning_method_id', $learn_method)->where('course_id', $course_id)->where('detail_course_id', $course_detail_id)->where('user_id', $user_id)->where('status', DetailOrders::SUCCESS)->count();
            if($check > 0)
            {
                return array(
                    'message' => 'Order Data Exsist',
                    'alert-type' => 'success',
                    'code' => 202
                );
            }
            $course_detail = DetailCourse::where('id', $course_detail_id)->first();
            $course = Course::where('id', $course_id)->first();

            // count all data order success by ticket
            $countOrder = DetailOrders::where('learning_method_id', $learn_method)
                        ->where('course_id', $course_id)
                        ->where('detail_course_id',$course_detail_id)
                        ->where('course_ticket_price_id', $course_ticket_price_id)
                        ->where('status', DetailOrders::SUCCESS)
                        ->count();
            $course_ticket_webinar  = CourseTicketPrices::where('id', $course_ticket_price_id)->first();
            // if quota maximum
            if ($countOrder >= $course_ticket_webinar->maximum_user_webinar)
            {
                $course_ticket_webinar->update(['status' => 1]);

                $notification = array(
                    'message' => 'Quota has been reached!',
                    'alert-type' => 'error',
                    'code' => 400,
                );

                return $notification;
            }

            //create Orders ===============================================================
            $data['price']          = $course_ticket_webinar->price;
            $data['arrayPayment']   = '';
            $data['status_code']    = 200;
            $data['status']         = "Order Success By Admin";
            $data['order_status']   = 1;
            $data['created_by']     = Auth::guard('admin')->user()->id;
            
            $order_id = Orders::insertGetId($data);
            
            //check booking code exsist
            do {
                $random = $this->randomString(8);
                $result = DetailOrders::where('booking_code', '=', $random)->count();
            } while ($result > 0);

            // Create detail order ============================================================
            $dataOrderDetail["order_id"]                = $order_id;
            $dataOrderDetail["booking_code"]            = $random;
            $dataOrderDetail["learning_method_id"]      = $learn_method;
            $dataOrderDetail["course_id"]               = $course_id;
            $dataOrderDetail["is_certification"]        = null;
            $dataOrderDetail["subscription_id"]         = null;
            $dataOrderDetail["detail_course_id"]        = $course_detail_id;
            $dataOrderDetail["course_ticket_price_id"]  = $course_ticket_price_id;
            $dataOrderDetail["user_id"]                 = $user_id;
            $dataOrderDetail["price"]                   = $course_ticket_webinar->price;
            $dataOrderDetail["start_date"]              = null;
            $dataOrderDetail["expire_date"]             = null;
            $dataOrderDetail["status"]                  = DetailOrders::SUCCESS;
            $dataOrderDetail['created_by']              = Auth::guard('admin')->user()->id;

            $order_detail_id = DetailOrders::insertGetId($dataOrderDetail);

            // Create logs =======================================================================
            $array['invoice']                = $data['invoice'];
            $array['detail_order_id']        = $order_detail_id;
            $array['subscription']           = null;
            $array['course']                 = $course_id;
            $array['detail_course_id']       = $course_detail_id;
            $array['course_ticket_price_id'] = $course_ticket_price_id;
            $array['user']                   = $user_id;
            $array['type']                   = 'Non Subscription';
            $array['total']                  = $course_ticket_webinar->price;
            $array['status_code']            = 200;
            $array['status']                 = "Success";
            $array['description']            = "Success Booked by Admin";
            $array['start_date']             = null;
            $array['end_date']               = null;

            app(PaymentController::class)->Orderlog($array);

            //sent email -------------------------------------------------------------
            app(EmailFunction::class)->webinar($user, $course_detail_id);
            
            //create logs course and user =======================================================================
            $status_code = 200;
            $message = "Success Add User and Course By Admin";
            $dataLog = array(
                'course_id'              => $course_id,
                'detail_course_id'       => $course_detail_id,
                'course_ticket_price_id' => $course_ticket_price_id
            );
            app(PaymentController::class)->create_user_course_matrix($dataLog, $user->id, $status_code, $message);
            
            $notification = array(
                'message' => 'Success booked By Admin',
                'alert-type' => 'success',
                'code' => 200
            );

            return $notification;
        }





















        // Certificate Order
        else if($learn_method == LearnMethod::CERTIFICATION)
        {
            $check = DetailOrders::where('learning_method_id', $learn_method)->where('course_id', $course_id)->where('user_id', $user_id)->where('status', DetailOrders::SUCCESS)->count();
            if($check > 0)
            {
                return array(
                    'message' => 'Order Data Exsist',
                    'alert-type' => 'success',
                    'code' => 202
                );
            }
            
            $course = Course::where('id', $course_id)->first();
            
            //create Orders ===============================================================
            $data['price']          = $course->price;
            $data['arrayPayment']   = '';
            $data['status_code']    = 200;
            $data['status']         = "Order Success By Admin";
            $data['order_status']   = 1;
            $data['created_by']     = Auth::guard('admin')->user()->id;
            
            $order_id = Orders::insertGetId($data);

            do {
                $random = $this->randomString(8);
                $result = DetailOrders::where('booking_code', '=', $random)->count();
            } while ($result > 0);

            // Create detail order ======================================================================
            $dataOrderDetail["order_id"]                = $order_id;
            $dataOrderDetail["booking_code"]            = $random;
            $dataOrderDetail["learning_method_id"]      = $learn_method;
            $dataOrderDetail["course_id"]               = $course_id;
            $dataOrderDetail["is_certification"]        = null;
            $dataOrderDetail["subscription_id"]         = null;
            $dataOrderDetail["detail_course_id"]        = null;
            $dataOrderDetail["course_ticket_price_id"]  = null;
            $dataOrderDetail["user_id"]                 = $user_id;
            $dataOrderDetail["price"]                   = $course->price;
            $dataOrderDetail["start_date"]              = Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s');
            $dataOrderDetail["expire_date"]             = Carbon::parse($course->finish_date)->format('Y-m-d H:i:s');
            $dataOrderDetail["status"]                  = DetailOrders::SUCCESS;

            $detail_id = DetailOrders::insertGetId($dataOrderDetail);
            
            //create order E-Learning free from certificate ===================================================
            $this->subscriptionAutoComplete($course, $detail_id, $order_id);

            // if certificate start learning and send Subscription
            if(Carbon::parse($course->start_date)->format('Y-m-d') <= Carbon::now('Asia/Jakarta')->format('Y-m-d'))
            {
                $added = app(CodemiFunction::class)->enroll_by_course_and_email($course->id_matrix, $user->email);
                
                // all data Certification Success Enrolled
                if(is_null($added['data']['failed']))
                {
                    //create logs addes course and user to lms
                    $dataLog = array(
                        'course_id'              => $course->id,
                        'detail_course_id'       => null,
                        'course_ticket_price_id' => null
                    );
                    $status_code = 200;
                    $message = "Success Add User to LMS";

                    app(PaymentController::class)->create_user_course_matrix($dataLog, $user->id, $status_code, $message);
                }
                // there is a problem of failure in the course
                else
                {
                    //create logs addes course and user to lms
                    $dataLog = array(
                        'course_id'              => $course->id,
                        'detail_course_id'       => null,
                        'course_ticket_price_id' => null
                    );
                    $status_code = 400;
                    $message = "Failed Add User to LMS";

                    app(PaymentController::class)->create_user_course_matrix($dataLog, $user->id, $status_code, $message);

                    // if data more than 0
                    if(count($added['data']['success']) > 0)
                    {
                        //create logs addes course and user to lms
                        $dataLog = array(
                            'course_id'              => $course->id,
                            'detail_course_id'       => null,
                            'course_ticket_price_id' => null
                        );
                        $status_code = 200;
                        $message = "Success Add User to LMS";

                        app(PaymentController::class)->create_user_course_matrix($dataLog, $user->id, $status_code, $message);
                    }
                }

                //data logs ==================================================================
                $array['invoice']                = $data['invoice'];
                $array['detail_order_id']        = $detail_id;
                $array['subscription']           = null;
                $array['course']                 = $course->id;
                $array['detail_course_id']       = null;
                $array['course_ticket_price_id'] = null;
                $array['user']                   = $user->id;
                $array['type']                   = 'Subscription + Certificate';
                $array['total']                  = null;
                $array['status_code']            = 200;
                $array['status']                 = "Success";
                $array['description']            = "Success Add User to LMS and Free E-Learning By Admin";
                $array['start_date']             = $dataOrderDetail["start_date"];
                $array['end_date']               = $dataOrderDetail["expire_date"];
                //create log
                app(PaymentController::class)->Orderlog($array);

                //sent email
                app(EmailFunction::class)->certification($user, Carbon::parse($course->finish_date)->addDays(7)->format('d F Y'));

                $notification = array(
                    'message' => 'Success booked By Admin',
                    'alert-type' => 'success',
                    'code' => 200
                );

                return $notification;
            }
            // update certificate waiting for schedule start_date and send Subscription
            else
            {
                // Create data logs ==================================================================
                $array['invoice']                = $data['invoice'];
                $array['detail_order_id']        = $detail_id;
                $array['subscription']           = null;
                $array['course']                 = $course->id;
                $array['detail_course_id']       = null;
                $array['course_ticket_price_id'] = null;
                $array['user']                   = $user->id;
                $array['type']                   = 'Subscription + Certificate';
                $array['total']                  = null;
                $array['status_code']            = 200;
                $array['status']                 = "Success";
                $array['description']            = "Success Add User to LMS and Free E-Learning By Admin";
                $array['start_date']             = $dataOrderDetail["start_date"];
                $array['end_date']               = $dataOrderDetail["expire_date"];
                //create log
                app(PaymentController::class)->Orderlog($array);

                //sent email
                app(EmailFunction::class)->certification($user, Carbon::parse($course->finish_date)->addDays(7)->format('d F Y'));

                $notification = array(
                    'message' => 'Success booked By Admin',
                    'alert-type' => 'success',
                    'code' => 200
                );

                return $notification;
            }









        }
        // E-Learning Order
        else if($learn_method == LearnMethod::ELEARNING)
        {
            $check = DetailOrders::where('learning_method_id', $learn_method)->whereNull('course_id')->where('user_id', $user_id)->where('status', DetailOrders::SUCCESS)->where('expire_date', '>=', Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s'))->count();
            if($check > 0)
            {
                return array(
                    'message' => 'Order Data Exsist'.$check,
                    'alert-type' => 'error',
                    'code' => 202
                );
            }

            //create Orders ===============================================================
            $data['price']          = 0;
            $data['arrayPayment']   = '';
            $data['status_code']    = 200;
            $data['status']         = "Order Success By Admin";
            $data['order_status']   = 1;
            $data['created_by']     = Auth::guard('admin')->user()->id;
            
            $order_id = Orders::insertGetId($data);

            $time_split         = explode(" - ",$time);
            $start_date_split   = $time_split[0];
            $expire_date_split  = $time_split[1];
            $start_date         = Carbon::parse($start_date_split)->format('Y-m-d H:i:s');
            $expire_date        = Carbon::parse($expire_date_split)->format('Y-m-d H:i:s');

            // Create detail order ===============================================================
            $dataOrderDetail["order_id"]                = $order_id;
            $dataOrderDetail["booking_code"]            = null;
            $dataOrderDetail["learning_method_id"]      = $learn_method;
            $dataOrderDetail["course_id"]               = null;
            $dataOrderDetail["is_certification"]        = null;
            $dataOrderDetail["subscription_id"]         = null;
            $dataOrderDetail["detail_course_id"]        = null;
            $dataOrderDetail["course_ticket_price_id"]  = null;
            $dataOrderDetail["user_id"]                 = $user->id;
            $dataOrderDetail["price"]                   = 0;
            $dataOrderDetail["start_date"]              = $start_date;
            $dataOrderDetail["expire_date"]             = $expire_date;
            $dataOrderDetail["status"]                  = DetailOrders::SUCCESS;

            $detail_id = DetailOrders::insertGetId($dataOrderDetail);

            // Create data logs ===================================================================
            $array['invoice']                = $data['invoice'];
            $array['detail_order_id']        = $detail_id;
            $array['subscription']           = null;
            $array['course']                 = null;
            $array['detail_course_id']       = null;
            $array['course_ticket_price_id'] = null;
            $array['user']                   = $user->id;
            $array['type']                   = 'Subscription';
            $array['total']                  = 0;
            $array['status_code']            = 200;
            $array['status']                 = "Success";
            $array['description']            = 'Success Add User to LMS By Admin';
            $array['start_date']             = $start_date;
            $array['end_date']               = $expire_date;

            app(PaymentController::class)->Orderlog($array);

            $id_matrix_array = array();
            $courses = Course::where('learn_method', LearnMethod::ELEARNING)->where('is_active', Course::ACTIVE)->get();

            foreach($courses as $course)
            {
                // push array to $id_matrix_array
                array_push($id_matrix_array,$course->id_matrix);
            }
            // Multiple Course id (separated by coma “,”)
            $id_matrix = implode(",",$id_matrix_array);
            $added = app(CodemiFunction::class)->enroll_by_course_and_email($id_matrix, $user->email);

            // all data E-Learning Success Enrolled
            if(is_null($added['data']['failed']))
            {
                // loop data course success & create logs course and user
                for($s = 0; $s < count($added['data']['success']); ++$s)
                {
                    $dataLog = array(
                        'course_id'              => Course::where('id_matrix', $added['data']['success'][$s])->where('is_active', Course::ACTIVE)->first()->id,
                        'detail_course_id'       => null,
                        'course_ticket_price_id' => null
                    );
                    $status_code = 200;
                    $message = "Success Add User to LMS";

                    app(PaymentController::class)->create_user_course_matrix($dataLog, $user->id, $status_code, $message);
                }
            }
            // there is a problem of failure in the course
            else
            {
                // loop data course failed & create logs course and user
                for($f = 0; $f < count($added['data']['failed']); ++$f)
                {
                    $dataLog = array(
                        'course_id'              => Course::where('id_matrix', $added['data']['failed'][$f])->where('is_active', Course::ACTIVE)->first()->id,
                        'detail_course_id'       => null,
                        'course_ticket_price_id' => null
                    );
                    $status_code = 400;
                    $message = "Failed Add User to LMS";

                    app(PaymentController::class)->create_user_course_matrix($dataLog, $user->id, $status_code, $message);
                }

                // if data more than 0
                if(count($added['data']['success']) > 0)
                {
                    // loop data course success & create logs course and user
                    for($s = 0; $s < count($added['data']['success']); ++$s)
                    {
                        $dataLog = array(
                            'course_id'              => Course::where('id_matrix', $added['data']['success'][$s])->where('is_active', Course::ACTIVE)->first()->id,
                            'detail_course_id'       => null,
                            'course_ticket_price_id' => null
                        );
                        $status_code = 200;
                        $message = "Success Add User to LMS";

                        app(PaymentController::class)->create_user_course_matrix($dataLog, $user->id, $status_code, $message);
                    }
                }
            }

            //sent email
            app(EmailFunction::class)->subscription($user, null, Carbon::parse($expire_date)->format('d F Y'));


            $notification = array(
                'message' => 'Success booked By Admin',
                'alert-type' => 'success',
                'code' => 200
            );
        }

        return $notification;
    }

    public function subscriptionAutoComplete($courses, $detail_id, $order_id)
    {
        $order = Orders::FindOrFail($order_id);
        $user = User::FindOrFail($order->user);
        $check = DetailOrders::whereNull('course_id')->whereNull('detail_course_id')->where('user_id', $order->user)->where('status', DetailOrders::SUCCESS)->where('expire_date', '>=', Carbon::parse($courses->finish_date)->addDays(7)->format('Y-m-d H:i:s'))->count();
        // if data Order E-Learning Not Found
        if($check <= 0)
        {
            // Create Detail Order E-Learning ===================================================================
            $dataOrderDetail["order_id"]                = $order_id;
            $dataOrderDetail["booking_code"]            = null;
            $dataOrderDetail["learning_method_id"]      = LearnMethod::ELEARNING;
            $dataOrderDetail["course_id"]               = null;
            $dataOrderDetail["is_certification"]        = $detail_id;
            $dataOrderDetail["subscription_id"]         = null;
            $dataOrderDetail["detail_course_id"]        = null;
            $dataOrderDetail["course_ticket_price_id"]  = null;
            $dataOrderDetail["user_id"]                 = $order->user;
            $dataOrderDetail["price"]                   = 0;
            $dataOrderDetail["start_date"]              = Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s');
            $dataOrderDetail["expire_date"]             = Carbon::parse($courses->finish_date)->addDays(7)->format('Y-m-d H:i:s');
            $dataOrderDetail["status"]                  = DetailOrders::SUCCESS;
            $dataOrderDetail['created_by']              = Auth::guard('admin')->user()->id;

            $order_detail_id = DetailOrders::insertGetId($dataOrderDetail);
            
            $id_matrix_array = array();
            $courses_ = Course::where('learn_method', LearnMethod::ELEARNING)->where('is_active', Course::ACTIVE)->get();
            foreach($courses_ as $course)
            {
                // push array to $id_matrix_array
                array_push($id_matrix_array,$course->id_matrix);
            }
            // Multiple Course id (separated by coma “,”)
            $id_matrix = implode(",",$id_matrix_array);
            $added = app(CodemiFunction::class)->enroll_by_course_and_email($id_matrix, $user->email);

            // all data E-Learning Success Enrolled
            if(is_null($added['data']['failed']))
            {
                // loop data course success & create logs course and user
                for($s = 0; $s < count($added['data']['success']); ++$s)
                {
                    $dataLog = array(
                        'course_id'              => Course::where('id_matrix', $added['data']['success'][$s])->where('is_active', Course::ACTIVE)->first()->id,
                        'detail_course_id'       => null,
                        'course_ticket_price_id' => null
                    );
                    $status_code = 200;
                    $message = "Success Add User to LMS";

                    app(PaymentController::class)->create_user_course_matrix($dataLog, $user->id, $status_code, $message);
                }
            }
            // there is a problem of failure in the course
            else
            {   
                // loop data course failed & create logs course and user
                for($f = 0; $f < count($added['data']['failed']); ++$f)
                {
                    $dataLog = array(
                        'course_id'              => Course::where('id_matrix', $added['data']['failed'][$f])->where('is_active', Course::ACTIVE)->first()->id,
                        'detail_course_id'       => null,
                        'course_ticket_price_id' => null
                    );
                    $status_code = 400;
                    $message = "Failed Add User to LMS";

                    app(PaymentController::class)->create_user_course_matrix($dataLog, $user->id, $status_code, $message);
                }

                // if data more than 0
                if(count($added['data']['success']) > 0)
                {
                    // loop data course success & create logs course and user
                    for($s = 0; $s < count($added['data']['success']); ++$s)
                    {
                        $dataLog = array(
                            'course_id'              => Course::where('id_matrix', $added['data']['success'][$s])->where('is_active', Course::ACTIVE)->first()->id,
                            'detail_course_id'       => null,
                            'course_ticket_price_id' => null
                        );
                        $status_code = 200;
                        $message = "Success Add User to LMS";

                        app(PaymentController::class)->create_user_course_matrix($dataLog, $user->id, $status_code, $message);
                    }
                }
            }

            // create data logs ===================================================================
            $array['invoice']                = $order->invoice;
            $array['detail_order_id']        = $order_detail_id;
            $array['subscription']           = null;
            $array['course']                 = null;
            $array['detail_course_id']       = null;
            $array['course_ticket_price_id'] = null;
            $array['user']                   = $user->id;
            $array['type']                   = 'Subscription From Certification';
            $array['total']                  = null;
            $array['status_code']            = 200;
            $array['status']                 = "Success";
            $array['description']            = 'Success Add User to LMS By Admin';
            $array['start_date']             = $dataOrderDetail["start_date"];
            $array['end_date']               = $dataOrderDetail["expire_date"];
            
            app(PaymentController::class)->Orderlog($array);
        }
    }

    function randomString($length)
    {
        $str = "";
        $characters = array_merge(range('A', 'Z'), range('0', '9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }

    //-----------------remove_students_from_class-----------------//
    public function deactive_from_matrix($email, $course_id, $detail_course_id = null, $course_ticket_price_id = null)
    {
        $user = User::where('email', $email)->first();
        $course = Course::where('id_matrix', $course_id)->first();
        if(is_null($course_ticket_price_id))
        {
            $remove = app(CodemiFunction::class)->unenroll_by_course_and_email($course->id_matrix, $user->email);
            // all data E-Learning Success Enrolled
            if(is_null($remove['data']['failed']))
            {
                // loop data course success & create logs course and user
                for($s = 0; $s < count($remove['data']['success']); ++$s)
                {
                    $dataLog = array(
                        'course_id'              => Course::where('id_matrix', $remove['data']['success'][$s])->first()->id,
                        'detail_course_id'       => null,
                        'course_ticket_price_id' => null
                    );
                    
                    app(PaymentController::class)->remove_user_course_matrix($dataLog, $user->id);
                }

                $notification = array(
                    'message' => 'Success Remove Course \"'.$course->title.'\" By Admin',
                    'alert-type' => 'success'
                );
                return redirect('admin/customers/transaction/'.$user->id)->with($notification);
            }
            // there is a problem of failure in the course
            else
            {
                $notification = array(
                    'message' => 'Failed Remove Course By Admin',
                    'alert-type' => 'error'
                );
                return redirect('admin/customers/transaction/'.$user->id)->with($notification);
            }
        }
        else
        {
            $dataLog = array(
                'course_id'              => $course->id,
                'detail_course_id'       => $detail_course_id,
                'course_ticket_price_id' => $course_ticket_price_id
            );
            app(PaymentController::class)->remove_user_course_matrix($dataLog, $user->id);

            $notification = array(
                'message' => 'Success Remove Course \"'.$course->title.'\" By Admin',
                'alert-type' => 'success'
            );
            return redirect('admin/customers/transaction/'.$user->id)->with($notification);
        }

    }
    
    public function remember_subscribe(Request $request)
    {
        // order expired h -1 E-Learning
        $orders = DB::table('orders')
                ->select('orders.*', 'users.email')
                ->leftJoin('users', 'users.id', '=', 'orders.user')
                ->where('status_code', '200')
                ->where('expire_date', '>=', Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s'))
                ->get();

        foreach($orders as $order)
        {
            if(round(Carbon::now('Asia/Jakarta')->floatDiffInDays($order->expire_date)) == 1 || round(Carbon::now('Asia/Jakarta')->floatDiffInDays($order->expire_date)) == 0)
            {
                app('App\Http\Controllers\Config\EmailFunction')->remember_subscribe($order);
            }
        }
        
        $notification = array(
            'message' => 'send email to customers',
            'title' => 'Success',
            'alert_type' => 'success',
            'code' => 200
        );

        return $notification;
    }

    //remove students from class LMS
    public function remove_students_from_class(Request $request)
    {
        // order expired E-Learning
        $orders = DB::table('orders')
                ->select('orders.*', 'users.email', 'users.user_id_matrix')
                ->leftJoin('users', 'users.id', '=', 'orders.user')
                ->where('status_code', '200')
                ->where('expire_date', '<=', Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s'))
                ->get();
        try {
            foreach($orders as $no => $order)
            {
                $orders_active = DB::table('orders')
                                ->select('orders.*', 'users.email', 'users.user_id_matrix')
                                ->leftJoin('users', 'users.id', '=', 'orders.user')
                                ->where('orders.status_code', '200')
                                ->where('orders.expire_date', '>=', Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s'))
                                ->where('orders.user', $order->user)
                                ->where('users.user_id_matrix', '!=', null)
                                ->first();
                if(!isset($orders_active))
                {
                    $courses = DB::table('added_courses_matrix')
                        ->select('added_courses_matrix.*', 'courses.id_matrix')
                        ->leftJoin('courses', 'courses.id', '=', 'added_courses_matrix.course_id')
                        ->where('learn_method_matrix', Course::LEARNING)
                        ->where('user_id', $order->user)
                        ->get();
                    foreach($courses as $course)
                    {
                        app('App\Http\Controllers\Matrix\MatrixController')->remove_students_from_class($course->id_matrix, $order->user_id_matrix);
                        DB::table('added_courses_matrix')
                        ->where('user_id', $order->user)
                        ->where('course_id', $course->course_id)
                        ->delete();
                    }
                    DB::table('orders')
                        ->select('orders.*', 'users.email', 'users.user_id_matrix')
                        ->leftJoin('users', 'users.id', '=', 'orders.user')
                        ->where('status_code', '200')
                        ->where('expire_date', '<=', Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s'))
                        ->where('user', $order->user)
                        ->update(['order_status' => 0]);
                }
            }
            
            $notification = array(
                'message' => 'synchronize remove students to class',
                'title' => 'Success',
                'alert_type' => 'success',
                'code' => 200
            );
    
            return $notification;
        } catch (Exception $e) {
            $notification = array(
                'message' => 'failed, try again',
                'title' => 'Failed',
                'alert_type' => 'error',
                'code' => 400
            );
    
            return $notification;
        }
    }

    //Send Certification to LMS
    public function synco_certificate(Request $request)
    {
        // Order Start Certificate
        $orders = DB::table('orders')
                ->select('orders.*', 'users.email', 'users.user_id_matrix')
                ->leftJoin('users', 'users.id', '=', 'orders.user')
                ->where('orders.subscription', null)
                ->where('orders.course','!=', null)
                ->where('orders.detail_course', null)
                ->where('orders.status_code', '200')
                ->where('start_date', '<=', Carbon::now('Asia/Jakarta')->format('Y-m-d'))
                ->where('users.user_id_matrix', '!=', null)
                ->get();
        try {
            foreach($orders as $no => $order)
            {
                $course = Course::where('id', $order->course)->first();
                $user = User::where('id', $order->user)->first();
                $added = app('App\Http\Controllers\Matrix\MatrixController')->add_students_to_class($course->id_matrix, $user->user_id_matrix);
                if ($added)
                {
                    //create logs addes course and user to matrix
                    $status_code = 200;
                    $message = "Success Add User and Course";

                    app(PaymentController::class)->create_user_course_matrix($course->id, $user->id, $status_code, $message);
                }
            }
            $notification = array(
                'message' => 'synchronize start to certificate',
                'title' => 'success',
                'alert_type' => 'success',
                'code' => 200
            );
    
            return $notification;
        } catch (Exception $e) {
            $notification = array(
                'message' => 'failed, try again',
                'title' => 'error',
                'alert_type' => 'error',
                'code' => 400
            );
    
            return $notification;
        }
    }

}
