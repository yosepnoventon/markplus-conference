<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Orders;
use App\Promo;
use App\Course;
use App\LearnMethod;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use File;

use Yajra\Datatables\Datatables;

class ReportPromoController extends Controller
{
    public function __construct()
    {
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $this->middleware('auth:admin');
    }

    public function promo_used()
    {
        return view('admin.promo.report-used');
    }

    public function promo_detail_used()
    {
        return view('admin.promo.report-detail-used');
    }

    public function anyDataUsed()
    {
        DB::statement(DB::raw('set @rownum=0'));
        $promo = Promo::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'promos.*'
        ])->get();

        return Datatables::of($promo)
            ->editColumn('quota_used', function ($promo)
            {
                DB::statement(DB::raw('set @rownum=0'));
                $order = Orders::where('promo_id', $promo->id)
                    ->where('status_code', 200)
                    ->where('promo_id', '!=', 0)
                    ->count();

                return $order;
            })
            ->editColumn('status', function ($promo)
            {
                DB::statement(DB::raw('set @rownum=0'));
                $promos_date_expire = Promo::where('code', $promo->code)->where('end_date', '>=', Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s'))->first();
                if($promo->status == '1')
                {
                    if(!$promos_date_expire)
                    {
                        return '<span style="color: red">Expired</span>';
                    }else{
                        return '<span style="color: green">Active</span>';
                    }
                }else
                {
                    if(!$promos_date_expire)
                    {
                        return '<span style="color: red">Expired</span>';
                    }else{
                        return '<span style="color: red">Inactive</span>';
                    }
                }
            })
            ->addColumn('action', function ($promo) {
                return '
                    <a href="promo-detail-used?code='.$promo->code . '" class="btn btn-info"><i class="fa fa-eye"></i> Detail </a>';
            })
            ->rawColumns(['quota_used', 'status', 'action'])
            ->make(true);
    }

    public function anyDataDetailUsed(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        if(isset($request->code))
        {
            $promo = Orders::select([
                DB::raw('@rownum  := @rownum  + 1 AS rownum'), DB::raw('CONCAT(users.first_name," ", users.last_name) AS name'),
                'orders.promo_code',
                'orders.promo_value',
                'orders.course',
                'orders.created_at',
                'users.email',
                'promos.learning_method_id',
            ])
            ->leftJoin('promos', 'promos.id', '=', 'orders.promo_id')
            ->leftJoin('users', 'users.id', '=', 'orders.user')
            ->where('orders.promo_code', $request->code)
            ->where('orders.status_code', 200)
            ->where('orders.promo_id', '!=', 0)
            ->get();
        }
        else
        {
            $promo = Orders::select([
                DB::raw('@rownum  := @rownum  + 1 AS rownum'), DB::raw('CONCAT(users.first_name," ", users.last_name) AS name'),
                'orders.promo_code',
                'orders.promo_value',
                'orders.course',
                'orders.created_at',
                'users.email',
                'promos.learning_method_id',
            ])
            ->leftJoin('promos', 'promos.id', '=', 'orders.promo_id')
            ->leftJoin('users', 'users.id', '=', 'orders.user')
            ->where('status_code', 200)
            ->where('promo_id', '!=', 0)
            ->get();
        }
        return Datatables::of($promo)
            ->editColumn('learning_method', function ($promo)
            {
                DB::statement(DB::raw('set @rownum=0'));
                $learnmethods = LearnMethod::where('id', $promo->learning_method_id)->first();
                if(!$learnmethods){
                    return "All Learn Method";
                }else{
                    return $learnmethods->title;
                }
            })
            ->editColumn('course', function ($promo)
            {
                if($promo->learning_method_id == "1")
                {
                    return "-";
                }
                else if($promo->learning_method_id == "2")
                {
                    DB::statement(DB::raw('set @rownum=0'));
                    $course = Course::where('courses.id', $promo->course)
                    ->first();
                    return $course->title;
                }else{
                    if($promo->course == null)
                    {
                        return "-";
                    }
                    else
                    {
                        DB::statement(DB::raw('set @rownum=0'));
                        $course = Course::where('courses.id', $promo->course)
                        ->first();
                        return $course->title;
                    }
                }
            })
            ->addColumn('created_at', function ($promo)
            {
                return Carbon::parse($promo->created_at)->format('d-F-Y H:i:s');
            })
            ->rawColumns(['learning_method', 'course', 'created_at'])
            ->make(true);
    }
}
