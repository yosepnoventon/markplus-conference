<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

use App\Admin;
use Illuminate\Http\Request;
use Session;
use DB;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class UsersController extends Controller
{
    public function __construct()
    {
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return redirect('/admin');

        return view('admin.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        if (Auth::guard('admin')->user()->role != Admin::ROLE_SUPERADMIN)
        {
            return redirect('/admin');
        }

        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required|min:6',
        ]);

        $request['password'] = Hash::make($request['password']);
        $requestData = $request->all();

        $admin = Admin::where('email', '=', $request['email'])->count();

        if($admin == 0)
        {
            Admin::create($requestData);

            $notification = array(
                'message' => 'Your data added!',
                'alert-type' => 'success'
            );

            return redirect('admin/users')->with($notification);
        }
        else
        {
            $notification = array(
                'message' => 'Email already exist!',
                'alert-type' => 'error'
            );

            return back()->with($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $admin = Admin::findOrFail($id);

        return view('admin.user.show', compact('admin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */

    public function edit($id)
    {
        $admin = Admin::findOrFail($id);

        return view('admin.user.edit', compact('admin'));
    }

    public function profile()
    {
        $id = Auth::guard('admin')->user()->id;
        $admin = Admin::findOrFail($id);

        return view('admin.user.edit', compact('admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $admin = Admin::findOrFail($id);

        $request->validate([
            'email' => 'required',
            'password' => 'required|min:6',
        ]);

        if($request->password != '')
        {
            $request['password'] = Hash::make($request['password']);
        }
        else
        {
            $request['password'] = $admin->password;
        }

        $requestData = $request->all();

        $admin->update($requestData);

        if($admin)
        {
            $notification = array(
                'message' => 'Your data updated!',
                'alert-type' => 'success'
            );

            return redirect('admin/users')->with($notification);
        }
        else
        {
            $notification = array(
                'message' => 'Failed to update data!',
                'alert-type' => 'error'
            );

            return back()->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Admin::destroy($id);

        $notification = array(
            'message' => 'Success delete',
            'alert-type' => 'success'
        );

        return redirect('admin/users')->with($notification);
    }

    public function anyData()
    {
        DB::statement(DB::raw('set @rownum=0'));
        $admin = Admin::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'admins.*'])->where('role', '!=', Auth::guard('admin')->user()->role);

        return Datatables::of($admin)
            ->addColumn('action', function ($admin) {
                return '<a href="users/'.$admin->id.'/edit" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i> Edit </a>
                        <a onclick="deleteData('.$admin->id. ')" class="btn btn-danger" style="color: #fff !important"><i class="fa fa-trash"></i> Delete </a>';
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
