<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Promo;
use App\PromoDetail;
use App\Course;
use App\DetailCourse;
use App\CourseTicketPrices;
use Illuminate\Http\Request;
use App\LearnMethod;
use Carbon\Carbon;
use DB;
use File;

use Yajra\Datatables\Datatables;

class PromoController extends Controller
{
    public function __construct()
    {
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $this->middleware('auth:admin');
    }

    public function index()
    {
        return view('admin.promo.index');
    }

    public function create()
    {
        $LearnMethods = LearnMethod::select('title', 'id')->get();
        return view('admin.promo.create', compact('LearnMethods'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'code' => 'required|unique:promos,code|max:20',
            'promo_type' => 'required',
            'value_type' => 'required',
            'quota' => 'required|numeric',
            'max_user_promo' => 'required|numeric',
            'date' => 'required',
        ]);

        // discount
        if($request->value_type == 'DISCOUNT')
        {
            // required value type
            $request->validate([
                'value_type' => 'required'
            ]);

            if($request->value_type == 'AMOUNT')
            {
                $request->validate([
                    'value' => 'required|numeric'
                ]);
            }else
            {
                $request->validate([
                    'value' => 'required|numeric|min:0|max:100'
                ]);
            }
        }
        // free Course
        else
        {
            // nothing
        }

        $time_split                         = explode(" - ", $request->date);
        $start_date_split                   = $time_split[0];
        $expire_date_split                  = $time_split[1];

        $requestData                        = $request->all();
        $requestData['start_date']          = Carbon::parse($start_date_split)->format('Y-m-d H:i:s');
        $requestData['end_date']            = Carbon::parse($expire_date_split)->format('Y-m-d H:i:s');
        $requestData['value_type']          = ($request->promo_type == 'DISCOUNT') ? $request->value_type : null;
        $requestData['value']               = isset($request->value) ? $request->value : 0;
        $requestData['learning_method_id']  = 0;
        $requestData['course_id']           = 0;
        // unset data that is not in the promos table
        unset($requestData['_token']);
        unset($requestData['course_detail_id']);
        unset($requestData['ticket_id']);
        unset($requestData['date']);
        unset($requestData['free_learning_method_id']);
        unset($requestData['free_course_id']);
        unset($requestData['free_course_detail_id']);
        unset($requestData['free_ticket_id']);
        $created = Promo::insertGetId($requestData);

        // save promo detail 
        $dataPromoDetai = array();
        $no=0;
        foreach($request->learning_method_id as $learning_method_id)
        {
            // for all learn method
            if($learning_method_id == 0)
            {
                $dataPromoDetai['promo_id']             = $created;
                $dataPromoDetai['learn_method_id']      = $learning_method_id;
                $dataPromoDetai['course_id']            = $request->course_id[$no];
                $dataPromoDetai['detail_course_id']     = 0;
                $dataPromoDetai['category_tiket_id']    = 0;
                $dataPromoDetai['free']                 = 0;
            }
            // for learn method E-Learning
            else if($learning_method_id == 1)
            {
                $dataPromoDetai['promo_id']             = $created;
                $dataPromoDetai['learn_method_id']      = $learning_method_id;
                $dataPromoDetai['course_id']            = $request->course_id[$no];
                $dataPromoDetai['detail_course_id']     = 0;
                $dataPromoDetai['category_tiket_id']    = 0;
                $dataPromoDetai['free']                 = 0;
            }
            // for learn method Certificate
            else if($learning_method_id == 3)
            {
                $dataPromoDetai['promo_id']             = $created;
                $dataPromoDetai['learn_method_id']      = $learning_method_id;
                $dataPromoDetai['course_id']            = $request->course_id[$no];
                $dataPromoDetai['detail_course_id']     = 0;
                $dataPromoDetai['category_tiket_id']    = 0;
                $dataPromoDetai['free']                 = 0;
            }
            // for learn method Webinar & In-Class
            else if($learning_method_id == 2 || $learning_method_id == 4)
            {
                $dataPromoDetai['promo_id']             = $created;
                $dataPromoDetai['learn_method_id']      = $learning_method_id;
                $dataPromoDetai['course_id']            = $request->course_id[$no];
                $dataPromoDetai['detail_course_id']     = $request->course_detail_id[$no];
                $dataPromoDetai['category_tiket_id']    = $request->ticket_id[$no];
                $dataPromoDetai['free']                 = 0;
            }
            // save promo detail
            PromoDetail::create($dataPromoDetai);
            $no++;
        }

        // free Course
        if($request->promo_type == 'FREE_COURSE')
        {
            // for learn method E-Learning
            if($request->free_learning_method_id == 1)
            {
                $dataPromoDetai['promo_id']             = $created;
                $dataPromoDetai['learn_method_id']      = $request->free_learning_method_id;
                $dataPromoDetai['course_id']            = $request->free_course_id;
                $dataPromoDetai['detail_course_id']     = 0;
                $dataPromoDetai['category_tiket_id']    = 0;
                $dataPromoDetai['free']                 = 1;
            }
            // for learn method Certificate
            else if($request->free_learning_method_id == 3)
            {
                $dataPromoDetai['promo_id']             = $created;
                $dataPromoDetai['learn_method_id']      = $request->free_learning_method_id;
                $dataPromoDetai['course_id']            = $request->free_course_id;
                $dataPromoDetai['detail_course_id']     = 0;
                $dataPromoDetai['category_tiket_id']    = 0;
                $dataPromoDetai['free']                 = 1;
            }
            // for learn method Webinar & In-Class
            else if($request->free_learning_method_id == 2 || $request->free_learning_method_id == 4)
            {
                $dataPromoDetai['promo_id']             = $created;
                $dataPromoDetai['learn_method_id']      = $request->free_learning_method_id;
                $dataPromoDetai['course_id']            = $request->free_course_id;
                $dataPromoDetai['detail_course_id']     = $request->free_course_detail_id;
                $dataPromoDetai['category_tiket_id']    = $request->free_ticket_id;
                $dataPromoDetai['free']                 = 1;
            }
            PromoDetail::create($dataPromoDetai);
        }

        if ($created)
        {
            $notification = array(
                'message' => 'Your data added!',
                'alert-type' => 'success'
            );

            return redirect('admin/promos')->with($notification);
        }
        else
        {
            $notification = array(
                'message' => 'Failed create data!',
                'alert-type' => 'error'
            );

            return back()->with($notification);
        }
    }

    public function show($id)
    {
        $promo = Promo::findPromoById($id);
        $LearnMethods = LearnMethod::select('title', 'id')->get();
        $PromoDetails = PromoDetail::getNonFreeByPromoId($id);
        $PromoDetailFrees = PromoDetail::getFreeByPromoId($id);

        return view('admin.promo.show', compact('promo', 'LearnMethods', 'PromoDetails', 'PromoDetailFrees'));
    }

    public function edit($id)
    {
        $LearnMethods = LearnMethod::select('title', 'id')->get();
        $promo = Promo::findOrFail($id);

        $PromoDetails = PromoDetail::getNonFreeByPromoId($id);
        $PromoDetailFrees = PromoDetail::getFreeByPromoId($id);
        // dd($PromoDetails);
        return view('admin.promo.edit', compact('LearnMethods', 'promo', 'PromoDetails', 'PromoDetailFrees'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'code' => 'required|max:20|unique:promos,code,'.$id,
            'promo_type' => 'required',
            'value_type' => 'required',
            'quota' => 'required|numeric',
            'max_user_promo' => 'required|numeric',
            'date' => 'required',
        ]);

        // discount
        if($request->value_type == 'DISCOUNT')
        {
            // required value type
            $request->validate([
                'value_type' => 'required'
            ]);

            if($request->value_type == 'AMOUNT')
            {
                $request->validate([
                    'value' => 'required|numeric'
                ]);
            }else
            {
                $request->validate([
                    'value' => 'required|numeric|min:0|max:100'
                ]);
            }
        }
        // free Course
        else
        {
            // nothing
        }

        $time_split                         = explode(" - ", $request->date);
        $start_date_split                   = $time_split[0];
        $expire_date_split                  = $time_split[1];


        $requestData = $request->all();
        $updated = Promo::findOrFail($id);

        $requestData                        = $request->all();
        $requestData['start_date']          = Carbon::parse($start_date_split)->format('Y-m-d H:i:s');
        $requestData['end_date']            = Carbon::parse($expire_date_split)->format('Y-m-d H:i:s');
        $requestData['value_type']          = ($request->promo_type == 'DISCOUNT') ? $request->value_type : null;
        $requestData['value']               = isset($request->value) ? $request->value : 0;
        $requestData['learning_method_id']  = 0;
        $requestData['course_id']           = 0;
        // unset data that is not in the promos table
        unset($requestData['course_detail_id']);
        unset($requestData['ticket_id']);

        $updated->update($requestData);

        // save promo detail 
        $dataPromoDetai = array();
        // delete first before update promo detail
        PromoDetail::deleteByPromoId($updated->id);
        foreach($request->learning_method_id as $no => $learning_method_id)
        {
            // for all learn method
            if($learning_method_id == 0)
            {
                $dataPromoDetai['promo_id']             = $updated->id;
                $dataPromoDetai['learn_method_id']      = $learning_method_id;
                $dataPromoDetai['course_id']            = $request->course_id[$no];
                $dataPromoDetai['detail_course_id']     = 0;
                $dataPromoDetai['category_tiket_id']    = 0;
                $dataPromoDetai['free']                 = 0;
            }
            // for learn method E-Learning
            else if($learning_method_id == 1)
            {
                $dataPromoDetai['promo_id']             = $updated->id;
                $dataPromoDetai['learn_method_id']      = $learning_method_id;
                $dataPromoDetai['course_id']            = $request->course_id[$no];
                $dataPromoDetai['detail_course_id']     = 0;
                $dataPromoDetai['category_tiket_id']    = 0;
                $dataPromoDetai['free']                 = 0;
            }
            // for learn method Certificate
            else if($learning_method_id == 3)
            {
                $dataPromoDetai['promo_id']             = $updated->id;
                $dataPromoDetai['learn_method_id']      = $learning_method_id;
                $dataPromoDetai['course_id']            = $request->course_id[$no];
                $dataPromoDetai['detail_course_id']     = 0;
                $dataPromoDetai['category_tiket_id']    = 0;
                $dataPromoDetai['free']                 = 0;
            }
            // for learn method Webinar & In-Class
            else if($learning_method_id == 2 || $learning_method_id == 4)
            {
                $dataPromoDetai['promo_id']             = $updated->id;
                $dataPromoDetai['learn_method_id']      = $learning_method_id;
                $dataPromoDetai['course_id']            = $request->course_id[$no];
                $dataPromoDetai['detail_course_id']     = $request->course_detail_id[$no];
                $dataPromoDetai['category_tiket_id']    = $request->ticket_id[$no];
                $dataPromoDetai['free']                 = 0;
            }
            PromoDetail::create($dataPromoDetai);
        }

        // free Course
        if($request->promo_type == 'FREE_COURSE')
        {
            // for learn method E-Learning
            if($request->free_learning_method_id == 1)
            {
                $dataPromoDetai['promo_id']             = $updated->id;
                $dataPromoDetai['learn_method_id']      = $request->free_learning_method_id;
                $dataPromoDetai['course_id']            = $request->free_course_id;
                $dataPromoDetai['detail_course_id']     = 0;
                $dataPromoDetai['category_tiket_id']    = 0;
                $dataPromoDetai['free']                 = 1;
            }
            // for learn method Certificate
            else if($request->free_learning_method_id == 3)
            {
                $dataPromoDetai['promo_id']             = $updated->id;
                $dataPromoDetai['learn_method_id']      = $request->free_learning_method_id;
                $dataPromoDetai['course_id']            = $request->free_course_id;
                $dataPromoDetai['detail_course_id']     = 0;
                $dataPromoDetai['category_tiket_id']    = 0;
                $dataPromoDetai['free']                 = 1;
            }
            // for learn method Webinar & In-Class
            else if($request->free_learning_method_id == 2 || $request->free_learning_method_id == 4)
            {
                $dataPromoDetai['promo_id']             = $updated->id;
                $dataPromoDetai['learn_method_id']      = $request->free_learning_method_id;
                $dataPromoDetai['course_id']            = $request->free_course_id;
                $dataPromoDetai['detail_course_id']     = $request->free_course_detail_id;
                $dataPromoDetai['category_tiket_id']    = $request->free_ticket_id;
                $dataPromoDetai['free']                 = 1;
            }
            // save promo detail
            PromoDetail::create($dataPromoDetai);
        }

        if ($updated)
        {
            $notification = array(
                'message' => 'Your data updated!',
                'alert-type' => 'success'
            );

            return redirect('admin/promos')->with($notification);
        }
        else
        {
            $notification = array(
                'message' => 'Failed to update data!',
                'alert-type' => 'error'
            );

            return back()->with($notification);
        }
    }

    public function destroy($id)
    {
        // delete promo
        Promo::destroy($id);
        // delete promo detailsby promo_id
        PromoDetail::deleteByPromoId($id);

        $notification = array(
            'message' => 'Success delete',
            'alert-type' => 'success'
        );

        return redirect('admin/about-us')->with($notification);
    }

    public function anyData()
    {
        DB::statement(DB::raw('set @rownum=0'));
        $promo = Promo::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'promos.*'
        ]);

        return Datatables::of($promo)
            ->editColumn('learning_method', function ($promo)
            {
                $learn = "";
                DB::statement(DB::raw('set @rownum=0'));
                $LearnMethods = LearnMethod::select('title', 'id')->get();
                $PromoDetails = PromoDetail::getNonFreeByPromoId($promo->id);

                foreach($PromoDetails as $PromoDetail)
                {
                    if($PromoDetail->learn_method_id == 0)
                    {
                        return "All Learn Method";
                    }
                    else
                    {
                        $learn .= LearnMethod::select("title")->where("id", $PromoDetail->learn_method_id)->first()->title ."<br>";
                    }
                }
                return $learn;
            })
            ->editColumn('value', function ($promo)
            {
                if($promo->promo_type == 'DISCOUNT')
                {
                    if($promo->value_type == 'AMOUNT')
                    {
                        return number_format($promo->value,2);
                    }else
                    {
                        return $promo->value.' %';
                    }
                }
                else
                {
                    DB::statement(DB::raw('set @rownum=0'));
                    $LearnMethods = LearnMethod::select('title', 'id')->get();
                    $PromoDetailFree = PromoDetail::getFreeByPromoId($promo->id);
                    foreach($PromoDetailFree as $PromoDetail)
                    {
                        return "FREE COURSES ".LearnMethod::select("title")->where("id", $PromoDetail->learn_method_id)->first()->title;
                    }
                }
            })
            ->editColumn('start_date', function ($promo)
            {
                return Carbon::parse($promo->start_date)->format('d-F-Y H:i:s');
            })
            ->editColumn('end_date', function ($promo)
            {
                return Carbon::parse($promo->end_date)->format('d-F-Y H:i:s');
            })
            ->editColumn('status', function ($promo)
            {
                if($promo->status == '1')
                {
                    return '<span style="color: green">Active</span>';
                }else
                {
                    return '<span style="color: red">Inactive</span>';
                }
            })
            ->addColumn('action', function ($promo) {
                return '
                    <a href="promos/'.$promo->id . '" class="btn btn-info"><i class="fa fa-eye"></i> Detail </a>
                    <a href="promos/' . $promo->id . '/edit" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i> Edit </a>
                    <a href="javascript:void(0)" onclick="deleteData('.$promo->id.')" class="btn btn-danger"><i class="fa fa-trash"></i> Delete </a>';
            })
            ->rawColumns(['learning_method', 'value', 'start_date', 'end_date', 'status', 'action'])
            ->make(true);
    }

    public function cekCode(Request $request)
    {
        $code = $request->code;
        if(is_null($request->id))
        {
            $check = Promo::whereCode($code)->first();
            if($check)
            {
                return response(['status' => 400],200);
            }
            else
            {
                return response(['status' => 200],200);
            }
        }
        else
        {
            $check = Promo::whereCode($code)->first();
            if($check)
            {
                $check2 = Promo::where('id', $request->id)->where('code', $code)->first();
                if($check2)
                {
                    return response(['status' => 200],200);
                }
                return response(['status' => 400],200);
            }
            else
            {
                return response(['status' => 200],200);
            }
        }
    }

    public function getCourse(Request $request)
    {
        $learn_method = $request->learn_method;
        $data = Course::where('learn_method', $learn_method)->get();
        return response(['data' => $data],200);
    }

    public function getDetailCourse(Request $request)
    {
        $course_id = $request->course_id;
        $data = DetailCourse::DetailCourseByCourseId($course_id);
        return response(['data' => $data],200);
    }

    public function getTicket(Request $request)
    {
        $detail_course_id = $request->detail_course_id;
        $data = CourseTicketPrices::courseTiketPriceByDetailId($detail_course_id);
        return response(['data' => $data],200);
    }
}
