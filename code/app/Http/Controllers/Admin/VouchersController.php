<?php

namespace App\Http\Controllers\Admin;

use App\Voucher;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Config\EmailFunction;
use Illuminate\Http\Request;
use App\Exports\CsvExport;
use Maatwebsite\Excel\Facades\Excel;
use DB;

use Yajra\Datatables\Datatables;

class VouchersController extends Controller
{
    public function __construct()
    {
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $this->middleware('auth:admin');
    }

    public function index()
    {
        return view('admin.voucher.index');
    }

    public function create()
    {
        return view('admin.voucher.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'code_voucher' => 'required|unique:conf_vouchers,code_voucher|max:10',
            'email' => 'required',
            'quota' => 'required'
        ]);

        $requestData = $request->all();
        $requestData['remaining_quota'] = $request->quota;

        $voucher = Voucher::create($requestData);

        if ($voucher)
        {
            // send voucher
            app(EmailFunction::class)->send_vouchers($voucher);

            $notification = array(
                'message' => 'Success Send Voucher to '.$voucher->email,
                'alert-type' => 'success'
            );

            return redirect('admin/vouchers')->with($notification);
        }
        else
        {
            $notification = array(
                'message' => 'Failed create data!',
                'alert-type' => 'error'
            );

            return back()->with($notification);
        }
    }

    public function show($id)
    {
        $Voucher = Voucher::findOrFail($id);

        return view('admin.voucher.show', compact('Voucher'));
    }

    public function edit($id)
    {
        $voucher = Voucher::findOrFail($id);

        return view('admin.voucher.edit', compact('voucher'));
    }

    public function update($id, Request $request)
    {
        $updated = Voucher::findOrFail($id);

        $request->validate([
            'address' => 'required',
            'url' => 'required',
            'phone' => 'required'
        ]);

        $requestData = $request->all();

        $updated->update($requestData);

        if ($updated)
        {
            $notification = array(
                'message' => 'Your data updated!',
                'alert-type' => 'success'
            );

            return redirect('admin/vouchers')->with($notification);
        }
        else
        {
            $notification = array(
                'message' => 'Failed to update data!',
                'alert-type' => 'error'
            );

            return back()->with($notification);
        }
    }

    public function destroy($id)
    {
        Voucher::destroy($id);

        $notification = array(
            'message' => 'Success delete',
            'alert-type' => 'success'
        );

        return redirect('admin/vouchers')->with($notification);
    }

    public function anyData()
    {
        DB::statement(DB::raw('set @rownum=0'));
        $voucher = Voucher::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'conf_vouchers.*'
        ]);

        return Datatables::of($voucher)
            ->make(true);
    }

    public function randomCode(Request $request)
    {
        //check booking code exsist
        do {
            $random = $this->randomString(5);
            $result = Voucher::where('code_voucher', $random)->count();
        } while ($result > 0);

        return response(['code' => $random],200);
    }

    public function cekCode(Request $request)
    {
        $code = $request->code;
        $check = Voucher::where('code_voucher', $code)->first();
        if(!is_null($check))
        {
            return response(['status' => 400],200);
        }
        else
        {
            return response(['status' => 200],200);
        }
    }

    function csv_export()
    {
		return Excel::download(new CsvExport, 'report-voucher.xlsx');
    }

    function randomString($length)
    {
        $str = "";
        $characters = range('0', '9');
        $max = count($characters) - 1;
        $str .= 'C';
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }
}
