<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Orders;
use App\User;
use App\LearnMethod;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use DB;

class TransactionController extends Controller
{
    public function __construct()
    {
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.dashboard');
    }

    public function transaction()
    {
        $learnmethods = LearnMethod::all();
        return view('admin.transaction.index', compact('learnmethods'));
    }

    public function transaction_detail($id){
       $order = Orders::select('orders.*', 'courses.title', 'users.first_name', 'users.last_name', 'courses.category_matrix', 'courses.title', 'detail_courses.city', 'detail_courses.area', 'detail_courses.start_date as course_start_date', 'detail_courses.finish_date as course_finish_date')
                ->leftJoin('users', 'users.id', '=', 'orders.user')
                ->leftJoin('courses', 'courses.id', '=', 'orders.course')
                ->leftJoin('detail_courses', 'detail_courses.course', '=', 'courses.id')
                ->where('orders.id', $id)
                ->first();
        if(!$order)
        {
            $learnmethods = LearnMethod::all();
            return view('admin.transaction.index', compact('learnmethods'));
        }

        if($order->course == null && $order->detail_course == null)
        {
            $learnmethod = LearnMethod::findOrFail(1)->title;
        }elseif($order->course != null && $order->detail_course != null)
        {
            $learnmethod = LearnMethod::findOrFail(2)->title;
        }else
        {
            $learnmethod = LearnMethod::findOrFail(3)->title;
        }
        $order_logs = DB::table('order_logs')->where('invoice', $order->invoice)->get();
        return view('admin.transaction.show', compact('order', 'order_logs', 'learnmethod'));
    }

    public function transaction_data(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        if(isset($request->method))
        {
            if($request->method == 1)
            {
                $transaction = DB::table('orders')
                        ->select([DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'orders.*', 'users.first_name', 'users.last_name', 'courses.learn_method_matrix'])
                        ->leftJoin('users', 'users.id', '=', 'orders.user')
                        ->leftJoin('courses', 'courses.id', '=', 'orders.course')
                        ->where('orders.course', null)
                        ->where('orders.detail_course', null)
                        ->where('orders.status_code', 200)
                        ->get();
            }
            else if($request->method == 2)
            {
                $transaction = DB::table('orders')
                        ->select([DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'orders.*', 'users.first_name', 'users.last_name', 'courses.learn_method_matrix'])
                        ->leftJoin('users', 'users.id', '=', 'orders.user')
                        ->leftJoin('courses', 'courses.id', '=', 'orders.course')
                        ->where('courses.learn_method', '2')
                        ->where('orders.status_code', 200)
                        ->get();
            }else if($request->method == 3)
            {
                $transaction = DB::table('orders')
                        ->select([DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'orders.*', 'users.first_name', 'users.last_name', 'courses.learn_method_matrix'])
                        ->leftJoin('users', 'users.id', '=', 'orders.user')
                        ->leftJoin('courses', 'courses.id', '=', 'orders.course')
                        ->where('courses.learn_method', '3')
                        ->where('orders.status_code', 200)
                        ->get();
            }else{
                // if transaction all
                $transaction = DB::table('orders')
                        ->select([DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'orders.*', 'users.first_name', 'users.last_name', 'courses.learn_method_matrix'])
                        ->leftJoin('users', 'users.id', '=', 'orders.user')
                        ->leftJoin('courses', 'courses.id', '=', 'orders.course')
                        ->where('orders.status_code', 200)
                        ->get();

                return Datatables::of($transaction)
                ->editcolumn('first_name', function ($transaction)
                {
                    return $transaction->first_name.' '.$transaction->last_name;
                })
                ->editcolumn('learn_method', function ($transaction)
                {
                    if($transaction->course == null && $transaction->detail_course == null)
                    {
                        return 'E-LEARNING';
                    }else{
                        return $transaction->learn_method_matrix;
                    }
                })
                ->editcolumn('price', function ($transaction)
                {
                    if($transaction->price != null){
                        return number_format($transaction->price,2,",",".");
                    }else{
                        return '0';
                    }
                })
                ->editcolumn('status', function ($transaction)
                {
                    if($transaction->status_code == 200)
                    {
                        return '<p style="color: green">'.$transaction->status_code.' - '.$transaction->status.'</p>';
                    }else if($transaction->status_code != null)
                    {
                        return '<p style="color: red">'.$transaction->status_code.' - '.$transaction->status.'</p>';
                    }else{
                        return '-';
                    }
                })
                ->editcolumn('is_admin', function ($transaction)
                {
                    if($transaction->created_by != null)
                    {
                        return 'By Admin';
                    }else
                    {
                        return 'By Customer';
                    }
                })
                ->addColumn('action', function ($transaction)
                {
                    return '<a href="transaction/'. $transaction->id.'" class="btn btn-info"><i class="fa fa-eye"></i> Detail </a>';
                })
                ->rawColumns(['first_name', 'learn_method', 'status', 'price', 'is_admin', 'action'])
                ->make(true);
            }
            // if transaction click buton category
            return Datatables::of($transaction)
            ->editcolumn('first_name', function ($transaction)
            {
                return $transaction->first_name.' '.$transaction->last_name;
            })
            ->editcolumn('learn_method', function ($transaction)
            {
                if($transaction->course == null && $transaction->detail_course == null)
                {
                    return 'E-LEARNING';
                }else{
                    return $transaction->learn_method_matrix;
                }
            })
            ->editcolumn('price', function ($transaction)
            {
                if($transaction->price != null){
                    return number_format($transaction->price,2,",",".");
                }else{
                    return '0';
                }
            })
            ->editcolumn('status', function ($transaction)
            {
                if($transaction->status_code == 200)
                {
                    return '<p style="color: green">'.$transaction->status_code.' - '.$transaction->status.'</p>';
                }else if($transaction->status_code != null)
                {
                    return '<p style="color: red">'.$transaction->status_code.' - '.$transaction->status.'</p>';
                }else{
                    return '-';
                }
            })
            ->editcolumn('is_admin', function ($transaction)
            {
                if($transaction->created_by != null)
                {
                    return 'By Admin';
                }else
                {
                    return 'By Customer';
                }
            })
            ->addColumn('action', function ($transaction)
            {
                return '<a href="'. $transaction->id.'" class="btn btn-info"><i class="fa fa-eye"></i> Detail </a>';
            })
            ->rawColumns(['first_name', 'learn_method', 'status', 'price', 'is_admin', 'action'])
            ->make(true);
            
        }else
        {
            $transaction = DB::table('orders')
                    ->select([DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'orders.*', 'users.first_name', 'users.last_name', 'courses.learn_method_matrix'])
                    ->leftJoin('users', 'users.id', '=', 'orders.user')
                    ->leftJoin('courses', 'courses.id', '=', 'orders.course')
                    ->where('orders.status_code', 200)
                    ->get();

                    return Datatables::of($transaction)
                    ->editcolumn('first_name', function ($transaction)
                    {
                        return $transaction->first_name.' '.$transaction->last_name;
                    })
                    ->editcolumn('learn_method', function ($transaction)
                    {
                        if($transaction->course == null && $transaction->detail_course == null)
                        {
                            return 'E-LEARNING';
                        }else{
                            return $transaction->learn_method_matrix;
                        }
                    })
                    ->editcolumn('price', function ($transaction)
                    {
                        if($transaction->price != null){
                            return number_format($transaction->price,2,",",".");
                        }else{
                            return '0';
                        }
                    })
                    ->editcolumn('status', function ($transaction)
                    {
                        if($transaction->status_code == 200)
                        {
                            return '<p style="color: green">'.$transaction->status_code.' - '.$transaction->status.'</p>';
                        }else if($transaction->status_code != null)
                        {
                            return '<p style="color: red">'.$transaction->status_code.' - '.$transaction->status.'</p>';
                        }else{
                            return '-';
                        }
                    })
                    ->editcolumn('is_admin', function ($transaction)
                    {
                        if($transaction->created_by != null)
                        {
                            return 'By Admin';
                        }else
                        {
                            return 'By Customer';
                        }
                    })
                    ->addColumn('action', function ($transaction)
                    {
                        return '<a href="admin/transaction/'. $transaction->id.'" class="btn btn-info"><i class="fa fa-eye"></i> Detail </a>';
                    })
                    ->rawColumns(['first_name', 'learn_method', 'status', 'price', 'is_admin', 'action'])
                    ->make(true);
        }
        
    }
}
