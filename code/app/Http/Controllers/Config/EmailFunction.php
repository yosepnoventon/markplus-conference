<?php

namespace App\Http\Controllers\Config;

use App\Http\Controllers\Controller;
use Mail;

class EmailFunction extends Controller
{
    public function send_vouchers($voucher)
    {
        $send = Mail::send('admin/email/vouchers', ['voucher' => $voucher], function($message) use ($voucher) {
            $message->to($voucher->email)->subject('MarkPlus Conference - Vouchers');
            $message->from(config('mail.from.address'),config('app.name'));
        });

        return $send;
    }
}
