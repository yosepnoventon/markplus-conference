<?php

namespace App\Http\Controllers\Config;

use App\Log;
use Veritrans_Config;
use Veritrans_Snap;

use Midtrans\Config as MidtransConfig;
use Midtrans\Snap as MidtransSnap;

/**
 * class MidtransPaymentApi
 * 
 * @author Hendri <hendri.gnw@gmail.com>
 */
class MidtransPaymentApi extends BaseApi
{
    const PRODUCTION_SERVER_KEY = 'Mid-server-DYQPFEV4yMz18tBph6bqx0J7';
    const PRODUCTION_CLIENT_KEY = 'Mid-client-G9TNXX25CwQz-jvc';
    const PRODUCTION_MERCHANT_ID = 'G618467809';
    const PRODUCTION_URL_SNAP_JS = 'https://app.midtrans.com/snap/snap.js';
    
    const SANDBOX_SERVER_KEY = 'SB-Mid-server-QfIyJe2nji9ow8CvGG5zY8cT';
    const SANDBOX_CLIENT_KEY = 'SB-Mid-client-AqSIHG7uoUgdUu36';
    const SANDBOX_MERCHANT_ID = 'G618467809';
    const SANDBOX_URL_SNAP_JS = 'https://app.sandbox.midtrans.com/snap/snap.js';
    
    const PAYMENT_TYPE_MANDIRI_ECASH = 'mandiri_ecash';
    const PAYMENT_TYPE_CREDIT_CARD = 'credit_card';
    const PAYMENT_TYPE_INDOSAT_DOMPETKU = 'indosat_dompetku';
    const PAYMENT_TYPE_TELKOMSEL_CASH = 'telkomsel_cash';
    const PAYMENT_TYPE_XL_TUNAI = 'telkomsel_cash';
    
    const TRANSACTION_STATUS_AUTHORIZE = 'authorize';
    const TRANSACTION_STATUS_CAPTURE = 'capture';
    const TRANSACTION_STATUS_SETTLEMENT = 'settlement';
    const TRANSACTION_STATUS_DENY = 'deny';
    const TRANSACTION_STATUS_PENDING = 'pending';
    const TRANSACTION_STATUS_CANCEL = 'cancel';
    const TRANSACTION_STATUS_REFUND = 'refund';
    const TRANSACTION_STATUS_PARTIAL_REFUND = 'partial_refund';
    const TRANSACTION_STATUS_CHARGEBACK = 'chargeback';
    const TRANSACTION_STATUS_PARTIAL_CHARGEBACK = 'partial_chargeback';
    const TRANSACTION_STATUS_EXPIRE = 'expire';
    const TRANSACTION_STATUS_FAILURE = 'failure';
    
    
    protected $_transactionBody;
    
    public function __construct() 
    {
        parent::__construct();
        
        // Set your Merchant Server Key
        MidtransConfig::$serverKey = $this->getServerKey();
        // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
        MidtransConfig::$isProduction = $this->isProductionMode();
        // Set sanitization on (default)
        MidtransConfig::$isSanitized = true;
        // Set 3DS transaction for credit card to true
        MidtransConfig::$is3ds = true;
    }
    
    public function getServerKey()
    {
        if ($this->testMode) {
            return self::SANDBOX_SERVER_KEY;
        }
        
        return self::PRODUCTION_SERVER_KEY;
    }
    
    public function getClientKey()
    {
        if ($this->testMode) {
            return self::SANDBOX_CLIENT_KEY;
        }
        return self::PRODUCTION_CLIENT_KEY;
    }
    
    public function getUrlSnapJs()
    {
        if ($this->testMode) {
            return self::SANDBOX_URL_SNAP_JS;
        }
        return self::PRODUCTION_URL_SNAP_JS;
    }
    
    public function getMerchantId()
    {
        if ($this->testMode) {
            return self::SANDBOX_MERCHANT_ID;
        }
        return self::PRODUCTION_MERCHANT_ID;
    }
    
    public function setTransactionBody($params = [])
    {
        $this->_transactionBody = $params;
    }
    
    public function getTransactionBody()
    {
        return $this->_transactionBody;
    }
    
    /**
     * get snap token from transaction body
     * 
     * @return string
     */
    public function getSnapToken()
    {
        $snapToken = MidtransSnap::getSnapToken($this->getTransactionBody());
        
        // Log::create([
        //     'category' => Log::CATEGORY_MIDTRANS,
        //     'name' => 'Payment Checkout get Snap Token',
        //     'model' => 'MidtransPaymentApi',
        //     'model_id' => null,
        //     'request' => json_encode($this->getTransactionBody()),
        //     'response' => json_encode($snapToken),
        // ]);
        
        return $snapToken;
    }
    
    /**
     * @return type
     */
    public static function getArraysCreditCardStatusPaid()
    {
        return [
            self::TRANSACTION_STATUS_AUTHORIZE,
            self::TRANSACTION_STATUS_CAPTURE,
        ];
    }
    
    /**
     * @return type
     */
    public static function getArraysMandiryLinePayStatusPaid()
    {
        return [
            self::TRANSACTION_STATUS_SETTLEMENT,
        ];
    }
}