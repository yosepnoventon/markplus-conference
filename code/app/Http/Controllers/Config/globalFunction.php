<?php

namespace App\Http\Controllers\Config;

use App\Http\Controllers\Controller;
use App\Libraries\ConnectLMS;
class globalFunction extends Controller
{
    public function __construct()
    {
        $this->host = 'markplus.matrixlms.com';

        $this->apiKey = '1af170882f9ea7e6df1ed4917570f3e173fb1730de36aa57a43a';
    }

    public function api_matrix()
    {
        $connect = new ConnectLMS(array(
            'host' => $this->host,
            'api_key' => $this->apiKey,
            'api_version' => '1',
            'use_ssl' => true,
        ));

        return $connect;
    }

    public function encrypt_decrypt($action, $string)
    {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c';
        $secret_iv = 'SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c';
        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if ($action == 'encrypt')
        {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        }
        elseif ($action == 'decrypt')
        {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }
}
