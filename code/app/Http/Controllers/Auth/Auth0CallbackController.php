<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth0\SDK\Exception\CoreException;
use Auth0\Login\Auth0Controller;

class Auth0CallbackController extends Auth0Controller
{
    /**
     * Wrap auth0 callback controller and handle exceptions if required
     */
    public function callback()
    {
        try {
            // try calling auth0 parent method
            return parent::callback();

        } catch (CoreException $exception) {
            $message = $exception->getMessage();

            // if active session exists, then user is already logged-in - no need to throw an error
            if ($message == "Can't initialize a new session while there is one active session already")
                $notification = array(
                    'message' => "Can not initialize a new session while there is one active session already",
                    'alert-type' => 'warning'
                );
                return redirect('/')->with($notification);

            // invalid state is often a temporary cookie or a timing problem, and is often resolved by simply logging out
            if ($message == "Invalid state")
                return redirect('user/logout');
            }
    }
}
