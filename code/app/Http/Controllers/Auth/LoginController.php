<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Hesto\MultiAuth\Traits\LogsoutGuard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\User;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    
    use AuthenticatesUsers, LogsoutGuard {
        LogsoutGuard::logout insteadof AuthenticatesUsers;
    }
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('test')->except(['logout', 'test']);
    }

    public function index(Request $request)
    {
        return view('user.auth.login');
    }
    
    public function login(Request $request)
    {
        $this->validateLogin($request);

        if(Auth::guard('user')->attempt(['email' => $request['email'], 'password' => $request['password'], 'verified' => User::VERIFIED]))
        {
            //Login on Authenticate
            ;
            $notification = array(
                'message' => 'Login Success',
                'alert-type' => 'success',
                'code' => 200
            );
            $previous_url = Session::get('url.intended');

            if(strpos($previous_url, "verification") !== false)
            {
                return redirect('/')->with($notification);
            }
            if(strpos($previous_url, "login") !== false)
            {
                return redirect('/')->with($notification);
            }
            else
            {
                return Redirect::to(Session::get('url.intended'))->with($notification);
            }
        }
        else
        {
            $notification = array(
                'message' => 'Error some parameter',
                'alert-type' => 'error'
            );
            
            return back()->with($notification)->withInput($request->only('email', 'remember'));
        }
    }
    public function logout(Request $request)
    {
        $this->guard('user')->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect('/');
    }

    public function returnPassword($email)
    {
        $user = User::where('email', $email)->first();

        return $user->password;
    }

    protected function guard()
    {
        return Auth::guard('user');
    }
}
