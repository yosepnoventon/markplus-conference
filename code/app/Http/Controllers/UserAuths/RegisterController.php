<?php

namespace App\Http\Controllers\UserAuths;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Config\CodemiFunction;
use App\Http\Controllers\Config\EmailFunction;
use Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('user.guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email',
                'phone' => 'required',
                'password' => 'required|min:6|confirmed',
                'captcha' => 'required|captcha'
            ],
            ['captcha.captcha' => 'invalid captcha']
        );
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'first_name'     => $data['first_name'],
            'last_name'      => $data['last_name'],
            'email'          => $data['email'],
            'phone'          => $data['phone'],
            'password_plain' => $data['password'],
            'password'       => bcrypt($data['password']),
            'token'          => $this->generateRandomString(),
            'verified'       => User::NOT_VERIFIED
        ]);
    }

    protected function validator_verfication(array $data)
    {
        return Validator::make($data, [
                'email' => 'required',
                'captcha' => 'required|captcha'
            ],
            ['captcha.captcha' => 'invalid captcha']
        );
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('user.auth.register');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showVerificationForm()
    {
        return view('user.auth.verification');
    }

    public function checkEmail(Request $request)
    {
        $email = $request->email;
        $user = User::where('email', $email)->first();
        if(is_null($user))
        {
            $notification = array(
                'message' => 'Email does\'t Exist!',
                'status' => '400',
                'alert_type' => 'error'
            );
        }elseif($user->verified == 1)
        {
            $notification = array(
                'message' => 'Email Already Verified!',
                'status' => '400',
                'alert_type' => 'success'
            );
        }
        else
        {
            $notification = array(
                'message' => 'Email has not been Verified!',
                'status' => '200',
                'alert_type' => 'success'
            );
        }
        return $notification;
    }

    public function sendVerification(Request $request)
    {
        $this->validator_verfication($request->all())->validate();

        $email = $request->email;
        $user = User::where('email', $email)->first();
        if(is_null($user))
        {
            $notification = array(
                'message' => 'Email does not Exist!',
                'alert-type' => 'warning'
            );

            return redirect('/user/register/verification')->with($notification);
        }
        elseif($user->verified == 1)
        {
            $notification = array(
                'message' => 'Email Already Verified!',
                'alert-type' => 'warning'
            );

            return redirect('/user/register/verification')->with($notification);
        }
        else
        {
            app(EmailFunction::class)->send_verification($user, $request);
            $notification = array(
                'message' => 'Please check email for verification',
                'alert-type' => 'success'
            );
    
            return redirect('/')->with($notification);
        }
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        if(User::where('email', $request->email)->first())
        {
            $notification = array(
                'message' => 'Email exist!',
                'alert-type' => 'error'
            );

            return back()->with($notification)->withInput($request->only('email', 'phone', 'first_name', 'last_name'));
        }
        else
        {
            $cekEmail = app(CodemiFunction::class)->get_user($request->email);
            if($cekEmail['success'] == false || $cekEmail == null)
            {
                event(new Registered($user = $this->create($request->all())));

                // send verification
                app(EmailFunction::class)->send_verification($user);

                $notification = array(
                    'message' => 'Please check email for verification',
                    'alert-type' => 'success'
                );

                return redirect('/')->with($notification);
            }else{
                $request['userid']         = $cekEmail['data']['id'];
                $request['user_id_matrix'] = $cekEmail['data']['username'];

                event(new Registered($user = $this->create($request->all())));

                // send verification
                app(EmailFunction::class)->send_verification($user);

                $notification = array(
                    'message' => 'Please check email for verification',
                    'alert-type' => 'success'
                );

                return redirect('/')->with($notification);
            }
        }
    }

    public function verification($token)
    {
        $user = User::where('token', $token)->first();

        if($user)
        {
            $cekEmail = app(CodemiFunction::class)->get_user($user->email);
            if($cekEmail['success'] == false || $cekEmail == null)
            {
                $array = [
                    'name'  => $user->first_name.' '.$user->last_name,
                    'email' => $user->email,
                    'password' => $user->password_plain
                ];
                
                $add_user = app(CodemiFunction::class)->register($array);
                $user['verified']       = User::VERIFIED;
                $user['userid']         = $add_user['data']['id'];
                $user['user_id_matrix'] = $add_user['data']['username'];
                $user->update();
            }else
            {
                $user['verified']       = User::VERIFIED;
                $user['userid']         = $cekEmail['data']['id'];
                $user['user_id_matrix'] = $cekEmail['data']['username'];
                $user->update();
            }

            if($user)
            {

                $notification = array(
                    'message'    => 'Verification Success',
                    'alert-type' => 'success'
                );

                return redirect('/')->with($notification);
            }
            else
            {
                $notification = array(
                    'message'    => 'Error some paramaters',
                    'alert-type' => 'error'
                );

                return back()->with($notification);
            }
        }
        else
        {
            $notification = array(
                'message' => 'Error some paramaters',
                'alert-type' => 'error'
            );

            return redirect('/')->with($notification);
        }
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */

    protected function guard()
    {
        return Auth::guard('user');
    }

    function generateRandomString($length = 100)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++)
        {
            $randomString .= $characters[rand(0, $charactersLength - 1)];

            if(User::where('token', $randomString)->count() > 0 )
            {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
        }
        return $randomString;
    }
}
