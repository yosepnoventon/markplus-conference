<?php

namespace App\Http\Controllers\UserAuths;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Auth\Passwords\PasswordBroker;
use App\Notifications\UserResetPassword;
use DB;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('user.guest');
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm()
    {
        return view('user.auth.passwords.email');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $request->validate([
                'email' => 'required|email',
                'captcha' => 'required|captcha'
            ],
            ['captcha.captcha' => 'invalid captcha']
        );

        $check_email = User::where('email', $request->email)->first();

        if($check_email)
        {
            // create
            $token = str_random(60);
            DB::table('user_password_resets')->insert([
                'email' => $request->email, 
                'token' => $token,
                'created_at' => date('Y-m-d H:i:s')
            ]);
            // $response = $this->broker()->sendResetLink($request->only('email'));
            $user = User::where('email', $request->email)->first();
            app('App\Http\Controllers\Config\EmailFunction')->forgot_password($token, $user);

            // if($response === Password::RESET_LINK_SENT)
            // {
                $notification = array(
                    'message' => 'Please check your email to verify',
                    'alert-type' => 'success',
                    // 'status' => trans($response)
                );

                return back()->with($notification);
            // }
            // else
            // {
            //     $notification = array(
            //         'message' => 'Failed to send your email',
            //         'alert-type' => 'error'
            //     );

            //     return back()->with($notification);
            // }
        }
        else
        {
            $notification = array(
                'message' => 'Your email not exist!',
                'alert-type' => 'error'
            );

            return back()->with($notification)->withInput($request->only('email'));
        }
    }

    public function broker()
    {
        return Password::broker('users');
    }
}
