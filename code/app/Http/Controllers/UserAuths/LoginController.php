<?php

namespace App\Http\Controllers\UserAuths;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Config\globalFunction;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Hesto\MultiAuth\Traits\LogsoutGuard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Web\CartController;
use Cookie;
use App\User;
use App\Voucher;
use App\Login;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Exception\RequestException;
use Cart;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    
    use AuthenticatesUsers, LogsoutGuard {
        LogsoutGuard::logout insteadof AuthenticatesUsers;
    }
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/success';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('login.guest')->except(['logout']);
    }

    public function index(Request $request)
    {
        
        $logged = Auth::guard('login')->user();
        return view('web.homepage');
    }

    public function login(Request $request, Login $logins)
    {
        $email = Login::where('email', $request->email)->first();
        $phone = Login::where('phone', $request->phone)->first();
        // validation for user found
        if(!is_null($email) || !is_null($phone))
        {
            $request->validate([
                'email' => 'required|email',
                'phone' => 'required|min:10|numeric',
                'code_voucher' => 'required',
            ]);
        }
        // validation for user not exsist
        else
        {
            $request->validate([
                'name' => 'required',
                'email' => 'required|email',
                'phone' => 'required|min:10|numeric',
                'code_voucher' => 'required',
                'agency_name' => 'required',
            ], ['agency_name.required'  => 'The Instansi field is required.']);
        }
        
        
        // cek voucher
        $voucher = Voucher::where('code_voucher', $request->code_voucher)->first();
        if(is_null($voucher))
        {
            $notification = array(
                'message' => 'Voucher Not Found',
                'alert-type' => 'error'
            );

            return back()->with($notification)->withInput($request->only('name', 'email', 'phone', 'code_voucher', 'agency_name'));
        }

        $login = Login::where('email', $request->email)->where('phone', $request->phone)->where('code_voucher', $request->code_voucher)->first();
        // if user not found and create user
        if(is_null($login))
        {
            // if email found
            if(!is_null($email))
            {
                $notification = array(
                    // 'message' => 'vouchers cannot be used because the email already exists',
                    // 'message' => 'Email already exists',
                    'message' => 'fill in according to Email, Phone Number, Voucher that has been logged in before',
                    'alert-type' => 'error'
                );

                return back()->with($notification)->withInput($request->only('name', 'email', 'phone', 'code_voucher', 'agency_name'));
            }

            // if phone found
            if(!is_null($phone))
            {
                $notification = array(
                    // 'message' => 'vouchers cannot be used because the email already exists',
                    // 'message' => 'Phone Number already exists',
                    'message' => 'fill in according to Email, Phone Number, Voucher that has been logged in before',
                    'alert-type' => 'error'
                );

                return back()->with($notification)->withInput($request->only('name', 'email', 'phone', 'code_voucher', 'agency_name'));
            }

            // if quota maximum
            if($voucher->remaining_quota <= 0)
            {
                $notification = array(
                    'message' => 'Quota has been reached',
                    'alert-type' => 'error'
                );
                return back()->with($notification)->withInput($request->only('name', 'email', 'phone', 'code_voucher', 'agency_name'));
            }
            $data = array();
            $data['name']           = $request->name;
            $data['email']          = $request->email;
            $data['phone']          = $request->phone;
            $data['code_voucher']   = $request->code_voucher;
            $data['agency_name']    = $request->agency_name;
            $create = $logins->create($data);

            // update voucher remining quota
            $count_voucher = Login::where('code_voucher', $request->code_voucher)->count();
            $voucher->remaining_quota = $voucher->quota - $count_voucher;
            $voucher->save();
             // login create session
            Auth::guard('login')->loginUsingId($create->id);
            $logged = Auth::guard('login')->user();

            $notification = array(
                'message' => 'Login Success',
                'alert-type' => 'success'
            );
            return redirect('/success')->with($notification);
        }
        // login create session
        else if(Auth::guard('login')->loginUsingId($login->id))
        {
            $logged = Auth::guard('login')->user();

            $notification = array(
                'message' => 'Login Success',
                'alert-type' => 'success'
            );
            return redirect('/success')->with($notification);
        }
        else
        {
            $notification = array(
                'message' => 'Error some parameter',
                'alert-type' => 'error'
            );
            return back()->with($notification)->withInput($request->only('email', 'remember'));
        }
    }

    public function logout(Request $request)
    {
        $this->guard('login')->logout();

        return redirect('/');
    }

    protected function guard()
    {
        return Auth::guard('login');
    }
}