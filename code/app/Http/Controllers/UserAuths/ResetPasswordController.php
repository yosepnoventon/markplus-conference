<?php

namespace App\Http\Controllers\UserAuths;

use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use DB;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    public $redirectTo = '/';


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Http\Response
     */
    public function showResetForm(Request $request, $token = null)
    {
        $reset = DB::table('user_password_resets')->where('token', $token)->first();
        if(!is_null($reset))
        {
            if(Carbon::parse($reset->created_at)->addDays(1)->format('Y-m-d H:i:s') >= Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s'))
            {
                return view('user.auth.passwords.reset', compact('reset', 'token'));
            }
            else
            {            
                $notification = array(
                    'message' => 'reset password is expire',
                    'alert-type' => 'success'
                );
                redirect('/')->with($notification);
            }
        }
        else
        {
            redirect('/');
        }
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function reset(Request $request)
    {
        $request->validate([
            'token' => 'required',
            'password' => 'required|min:6|confirmed'
        ]);

        $reset = DB::table('user_password_resets')->where('token', $request->token)->first();
        if(is_null($reset))
        {
            $notification = array(
                'message' => 'token invalid!',
                'alert-type' => 'error'
            );
            return back()->with($notification)->withInput($request->only('token'));
        }

        $user = User::where('email', $reset->email)->first();

        if($user)
        {
            // store to lms change password
            $array = [
                'userid' => $user->userid,
                'password' => $request->password,
                'update_password' => "true"
            ];
            $update_user = app('App\Http\Controllers\Matrix\MatrixController')->update_user($array);

            $user->forceFill([
                'password_plain' => $request->password,
                'password' => bcrypt($request->password),
                'remember_token' => Str::random(60),
            ])->save();

            // delete Email Auth0 by UserID Auth0
            $client = new \GuzzleHttp\Client();

            $ress = $client->request("POST", "https://mp-institute.au.auth0.com/oauth/token", [
                "header" => [
                    "Content-Type" => "application/json"
                ],
                "form_params" => [
                    "client_id" => "5634bNl8DYL6j1p41B4L4dMLI4aRS0sI",
                    "client_secret" => "CfPIpI_BbNXl-5Py7cKLPw6tDWoDQ6i76m4Or5FrxCwZWfP6vQiZq31ppbVPAd3H",
                    "audience" => "https://mp-institute.au.auth0.com/api/v2/",
                    "grant_type" => "client_credentials"
                ]
            ]);
            $client2 = new \GuzzleHttp\Client();
            $accessToken = json_decode($ress->getBody()->getContents());
            $res = $client2->request("GET", "https://mp-institute.au.auth0.com/api/v2/users-by-email", [
                "headers" => [
                    "Content-Type" => "application/json",
                    "authorization" => "Bearer ". $accessToken->access_token
                ],
                "query" => [
                    "email" => $user->email
                ]
            ]);
            $ddat = json_decode($res->getBody()->getContents());
            if($ddat != []){
                $client3 = new \GuzzleHttp\Client();
                $client3->request("DELETE", "https://mp-institute.au.auth0.com/api/v2/users/".$ddat[0]->user_id, [
                    "headers" => [
                        "Content-Type" => "application/json",
                        "authorization" => "Bearer ". $accessToken->access_token
                    ]
                ]);
            }

            // $this->guard()->login($user);

            $url = config('matrix.url').'/log_out';
            $execute = $this->get_data($url);
            
            $notification = array(
                'message' => 'Success reset password',
                'alert-type' => 'success'
            );
            Session::flush();
            Auth::guard('user')->logout();
            return redirect('/')->with($notification);
        }
        else
        {
            $notification = array(
                'message' => 'Your email not exist!',
                'alert-type' => 'error'
            );

            return back()->with($notification)->withInput($request->only('email'));
        }
    }


    public function broker()
    {
        return Password::broker('users');
    }

    /**
     * Get the guard to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('user');
    }

    function get_data($url)
    {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
}
