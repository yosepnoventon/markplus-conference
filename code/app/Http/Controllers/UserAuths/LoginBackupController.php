<?php

namespace App\Http\Controllers\UserAuths;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Hesto\MultiAuth\Traits\LogsoutGuard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Symfony\Component\HttpFoundation\Cookie;
use Session;
use Mail;
use Config;
class LoginControllerb extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers, LogsoutGuard {
        LogsoutGuard::logout insteadof AuthenticatesUsers;
    }

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('user.guest', ['except' => 'logout']);

        $this->request = $request;
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */

    public function login(Request $request)
    {
        // $authorize_params = [
        //     'scope' => 'openid profile email',
        // ];
        // return \App::make('auth0')->login(null, null, $authorize_params);

        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);

        if(User::where('email', $request->email)->first() == "")
        {
            $notification = array(
                'message' => 'Email doesnt exist',
                'alert-type' => 'error'
            );

            return back()->with($notification)->withInput($request->only('email'));
        }
        elseif(User::where('email', $request->email)->where('verified', User::NOT_VERIFIED)->first())
        {
            $user = User::where('email', $request->email)->where('verified', User::NOT_VERIFIED)->first();

            app('App\Http\Controllers\Config\EmailFunction')->send_verification($user, $request);

            $notification = array(
                'message' => 'Please check your email to verify',
                'alert-type' => 'error'
            );

            return back()->with($notification)->withInput($request->only('email', 'remember'));
        }

        elseif(!Hash::check($request['password'], $this->returnPassword($request->email)))
        {
            $notification = array(
                'message' => 'You may have entered incorrect username or password',
                'alert-type' => 'error'
            );

            return back()->with($notification)->withInput($request->only('email', 'remember'));
        }
        elseif(Auth::guard('user')->attempt(['email' => $request['email'], 'password' => $request['password'], 'verified' => User::VERIFIED]))
        {
            $logged = Auth::guard('user')->user();

            $notification = array(
                'message' => 'Login Success',
                'alert-type' => 'success',
                'code' => 200,
                'userid' => $logged->userid,
                'password' => $request->password
            );
            // $client = new \GuzzleHttp\Client();

            // $res = $client->request('get', config('matrix.url_sso').'/api/v1/login_get', [
            //     'query' => [
            //         'email' => $request->email,
            //         'password' => $request->password
            //     ]
            // ]);
            // $ddata = json_decode($res->getBody()->getContents());
            setcookie('sso_login', $ddata->access_token, time() + (7*62*3600), config('matrix.sso')); // 2 days
            // $user = User::where('email',$request['email']);
            // $user->update(['jwt_token' => $ddata->access_token]);

            $previous_url = Session::get('url.intended');

            if(strpos($previous_url, "verification") !== false)
            {
                return redirect('/')->with($notification);
            }
            if(strpos($previous_url, "login") !== false)
            {
                return redirect('/')->with($notification);
            }
            else
            {
                return Redirect::to(Session::get('url.intended'))->with($notification);
            }
        }
        else
        {
            $notification = array(
                'message' => 'Error some parameter',
                'alert-type' => 'error'
            );
            return back()->with($notification)->withInput($request->only('email', 'remember'));
        }
    }

    public function redirectTo()
    {
        if ($this->request->has('previous'))
        {
            $this->redirectTo = $this->request->get('previous');
        }

        return $this->redirectTo ?? '/';
    }

    public function showLoginForm()
    {
        // $authorize_params = [
        //     'scope' => 'openid profile email',
        //   ];

        // return \App::make('auth0')->login(null, null, $authorize_params);

        Session::put('url.intended', URL::previous());

        return view('user.auth.login');
    }

    public function logout()
    {
        if (Auth::guard('user')->check())
        {
            $url = config('matrix.url').'/log_out';

            $execute = $this->get_data($url);

            setcookie('sso_login', '', 1, '/', '.markplusinstitute.com');
            Session::flush();
            // Auth::guard('user')->logout();

            return redirect('/');
        }

        return redirect('/');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('user');
    }

    public function returnPassword($email)
    {
        $user = User::where('email', $email)->where('verified', User::VERIFIED)->first();

        return $user->password;
    }

    function get_data($url)
    {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
}