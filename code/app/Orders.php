<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Orders extends Model
{
    const STATUS_CONFIRM_PAYMENT = "CONFIRM PAYMENT";
    const STATUS_PENDING         = "PENDING";
    const STATUS_ACCEPTED        = "ACCEPTED";
    const STATUS_PAID            = "PAID";
    const STATUS_DENIED          = "DENIED";
    const STATUS_EXPIRE          = "EXPIRED";
    const STATUS_CANCEL          = "CANCEL";

    protected $table = 'orders';

    protected $primaryKey = 'id';

    protected $fillable = [
        'invoice',
        'is_certificate',
        'learning_method_id',
        'subscription',
        'course',
        'detail_course',
        'course_ticket_price_id',
        'booking_code',
        'price',
        'promo_id',
        'promo_type',
        'promo_value',
        'promo_code',
        'user',
        'arrayPayment',
        'status_code',
        'status',
        'start_date',
        'expire_date',
        'is_payment',
        'order_status',
        'created_by',
        'updated_by'
    ];
    
    protected $casts = [
        'arrayPayment' => 'array'
    ];

    /**
     * Get all order details associated to this order.
     */

    public function ordersAssign()
    {
        return Orders::whereNull('booking_code')->where('status_code', 200)->where('user', Auth::guard('user')->user()->id)->whereNull('created_by')->orderBy('id', 'desc')->get();
    }

    public function orderDetails()
    {
        return $this->hasMany(DetailOrders::class, 'order_id');
    }

    // learning method E-Learning
    public function eLearningDetails()
    {
        return $this->hasMany(DetailOrders::class, 'order_id')->selectRaw('* ,count(*) as qty')->where('learning_method_id', 1)->whereNull('is_certification')->groupBy('subscription_id');
    }

    // learning method Webinar
    public function webinarDetails()
    {
        return $this->hasMany(DetailOrders::class, 'order_id')->selectRaw('* ,count(*) as qty')->where('learning_method_id', 2)->groupBy('course_ticket_price_id');
    }

    // learning method Certificate
    public function certificateDetails()
    {
        return $this->hasMany(DetailOrders::class, 'order_id')->selectRaw('* ,count(*) as qty')->where('learning_method_id', 3)->groupBy('course_id');
    }

    // learning method In-Class
    public function inClassDetails()
    {
        return $this->hasMany(DetailOrders::class, 'order_id')->selectRaw('* ,count(*) as qty')->where('learning_method_id', 4)->groupBy('course_ticket_price_id');
    }

    public function users()
    {
        return $this->belongsTo(User::class,'user');
    }

    /**
     * get all order details for each learning method that has been received adn for Assign Product
     */
    // learning method E-Learning
    public function assignELearningDetails()
    {
        return $this->hasMany(DetailOrders::class, 'order_id')->selectRaw('* ,count(*) as qty')->where('learning_method_id', 1)->whereNull('is_certification')->groupBy('learning_method_id');
    }

    // learning method Webinar
    public function assignWebinarDetails()
    {
        return $this->hasMany(DetailOrders::class, 'order_id')->selectRaw('* ,count(*) as qty')->where('learning_method_id', 2)->groupBy('detail_course_id');
    }

    // learning method In-Class
    public function assignInClassDetails()
    {
        return $this->hasMany(DetailOrders::class, 'order_id')->selectRaw('* ,count(*) as qty')->where('learning_method_id', 4)->groupBy('detail_course_id');
    }

    static function CountOrderDetailTicketSuccess($course_detail_id, $course_ticket_price_id)
    {
        return Orders::where('detail_course', $course_detail_id)
                    ->where('course_ticket_price_id', $course_ticket_price_id)
                    ->where('status_code', 200)
                    ->count();
    }
}
