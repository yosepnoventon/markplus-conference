<?php

namespace App;

use App\Notifications\UserResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    const VERIFIED = 1;
    const NOT_VERIFIED = 0;

    protected $table = 'users';

    protected $fillable = [
        'first_name',
        'last_name',
        'phone',
        'email',
        'dob',
        'company',
        'password',
        'password_plain',
        'token',
        'jwt_token',
        'userid',
        'user_id_matrix',
        'remember_token',
        'verified',
        'sales_id',
        'created_by',
        'updated_by'
    ];

    protected $hidden = [
        'password', 'remember_token', 'password_plain', 'token', 'jwt_token'
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new UserResetPassword($token));
    }
}
