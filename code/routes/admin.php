<?php
Route::get('/', ['as' => 'transaction.data', 'uses' =>  'Admin\\TransactionController@index'])->name('home');
Route::get('/transaction/datatable/', ['as' => 'transaction.datatable', 'uses' =>  'Admin\\TransactionController@transaction_data']);
Route::get('/transaction', ['uses' =>  'Admin\\TransactionController@transaction']);
Route::get('/transaction/{id}', ['uses' =>  'Admin\\TransactionController@transaction_detail']);