<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'middleware' => ['requiredParameterJson']], function () {
    Route::group(['middleware' => ['checkToken']], function () {
        Route::post('/createCategory', 'Api\CategoryController@save');
        Route::put('/updateCategory/{id_matrix}', 'Api\CategoryController@update');
        
        Route::post('/createLearnMethod', 'Api\LearnMethodController@save');
        Route::put('/updateLearnMethod/{id_matrix}', 'Api\LearnMethodController@update');

        Route::post('/createCourse', 'Api\CourseController@save');
        Route::put('/updateCourse/{id_matrix}', 'Api\CourseController@update');

        Route::post('/createDetailCourse', 'Api\DetailCourseController@save');
        Route::put('/updateDetailCourse/{id_matrix}', 'Api\DetailCourseController@update');

        Route::post('/setCourseProgress', 'Api\CourseProgressController@set');

        Route::get('/login', 'Api\LoginController@login');
        Route::get('/logout', 'Api\LoginController@logout');
    });
});