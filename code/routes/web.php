<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('captcha', function() {
  return '<p>' . captcha_img() . '</p>';
});
//web
Route::get('/', function(){
  return redirect('/register');
});

### Login Auth ##
// Route::get('/login', 'UserAuths\LoginController@index')->name('index');
// Route::post('/login', 'UserAuths\LoginController@login');
// Route::get('/logout', 'UserAuths\LoginController@logout');


Route::group(['middleware' => 'login.guest'], function () {
    Route::get('/login', 'Web\\WebController@login')->name('index');
    Route::post('/login', 'Web\\WebController@submit_login');
    Route::get('/register', 'Web\\WebController@register');
    Route::post('/register', 'Web\\WebController@submit_register');
});
Route::get('/logout', 'Web\\WebController@logout');


// use middleware user
Route::group(['middleware' => 'login'], function () {
  Route::get('/success', 'Web\\WebController@index');
  Route::get('/introduction', 'Web\\WebController@index');
  Route::get('/virtual-reality', 'Web\\WebController@virtualReality');

  Route::get('/chat', 'Web\\WebController@chat');
  Route::post('chat', 'Web\\ChatController@store')->name('chat.store');
  Route::post('chat/join', 'Web\\ChatController@join')->name('chat.join');
  Route::post('interest/add', 'Web\\InterestPromoController@add')->name('interest.add');
});
// end middleware

//Midtrans
Route::post('/payment-midtrans', ['as'=>'midtrans.notification', 'uses'=> 'Web\\MidtransController@vtweb']);
Route::get('/payment-midtrans/{order_id}', ['as'=>'midtrans.notification', 'uses'=> 'Web\\MidtransController@vtweb']);
Route::post('/payment-midtrans/{order_id}', ['as'=>'midtrans.notification', 'uses'=> 'Web\\MidtransController@vtweb']);
Route::get('/do_payment', 'Web\\PaymentController@do_payment');

Route::post('/midtrans/notification', ['as'=>'midtrans.notification', 'uses'=> 'Web\\MidtransController@notification']);
Route::get('/midtrans/paymentfinish', ['as'=>'midtrans.paymentfinish', 'uses'=> 'Web\\MidtransController@paymentfinish']);
Route::get('/midtrans/paymentunfinish', ['as'=>'midtrans.paymentunfinish', 'uses'=> 'Web\\MidtransController@paymentunfinish']);
Route::get('/midtrans/errorhandle', ['as'=>'midtrans.errorhandle', 'uses'=> 'Web\\MidtransController@errorhandle']);

//static page
Route::get('/about-us', 'Web\\StaticPageController@about_us');
Route::get('/contact-us', 'Web\\StaticPageController@contact_us');
Route::get('/faq', 'Web\\StaticPageController@faq');
Route::get('/term-and-condition', 'Web\\StaticPageController@term_and_condition');

Route::group(['prefix' => 'admin'], function ()
{
    Route::get('/login', 'AdminAuth\\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'AdminAuth\\LoginController@login');
    Route::get('/logout', 'AdminAuth\\LoginController@logout')->name('logout');

    Route::get('/profile', 'Admin\\UsersController@profile');
    
    //master
    Route::get('/users/data', ['as' => 'users-admin.data', 'uses' => 'Admin\\UsersController@anyData']);
    Route::resource('users', 'Admin\\UsersController');

    Route::get('/vouchers/data', ['as' => 'vouchers.data', 'uses' => 'Admin\\VouchersController@anyData']);
    Route::resource('vouchers', 'Admin\\VouchersController');
    Route::post('/vocuhers/randomCode', ['uses' => 'Admin\\VouchersController@randomCode']);
    Route::post('/vocuhers/cekCode', ['uses' => 'Admin\\VouchersController@cekCode']);
    Route::get('vocuhers/export', 'Admin\\VouchersController@csv_export')->name('export');

    
    Route::get('participants/export', 'Admin\\ParticipantsController@csv_export')->name('export.participants');
    Route::get('/participants/data', ['as' => 'participants.data', 'uses' => 'Admin\\ParticipantsController@anyData']);
    Route::resource('participants', 'Admin\\ParticipantsController');


    Route::get('/report/promo-used', ['as' => 'report.promo-used', 'uses' => 'Admin\\ReportPromoController@promo_used']);
    Route::get('/report/promo-used/data', ['as' => 'report.promo-used.data', 'uses' => 'Admin\\ReportPromoController@anyDataUsed']);

    Route::get('/about-us/data', ['as' => 'about-us.data', 'uses' => 'Admin\\AboutUsController@anyData']);
    Route::resource('about-us', 'Admin\\AboutUsController');

});
// test email
Route::get('/email', function(){
  return view('admin.email.test');
});
