<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
        'webhook' => [
            'secret' => env('STRIPE_WEBHOOK_SECRET'),
            'tolerance' => env('STRIPE_WEBHOOK_TOLERANCE', 300),
        ],
    ],
    'midtrans' => [
        // Midtrans server key
        'serverKey'     => env('MIDTRANS_SERVERKEY'),
        // Midtrans client key
        'clientKey'     => env('MIDTRANS_CLIENTKEY'),
        // Isi false jika masih tahap development dan true jika sudah di production, default false (development)
        'isProduction'  => env('MIDTRANS_IS_PRODUCTION', false),
        'isSanitized'   => env('MIDTRANS_IS_SANITIZED', true),
        'is3ds'         => env('MIDTRANS_IS_3DS', true),
        'snap'         => env('MIDTRANS_SNAP', "https://app.sandbox.midtrans.com/snap/snap.js"),
    ],
    'api_key' => env('API_KEY', 'W8puqnZuQmoxUMM0oA_o21b8NyX7ts3Gw'),
    'is_production' => env('CODEMI_IS_PRODUCTION', true),

    'firebase' => [
        'api_key' => 'AIzaSyBs0M9-LmMsF-WHwS1UYB3PuuldiwjBg_A',
        'auth_domain' => 'chat-cranium.firebaseapp.com',
        'database_url' => 'https://chat-cranium.firebaseio.com/',
        'secret' => 'LSwnk0u5z8wnOu8YiHCBd3LlFzFt2TiIAGUm0LG0',
        'storage_bucket' => 'chat-cranium.appspot.com',
        'project_id' => 'chat-cranium',
        'messaging_sender_id' => '174777682602'
    ]
];
