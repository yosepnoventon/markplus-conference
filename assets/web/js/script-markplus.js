$(document).ready(function() {
    $("#sidebar").mCustomScrollbar({
        theme: "minimal"
    });

    $('#dismiss, .overlay').on('click', function() {
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    });

    $('#sidebarCollapse').on('click', function() {
        $('#sidebar').addClass('active');
        $('.overlay').addClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });


    $("#search-show").click(function() {
        $("#header-search").slideToggle()
        $("#overlay-header").addClass("active")
        $("#header-top").css("display", "none")
    })
    $("#btn-close").click(function() {
        $("#header-top").slideToggle()
        $("#header-search").css("display", "none")
        $("#overlay-header").removeClass("active")
    })

    $("#overlay-header").click(function() {

        $("#overlay-header").removeClass("active")
        $("#header-top").slideToggle()
        $("#header-search").css("display", "none")
    })


});





// Back To Top
// When the user scrolls down 20px from the top of the document, show the button
// window.onscroll = function() {
//     scrollFunction()
// };

// function scrollFunction() {
//     if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
//         document.getElementById("myBtn").style.display = "block";
//     } else {
//         document.getElementById("myBtn").style.display = "none";
//     }
// }

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
// Select Boxes
$('.select-custom').each(function() {

    var $this = $(this),
        numberOfOptions = $(this).children('option').length;

    $this.addClass('select-hidden');
    $this.wrap('<div class="select-noa" id="floating-input"></div>');
    $this.after('<div class="select-styled-noa"></div>');

    var $styledSelect = $this.next('div.select-styled-noa');
    $styledSelect.text($this.children('option').eq(0).text());

    var $list = $('<ul />', {
        'class': 'select-options-noa'
    }).insertAfter($styledSelect);

    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
    }

    var $listItems = $list.children('li');

    $styledSelect.click(function(e) {
        e.stopPropagation();
        $('div.select-styled-noa.active').not(this).each(function() {
            $(this).removeClass('active').next('ul.select-options-noa').hide();
        });
        $(this).toggleClass('active').next('ul.select-options-noa').toggle();
    });

    $listItems.click(function(e) {
        e.stopPropagation();
        $styledSelect.text($(this).text()).removeClass('active');
        $this.val($(this).attr('rel'));
        $list.hide();
        //console.log($this.val());
    });

    $(document).click(function() {
        $styledSelect.removeClass('active');
        $list.hide();
    });

});